//
//  ShipmentType+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/30/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ShipmentType {

    @NSManaged var mode: String?
    @NSManaged var name: String?
    @NSManaged var type: String?

}

//
//  SalesRepresentative.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/6/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(SalesRepresentative)
class SalesRepresentative: NSManagedObject {

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(SalesRepresentative))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            let dictionary = item as! NSDictionary
            let newRow: SalesRepresentative = CoreDataHelper.getRowEntity(NSStringFromClass(SalesRepresentative)) as! SalesRepresentative
            
            newRow.number = dictionary["number"] as! String
            newRow.name = dictionary["name"] as! String
        }
        CoreDataHelper.saveContext()
    }
    
    class func getAllElements() -> NSMutableArray{
        let salesRepresentatives: NSMutableArray = NSMutableArray()
        var salesRepresentativeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(SalesRepresentative))
        
        for item in array{
            salesRepresentativeDic = NSMutableDictionary()
            
            salesRepresentativeDic!.setValue((item as! SalesRepresentative).name, forKey: "description")
            salesRepresentativeDic!.setValue((item as! SalesRepresentative).number, forKey: "code")
            
            salesRepresentatives.addObject(salesRepresentativeDic!)
        }
        
        return salesRepresentatives
    }
    
    class func getName(employeeId:String) -> String? {
        //let salesRepresentatives: NSMutableArray = NSMutableArray()
        //var salesRepresentativeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString("SalesRepresentative", filter: "number == %@", value: employeeId)
        print("array Sales representative \(array.count)")
        return array.count > 0 ? (array[0] as! SalesRepresentative).name : employeeId
    }

}

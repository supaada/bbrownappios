//
//  Doctor.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/30/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(Doctor)
class Doctor: NSManagedObject {

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(Doctor))
    }
    
    class func saveData(array: NSArray){
        
        for doctorName in array {
            let newRow: Doctor = CoreDataHelper.getRowEntity(NSStringFromClass(Doctor)) as! Doctor
            newRow.name = doctorName as? String
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func getAllElements() -> NSMutableArray{
        let doctors: NSMutableArray = NSMutableArray()
        var doctorDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Doctor))
        
        for item in array{
            doctorDic = NSMutableDictionary()
            
            doctorDic!.setValue((item as! Doctor).name, forKey: "description")
            doctorDic!.setValue((item as! Doctor).name, forKey: "code")
            
            doctors.addObject(doctorDic!)
        }
        
        return doctors
    }

}

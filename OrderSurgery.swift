//
//  OrderSurgery.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/9/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(OrderSurgery)
class OrderSurgery: NSManagedObject {
    struct OrderSurgeryProperties {
        static let statisticsOrderNumberFilter = "statusId == %@"
        static let pendingOrderNumberFilter = "statusId == 1 AND wasSent == %@"
        static let createdOrderNumberFilter = "statusId == 1 AND wasSent == %@"
        static let idFilter = "id == %@"
        static let orderNumberToSendFilter = "wasSent == %@"
    }
// Insert code here to add functionality to your managed object subclass
    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(OrderSurgery))
    }
    
    class func deleteData(idOrder: NSNumber){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: NSManagedObject? = array.first as? NSManagedObject
        
        if order != nil {
            CoreDataHelper.deleteRow(order!)
        }
    }
    
    class func getAllRejectedOrders() -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.statisticsOrderNumberFilter, value: 2)
        
        return getElements(array)
    }
    
    class func getAllStatisticOrders() -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.statisticsOrderNumberFilter, value: 3)
        
        return getElements(array)
    }
    
    class func getAllPendingOrders() -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.pendingOrderNumberFilter, value: 0)
        
        return getElements(array)
    }
    
    class func getAllCreatedOrders() -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.pendingOrderNumberFilter, value: 1)
        
        return getElements(array)
    }
    
    class func getAllOrdersToSend() -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.orderNumberToSendFilter, value: 0)
        
        return NSMutableArray(array: array)
    }
    
    class func getElements(array: [AnyObject]) -> NSMutableArray{
        let elements: NSMutableArray = NSMutableArray()
        
        for item in array{
            let element: OrderSurgery = item as! OrderSurgery
            let elementDic: NSMutableDictionary = NSMutableDictionary()
            
            elementDic.setValue(element.id, forKey: "id")
            elementDic.setValue(element.folioOrder, forKey: "folioOrder")
            elementDic.setValue(element.patientName, forKey: "patientName")
            elementDic.setValue(element.surgeryType, forKey: "surgeryType")
            elementDic.setValue(element.customerName, forKey: "customerName")
            elementDic.setValue(element.doctorName, forKey: "doctorName")
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            elementDic.setValue(dateFormatter.stringFromDate(element.surgeryDateTime ?? NSDate()), forKey: "surgeryDateTime")
            
            elements.addObject(elementDic)
        }
        
        return elements
    }
    
    class func countPendingOrders() -> Int{
        let count = CoreDataHelper.countForFetchByFilterNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.pendingOrderNumberFilter, value: 0)
        
        return count
    }
    
    class func getOrder(idOrder: NSNumber) -> OrderSurgery?{
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        print("array count \(array.count)")
        return order
    }
    
    class func saveData(surgeryType: SurgeryTypeViewStruct?, contactRequestSurgery: ContactRequestSurgeryViewStruct?, patientInfo: PatientInfoViewStruct?, comments: String?, recordDateTime: NSDate) -> NSNumber{
        var id: NSNumber = 0
        
        if let surgeryType = surgeryType {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let seconds: NSTimeInterval = NSDate().timeIntervalSince1970
            let milliseconds:Double =  seconds*1000
            let newRow: OrderSurgery = CoreDataHelper.getRowEntity(NSStringFromClass(OrderSurgery)) as! OrderSurgery
            var idSequence: Int64 = Int64(NSUserDefaults.standardUserDefaults().doubleForKey("idSequence"))
            
            idSequence += 1
            newRow.id = NSNumber(longLong: idSequence)
            id = newRow.id!
            
            // Insertando Datos de la vista de Tipo de Cirugia
            if let folioOrder = surgeryType.folioOrder where !folioOrder.isEmpty {
                newRow.folioOrder = folioOrder
            }else{
                newRow.folioOrder = ""
            }
            
            if let surgeryTypeName = surgeryType.surgeryTypeName, surgeryTypeNumber = surgeryType.surgeryTypeNumber {
                newRow.surgeryType = surgeryTypeName
                newRow.surgeryTypeNumber = surgeryTypeNumber
            }

            
            if let surgeryFamilyName = surgeryType.surgeryFamilyTypeName, surgeryFamilyNumber = surgeryType.surgeryFamilyTypeNumber {
                newRow.surgeryFamilyName = surgeryFamilyName
                newRow.surgeryFamilyNumber = surgeryFamilyNumber
            }
            
            if let surgerySetName = surgeryType.surgerySetName, surgerySetNumber = surgeryType.surgerySetNumber {
                newRow.surgerySetName = surgerySetName
                newRow.surgerySetNumer = surgerySetNumber
            }

            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            if let surgeryDate = surgeryType.surgeryDateTime, formated = dateFormatter.dateFromString(surgeryDate){
                newRow.surgeryDateTime =  formated
            } else {
                newRow.surgeryDateTime =  NSDate()
            }
            newRow.timestamp = NSDate()
            
            if let implantSystemName = surgeryType.implantSystemName, implantSystemCode = surgeryType.implantSystemCode {
                newRow.implantSystem = implantSystemName
                newRow.implantSystemCode = implantSystemCode
                newRow.implantSystemRequired = false
            }
            
            if let instrumentalSetName = surgeryType.instrumentalSetName , instrumentalSetCode = surgeryType.instrumentalSetCode {
                newRow.instrumentalSet = instrumentalSetName
                newRow.instrumentalSetCode = instrumentalSetCode
                newRow.instrumentalSetRequired = false
            }
            
            if let technicalName = surgeryType.technicalName , technicalNumber = surgeryType.technicalNumber, technicalRequired = surgeryType.technicalRequired {
                newRow.technical = technicalName
                newRow.technicalNumber = technicalNumber
                newRow.technicalRequired = technicalRequired
            }
            
            if let salesRepresentativeName = surgeryType.salesRepresentativeName, salesRepresentativeNumber = surgeryType.salesRepresentativeNumber {
                newRow.salesRepresentative = salesRepresentativeName
                newRow.salesRepresentativeNumber = salesRepresentativeNumber
            }
            
            if let priceImplantSystem = surgeryType.priceImplantSystem {
                newRow.priceImplantSystem = priceImplantSystem
            }
            
            if let priceListName = surgeryType.priceListName {
                newRow.priceList = priceListName
            }
            
            if let priceListCode = surgeryType.priceListCode {
                newRow.priceListCode = priceListCode
            }
            
            if let statusId = surgeryType.statusId {
                newRow.statusId = statusId
            }

            // Insertando Datos de la vista de Contacto de la Solicitud de Orden
            if contactRequestSurgery != nil {
                if contactRequestSurgery!.customerName != nil {
                    newRow.customerName = contactRequestSurgery!.customerName!
                }
                
                if contactRequestSurgery!.customerNumber != nil {
                    newRow.customerNumber = contactRequestSurgery!.customerNumber!
                }
                
                if contactRequestSurgery!.doctorName != nil {
                    newRow.doctorName = contactRequestSurgery!.doctorName!
                }
                
                if contactRequestSurgery!.patientName != nil {
                    newRow.patientName = contactRequestSurgery!.patientName!
                }
                
                if contactRequestSurgery!.shipmentType != nil {
                    newRow.shipmentType = contactRequestSurgery!.shipmentType!
                }
                
                if contactRequestSurgery!.billToCode != nil {
                    newRow.billToId = contactRequestSurgery!.billToCode!
                }
                
                if contactRequestSurgery!.otherCustomerName != nil {
                    newRow.otherCustomer = contactRequestSurgery!.otherCustomerName!
                }
                
                if contactRequestSurgery!.otherCustomerNumber != nil {
                    newRow.otherCustomerNumber = contactRequestSurgery!.otherCustomerNumber!
                }
                
                if contactRequestSurgery!.paymentConditionCode != nil {
                    newRow.paymentConditionId = contactRequestSurgery!.paymentConditionCode!
                }
                
                if contactRequestSurgery!.hasDepositInfo != nil && contactRequestSurgery!.hasDepositInfo == true {
                    let newRowDepositInfo: DepositInformation = CoreDataHelper.getRowEntity(NSStringFromClass(DepositInformation)) as! DepositInformation
                    
                    if contactRequestSurgery!.bank != nil {
                        newRowDepositInfo.bank = contactRequestSurgery!.bank!
                    }
                    
                    if contactRequestSurgery!.depositDate != nil {
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        newRowDepositInfo.depositDate = dateFormatter.dateFromString(contactRequestSurgery!.depositDate!)!
                    }
                    
                    if contactRequestSurgery!.referenceNumber != nil {
                        newRowDepositInfo.referenceNumber = contactRequestSurgery!.referenceNumber!
                    }
                    
                    if contactRequestSurgery!.transactionNumber != nil {
                        newRowDepositInfo.transactionNumber = contactRequestSurgery!.transactionNumber!
                    }
                    
                    newRow.depositInfo = newRowDepositInfo
                }
            }
            
            // Insertando Datos de la vista de Datos Fiscales del Paciente
            if patientInfo != nil {
                let newRowPatientInfo: PatientInformation = CoreDataHelper.getRowEntity(NSStringFromClass(PatientInformation)) as! PatientInformation
                
                
                if patientInfo!.name != nil {
                    newRowPatientInfo.name = patientInfo!.name!
                }
                
                if patientInfo!.taxId != nil {
                    newRowPatientInfo.taxId = patientInfo!.taxId!
                }
                
                if patientInfo!.street != nil {
                    newRowPatientInfo.street = patientInfo!.street!
                }
                
                if patientInfo!.buildingNumber != nil {
                    newRowPatientInfo.buildingNumber = patientInfo!.buildingNumber!
                }
                
                if patientInfo!.apparment != nil {
                    newRowPatientInfo.apparment = patientInfo!.apparment!
                }
                
                if patientInfo!.locality != nil {
                    newRowPatientInfo.locality = patientInfo!.locality!
                }
                
                if patientInfo!.postalCode != nil {
                    newRowPatientInfo.postalCode = patientInfo!.postalCode!
                }
                
                if patientInfo!.town != nil {
                    newRowPatientInfo.town = patientInfo!.town!
                }
                
                if patientInfo!.stateName != nil {
                    newRowPatientInfo.state = patientInfo!.stateName!
                }
                
                if patientInfo!.countryName != nil {
                    newRowPatientInfo.country = patientInfo!.countryName!
                }
                
                if patientInfo!.email != nil {
                    newRowPatientInfo.email = patientInfo!.email!
                }
                
                newRow.patientInfo = newRowPatientInfo
            }
            
            // Insertando Datos de la vista de Datos Fiscales del Paciente
            if comments != nil {
                newRow.comments = comments!
            }
            
            newRow.recordDateTime = recordDateTime
            newRow.wasSent = NSNumber(bool: false)
            
            CoreDataHelper.saveContext()
            NSUserDefaults.standardUserDefaults().setValue(newRow.id!.doubleValue, forKey: "idSequence")
        }
        
        return id
    }
    
    class func setSentOrder(idOrder: NSNumber){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        
        order?.wasSent = NSNumber(bool: true)
        
        CoreDataHelper.saveContext()
    }
    
    class func updateSentOrder(idOrder: NSNumber, folioOrder: String, status: NSNumber, recordDate: String?, recordTime: String?, comments: String?){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        
        order?.folioOrder = folioOrder
        order?.statusId = status
        
        if recordDate != nil && recordTime != nil {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let recordDateTime: String = recordDate! + " " + recordTime!
            
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            
            order?.recordDateTime = dateFormatter.dateFromString(recordDateTime)!
        }
        
        if comments != nil {
            order?.comments = comments!
        }
        
        order?.wasSent = NSNumber(bool: true)
        
        CoreDataHelper.saveContext()
    }
    
    class func updateSentOrder(idOrder: NSNumber, folioOrder: String, status: NSNumber, recordDateTime: NSDate, comments: String?){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        
        order?.folioOrder = folioOrder
        order?.statusId = status
        order?.recordDateTime = recordDateTime
        
        
        if comments != nil {
            order?.comments = comments!
        }
        
        order?.wasSent = NSNumber(bool: true)
        
        CoreDataHelper.saveContext()
    }
    
    class func updateSentOrder2(idOrder: NSNumber, folioOrder: String, recordDateTime: NSDate, comments: String?){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        
        order?.folioOrder = folioOrder
        order?.recordDateTime = recordDateTime
        
        
        if comments != nil {
            order?.comments = comments!
        }
        
        //order?.wasSent = NSNumber(bool: true)
        
        CoreDataHelper.saveContext()
    }
    
    class func updateSentOrder3(idOrder: NSNumber, folioOrder: String, status: NSNumber, recordDateTime: NSDate){
        let array = CoreDataHelper.fetchFilterEntityNumber(NSStringFromClass(OrderSurgery), filter: OrderSurgeryProperties.idFilter, value: idOrder)
        let order: OrderSurgery? = array.first as? OrderSurgery
        
        order?.folioOrder = folioOrder
        order?.statusId = status
        order?.recordDateTime = recordDateTime
        
        order?.wasSent = NSNumber(bool: true)
        
        CoreDataHelper.saveContext()
    }
}

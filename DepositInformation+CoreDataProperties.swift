//
//  DepositInformation+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/6/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DepositInformation {

    @NSManaged var bank: String?
    @NSManaged var depositDate: NSDate?
    @NSManaged var referenceNumber: String?
    @NSManaged var transactionNumber: String?
    @NSManaged var deposit: NSData?
    @NSManaged var orderSurgery: OrderSurgery?

}

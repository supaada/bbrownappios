//
//  DestinationInformation+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/6/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension DestinationInformation {

    @NSManaged var appartment: String?
    @NSManaged var buildingNumber: String?
    @NSManaged var countryCode: String?
    @NSManaged var stateCode: String?
    @NSManaged var email: String?
    @NSManaged var locality: String?
    @NSManaged var street: String?
    @NSManaged var name: String?
    @NSManaged var postalCode: String?
    @NSManaged var town: String?
    @NSManaged var destinationId: String?
    @NSManaged var clientId: String?
    @NSManaged var orderSurgery: OrderSurgery?

}

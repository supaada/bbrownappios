//
//  ImplantSet+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/4/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ImplantSet {

    @NSManaged var uuid: NSNumber?
    @NSManaged var implantesAdicional1: String?
    @NSManaged var implantesBase: String?
    @NSManaged var surgery: Surgery?

}

//
//  PatientInformation+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/6/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PatientInformation {

    @NSManaged var apparment: String?
    @NSManaged var buildingNumber: String?
    @NSManaged var country: String?
    @NSManaged var email: String?
    @NSManaged var locality: String?
    @NSManaged var name: String?
    @NSManaged var postalCode: String?
    @NSManaged var state: String?
    @NSManaged var street: String?
    @NSManaged var taxId: String?
    @NSManaged var town: String?
    @NSManaged var orderSurgery: OrderSurgery?

}

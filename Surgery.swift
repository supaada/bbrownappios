//
//  Surgery.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/3/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

enum SurgeryPredicates:String {
    case id = "uuid  == %@"
}

@objc(Surgery)
class Surgery: NSManagedObject {
    
    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(Surgery))
    }
    
    class func deleteDataAll(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ImplantSet))
        CoreDataHelper.deleteFrom(NSStringFromClass(SurgeryTypeSet))
        CoreDataHelper.deleteFrom(NSStringFromClass(Surgery))
    }
    
// Insert code here to add functionality to your managed object subclass
    class func saveData(array: NSArray){
        for item in array {
            if let surgery:NSDictionary = item as? NSDictionary {
                let newImplantSet: ImplantSet = CoreDataHelper.getRowEntity(NSStringFromClass(ImplantSet)) as! ImplantSet
                let newSurgery = CoreDataHelper.getRowEntity(NSStringFromClass(Surgery)) as! Surgery
                let newSurgeryTypeSet: SurgeryTypeSet = CoreDataHelper.getRowEntity(NSStringFromClass(SurgeryTypeSet)) as! SurgeryTypeSet
                newImplantSet.uuid = surgery["claveSetImplante"] as? Int
                
                if let implantBase = surgery["implantBase"] as? String where implantBase != "." {
                    newImplantSet.implantesBase = implantBase
                }
                
                if let adicionalImplant1 = surgery["aditionalImplant1"] as? String where adicionalImplant1 != "." {
                    newImplantSet.implantesAdicional1 = adicionalImplant1
                }
                
                newSurgeryTypeSet.uuid = surgery["instrumentalSetCode"] as? Int
                if let instumentalBase = surgery["instrumentalBase"] as? String where instumentalBase != "." {
                    newSurgeryTypeSet.instrumentalBase = instumentalBase
                }
                if let adicional1 = surgery["aditional1"] as? String where adicional1 != "." {
                    newSurgeryTypeSet.adicional1 = adicional1
                }
                if let adicional2 = surgery["aditional2"] as? String where adicional2 != "." {
                    newSurgeryTypeSet.adicional2 = adicional2
                }
                if let adicional3 = surgery["aditional3"] as? String where adicional3 != "." {
                    newSurgeryTypeSet.adicional3 = adicional3
                }
                if let adicional4 = surgery["aditional4"] as? String where adicional4 != "." {
                    newSurgeryTypeSet.adicional4 = adicional4
                }
                
                newSurgery.uuid = surgery["surgeryId"] as? Int
                newSurgery.surgeryType = surgery["surgeryCode"] as? String
                newSurgery.familyType = surgery["surgerySetFamily"] as? String
                newSurgery.claveSetCirugia = surgery["surgerySetCode"] as? String
                newSurgery.descripcionSetCirugia = surgery["surgerySetDescription"] as? String
                newSurgery.precio = surgery["precio"] as? Int
                newSurgery.surgeryTypeSet = newSurgeryTypeSet
                newSurgery.implantSet = newImplantSet
            }
        }
        CoreDataHelper.saveContext()
    }

    
    class func save(row: Dictionary<String,String>){
        if let newRow: Surgery = CoreDataHelper.getRowEntity(NSStringFromClass(Surgery)) as? Surgery, let rowId = row["id_tipo_cirugia"], let rowIdInt = Int(rowId), let rowClave = row["clave_tipo_cirugia"] {
            newRow.uuid = rowIdInt
            newRow.surgeryType = rowClave
            CoreDataHelper.saveContext()
        }
    }
    
    class func getElementsSetCirugia(nameDelegate:String) -> (setsIntrumental:NSArray, baseIntrumental:String, setsImplants:NSArray, baseImplant:String){
        let arraySetsInstrumental: NSMutableArray = NSMutableArray()
        let arraySetsImplante: NSMutableArray = NSMutableArray()
        let array = CoreDataHelper.fetchFilterEntityString("Surgery", filter: "familyType == %@", value: nameDelegate)
        var setSetsInstrumental = Set<String>()
        var setSetsImplant = Set<String>()
        var baseInstrumentalTuple = ""
        var baseImplantTuple = ""
        for item in array{
            if let current = item as? Surgery {
                if let base = current.surgeryTypeSet?.instrumentalBase where base != "NO APLICA"  {
                    setSetsInstrumental.insert(base)
                    baseInstrumentalTuple = base
                }
                if let aditional1 = current.surgeryTypeSet?.adicional1 where aditional1 != "empty" && aditional1 != "NO APLICA" {
                    setSetsInstrumental.insert(aditional1)
                }
                if let aditional2 = current.surgeryTypeSet?.adicional2 where aditional2 != "empty" && aditional2 != "NO APLICA"  {
                    setSetsInstrumental.insert(aditional2)
                }
                if let aditional3 = current.surgeryTypeSet?.adicional3 where aditional3 != "empty" && aditional3 != "NO APLICA" {
                    setSetsInstrumental.insert(aditional3)
                }
                if let adicional4 = current.surgeryTypeSet?.adicional4 where adicional4 != "empty" && adicional4 != "NO APLICA" {
                    setSetsInstrumental.insert(adicional4)
                }
                if let baseImplant = current.implantSet?.implantesBase where baseImplant != "" && baseImplant != "NO APLICA" {
                    setSetsImplant.insert(baseImplant)
                    baseImplantTuple = baseImplant
                }
                if let aditionalImplant1 = current.implantSet?.implantesAdicional1 where aditionalImplant1 != "empty" && aditionalImplant1 != "" && aditionalImplant1 != "NO APLICA" {
                    setSetsImplant.insert(aditionalImplant1)
                }
            }
        }
        
        for instrumental in setSetsInstrumental.sort() {
            arraySetsInstrumental.addObject(["code":instrumental,"description":instrumental])
        }
        for implante in setSetsImplant.sort() {
            arraySetsImplante.addObject(["code":implante,"description":implante])
        }
        return (arraySetsInstrumental, baseInstrumentalTuple, arraySetsImplante, baseImplantTuple)
    }
    
    class func getSurgerySetBySets(setsImplantes:Set<String>, setsInstrumental:Set<String>, baseSent:String) -> NSDictionary? {
        var surgeryTypeDic: NSDictionary?

        let arrayLists = CoreDataHelper.fetchFilterEntityString("Surgery", filter: "familyType == %@", value: baseSent)
        var idSurgeryType: NSNumber?

        for item in arrayLists {
            if let current = item as? Surgery, setsInstrumentalCurrent = current.surgeryTypeSet, setsImplantCurrent = current.implantSet  {
                var setsInstrumentalDB = Set<String>()
                var setsImplantDB = Set<String>()

                if let base = setsInstrumentalCurrent.instrumentalBase where base != "empty" && base != "NO APLICA" {
                    setsInstrumentalDB.insert(base)
                }
                if let adicional1 = setsInstrumentalCurrent.adicional1 where adicional1 != "empty" {
                    setsInstrumentalDB.insert(adicional1)
                }
                if let adicional2 = setsInstrumentalCurrent.adicional2 where adicional2 != "empty" {
                    setsInstrumentalDB.insert(adicional2)
                }
                if let adicional3 = setsInstrumentalCurrent.adicional3 where adicional3 != "empty" {
                    setsInstrumentalDB.insert(adicional3)
                }
                if let adicional4 = setsInstrumentalCurrent.adicional4 where adicional4 != "empty" {
                    setsInstrumentalDB.insert(adicional4)
                }
                
                if let baseImplant = setsImplantCurrent.implantesBase where baseImplant != "empty" && baseImplant != "NO APLICA" {
                    setsImplantDB.insert(baseImplant)
                }
                
                if let aditionalImplant = setsImplantCurrent.implantesAdicional1 where aditionalImplant != "empty" && aditionalImplant != "NO APLICA" {
                    setsImplantDB.insert(aditionalImplant)
                }
                
                if setsInstrumentalDB.count == setsInstrumental.count && setsInstrumentalDB.isSubsetOf(setsInstrumental) && setsInstrumental.isSubsetOf(setsInstrumentalDB) {
                    if setsImplantDB.count == setsImplantes.count && setsImplantDB.isSubsetOf(setsImplantes) && setsImplantes.isSubsetOf(setsImplantDB) {
                        idSurgeryType = current.uuid
                        break
                    }
                }
                
                if setsImplantDB.count == setsImplantes.count && setsImplantDB.isSubsetOf(setsImplantes) && setsImplantes.isSubsetOf(setsImplantDB) {
                    if setsInstrumentalDB.count == setsInstrumental.count && setsInstrumentalDB.isSubsetOf(setsInstrumental) && setsInstrumental.isSubsetOf(setsInstrumentalDB) {
                        idSurgeryType = current.uuid
                        break
                    }
                }
            }
        }
        if let idSurgeryType = idSurgeryType {
        let array = CoreDataHelper.fetchFilterEntityNumber("Surgery", filter: "uuid == %@", value: idSurgeryType)
        for item in array{
            if let current = item as? Surgery, description = current.descripcionSetCirugia, code = current.familyType, uuid = current.uuid {
                surgeryTypeDic = ["description":description, "code":code, "uuid":uuid]
            }
        }
        }
        return surgeryTypeDic
    }
    
    class func getSurgerySetByInstrumentalOnly(baseSent:String) -> NSDictionary? {
        var surgeryTypeDic: NSDictionary?
        let arrayLists = CoreDataHelper.fetchFilterEntityString("Surgery", filter: "familyType == %@ && implantSet.implantesBase == 'NO APLICA'", value: baseSent)
        
        if let first = arrayLists.first, idSurgeryType = first.uuid as? Int {
            let array = CoreDataHelper.fetchFilterEntityNumber("Surgery", filter: "uuid == %@", value: idSurgeryType)
            for item in array{
                if let current = item as? Surgery, description = current.descripcionSetCirugia, code = current.familyType, uuid = current.uuid {
                    surgeryTypeDic = ["description":description, "code":code, "uuid":uuid]
                }
            }
        }
        return surgeryTypeDic
    }
    
    class func getSurgerySetByImplantOnly(baseSent:String) -> NSDictionary? {
        var surgeryTypeDic: NSDictionary?
        let arrayLists = CoreDataHelper.fetchFilterEntityString("Surgery", filter: "familyType == %@ && surgeryTypeSet.instrumentalBase == 'NO APLICA' ", value: baseSent)
        if let first = arrayLists.first, idSurgeryType = first.uuid as? Int {
            let array = CoreDataHelper.fetchFilterEntityNumber("Surgery", filter: "uuid == %@", value: idSurgeryType)
            for item in array{
                if let current = item as? Surgery, description = current.descripcionSetCirugia, code = current.familyType, uuid = current.uuid {
                    surgeryTypeDic = ["description":description, "code":code, "uuid":uuid]
                }
            }
        }
        return surgeryTypeDic
    }
    
    class func getElementsSetCirugiaId(nameDelegate:String) -> NSArray{
        let surgeryTypes: NSMutableArray = NSMutableArray()
        var surgeryTypeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Surgery))
        var setSetsImplantes = Set<String>()
        for item in array{
            surgeryTypeDic = NSMutableDictionary()
            if let current = item as? Surgery where current.familyType == nameDelegate {
                surgeryTypeDic!.setValue(current.descripcionSetCirugia, forKey: "description")
                surgeryTypeDic!.setValue(current.surgeryType, forKey: "surgeryType")
                surgeryTypeDic!.setValue(current.familyType, forKey: "code")
                surgeryTypeDic!.setValue(current.uuid?.description, forKey: "uuid")
                surgeryTypes.addObject(surgeryTypeDic!)
                if let base = current.surgeryTypeSet?.instrumentalBase {
                    setSetsImplantes.insert(base)
                }
                if let adicional1 = current.surgeryTypeSet?.adicional1 where adicional1 != "empty" {
                    setSetsImplantes.insert(adicional1)
                }
                if let adicional2 = current.surgeryTypeSet?.adicional2 where adicional2 != "empty" {
                    setSetsImplantes.insert(adicional2)
                }
                if let adicional3 = current.surgeryTypeSet?.adicional3 where adicional3 != "empty" {
                    setSetsImplantes.insert(adicional3)
                }
                if let adicional4 = current.surgeryTypeSet?.adicional4 where adicional4 != "empty" {
                    setSetsImplantes.insert(adicional4)
                }
            }
        }
        
        print("setSetsImplantes \(setSetsImplantes.count)")
        return surgeryTypes
    }
    
    class func getElementsFam(nameDelegate:String) -> NSArray{
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Surgery))
        var uniqueDic = [String:[String:String]]()
        for item in array {
            guard let current = item as? Surgery, type = current.surgeryType, fam = current.familyType, id = current.uuid else { continue }
            if type == nameDelegate {
                if let _ = uniqueDic[fam] { continue } else {
                    let surgeryTypeDic:[String:String] = ["description":fam, "detail": type, "code":"\(id)"]
                    uniqueDic[fam] = surgeryTypeDic
                }
            }
        }
        return [[String:String]](uniqueDic.values)
    }
    
    class func getAllElementsSurgery() -> NSArray{
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Surgery))
        var uniqueDic = [String:[String:String]]()
        
        for item in array {
            guard let current = item as? Surgery, type = current.surgeryType else{ continue }
            if let _ = uniqueDic[type] {} else {
                let surgeryTypeDic:[String:String] = ["description":type, "code": type]
                uniqueDic[type] = surgeryTypeDic
            }
        }
        return [[String:String]](uniqueDic.values)
    }
    
    class func getAllElementsSurgeryTypes() -> NSMutableArray{
        let surgeryTypes: NSMutableArray = NSMutableArray()
        var surgeryTypeDic: NSMutableDictionary?
        let array = ["CADERA", "RODILLA", "COLUMNA", "TRAUMA", "NEURO"]
        
        for item in array{
            surgeryTypeDic = NSMutableDictionary()
            surgeryTypeDic!.setValue(item, forKey: "description")
            surgeryTypeDic!.setValue(item, forKey: "code")
            surgeryTypes.addObject(surgeryTypeDic!)
            
        }
        
        return surgeryTypes
    }
    
    class func getElementById(number: Int) -> Surgery?{
        let array = CoreDataHelper.fetchFilterEntityNumber("Surgery", filter: "uuid == %@", value: number)
        return array.first as? Surgery
    }
    
    class func isThereInfoCatalog() -> Bool {
        return CoreDataHelper.isThereInfoCatalog("Surgery")
    }

    class func getScheduleBySetSystem(implantSystemCode: String?, success:((dData:NSData!)-> Void)) {
        //var inventory = true
        
        if let implantSystemCode = implantSystemCode {
            DataManager.syncronizeAgenda(implantSystemCode, success: success)
        }
        
        //return (inventory,[])
    }
    
    class func dummyData() -> NSMutableArray? {
        let schedule: NSMutableArray = NSMutableArray()
        let dictionary1: [String:String] = ["startDate":"2015-12-27","endDate":"2015-12-28","startTime":"10:30","endTime":"10:30"]
        let dictionary2: [String:String] = ["startDate":"2016-01-02","endDate":"2016-01-06","startTime":"14:30","endTime":"14:30"]
        let dictionary3: [String:String] = ["startDate":"2015-12-15","endDate":"2015-12-17","startTime":"21:00","endTime":"21:00"]
        let array = [dictionary1,dictionary2,dictionary3]
        
        let dateFormatterGeneral: NSDateFormatter = NSDateFormatter()
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        dateFormatterGeneral.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "HH:mm"
        
        for item in array{
            let dictionary = NSMutableDictionary()
            let arrayDictionary = item as NSDictionary!
            
            let date1: NSDate = dateFormatterGeneral.dateFromString(arrayDictionary.valueForKey("startDate") as! String)!
            let date2: NSDate = dateFormatterGeneral.dateFromString(arrayDictionary.valueForKey("endDate") as! String)!
            
            dictionary.setValue(date1, forKey: "startDate")
            dictionary.setValue(date2, forKey: "endDate")
            dictionary.setValue(arrayDictionary.valueForKey("startTime") as! String, forKey: "startTime")
            dictionary.setValue(arrayDictionary.valueForKey("endTime") as! String, forKey: "endTime")
            
            schedule.addObject(dictionary)
        }
        return schedule
    }
}

//
//  SurgeryTypeSet+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/4/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SurgeryTypeSet {

    @NSManaged var adicional1: String?
    @NSManaged var adicional2: String?
    @NSManaged var adicional3: String?
    @NSManaged var adicional4: String?
    @NSManaged var uuid: NSNumber?
    @NSManaged var instrumentalBase: String?
    @NSManaged var surgery: Surgery?

}

//
//  OrderSurgery+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/28/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension OrderSurgery {

    @NSManaged var billToId: String?
    @NSManaged var changedRepresentative: NSNumber?
    @NSManaged var comments: String?
    @NSManaged var customerName: String?
    @NSManaged var customerNumber: String?
    @NSManaged var doctorName: String?
    @NSManaged var folioOrder: String?
    @NSManaged var id: NSNumber?
    @NSManaged var implantSystem: String?
    @NSManaged var implantSystemAvailability: String?
    @NSManaged var implantSystemCode: String?
    @NSManaged var implantSystemRequired: NSNumber?
    @NSManaged var instrumentalSet: String?
    @NSManaged var instrumentalSetAvailability: String?
    @NSManaged var instrumentalSetCode: String?
    @NSManaged var instrumentalSetRequired: NSNumber?
    @NSManaged var otherCustomer: String?
    @NSManaged var otherCustomerNumber: String?
    @NSManaged var patientName: String?
    @NSManaged var paymentConditionId: String?
    @NSManaged var priceImplantSystem: NSNumber?
    @NSManaged var priceList: String?
    @NSManaged var priceListCode: String?
    @NSManaged var recordDateTime: NSDate?
    @NSManaged var salesRepresentative: String?
    @NSManaged var salesRepresentativeNumber: String?
    @NSManaged var shipmentType: String?
    @NSManaged var statusId: NSNumber?
    @NSManaged var surgeryDateTime: NSDate?
    @NSManaged var surgeryFamilyName: String?
    @NSManaged var surgeryFamilyNumber: String?
    @NSManaged var surgerySetName: String?
    @NSManaged var surgerySetNumer: String?
    @NSManaged var surgeryType: String?
    @NSManaged var surgeryTypeNumber: String?
    @NSManaged var technical: String?
    @NSManaged var technicalAvailability: String?
    @NSManaged var technicalNumber: String?
    @NSManaged var technicalRequired: NSNumber?
    @NSManaged var wasSent: NSNumber?
    @NSManaged var timestamp: NSDate?
    @NSManaged var billInfo: BillInformation?
    @NSManaged var depositInfo: DepositInformation?
    @NSManaged var destinationInfo: DestinationInformation?
    @NSManaged var patientInfo: PatientInformation?
    @NSManaged var pendings: Pending?

}

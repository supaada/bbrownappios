//
//  SurgeryTypeTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol SurgeryTypeDelegate {
    func userDidEnterSurgeryType(structVar:SurgeryTypeViewStruct?)
}

public protocol SurgeryRequestPendingDelegate {
    func addPending(pending:PendingEnum)
}

public protocol SurgeryRequestStatisticsDelegate {
    func addToStatistics()
}

public protocol DismissSurgeryRequestDelegate {
    func dismissSurgeryRequest()
}

enum SelectSegueIdentifier: String {
    case CatalogSegue = "goCatalogSelection", DateTimeSelectionSegue = "goDateTimeSelection", ContactSegue = "goContact2", CalendarScheduleSegue = "goCalendarSchedule", CatalogInseptionSegue = "goCatalogInseptionSegue", CalendarInseptionScheduleSegue = "goCalendarInseptionSchedule", CatalogTrinseptionSegue = "goCatalogTrinseptionSegue", CalendarScheduleTimeSegue = "goCalendarScheduleTime", DateTimeSelection3Segue = "goDateTimeSelection3Segue", Calendar3Segue = "goCalendarTrinseptionSegue", TimeSchedule = "goTimeSchedule", CatalogSetSegue = "goCatalogSetSegue", CalendarSetSegue = "goCalendarSetSegue", SearchOrdersSurgery = "goSearchOrdersSurgery", SearchOrdersContact = "goSearchOrdersContact"
    case None
}

enum SelectReason: String{
    case ReasonImplentSystem = "Sistema de Implante", ReasonInstrumentalSet = "Set de Instrumental", ReasonTechnical = "Técnico Instrumentista", ReasonUnacceptedPrice = "Costo no aceptado"
    case None
}
    
public enum CatalogPosition:Int {
    case IdSurgeryTypeCatalog = 0,IdSurgeryFamilyCatalog, IdSurgerySetCatalog, IdSystemsCatalog, IdInstrumentalsCatalog, IdTechsCatalog, IdSalesRepresentativeCatalog, IdPriceListCatalog, IdCustomerCatalog, IdDoctorCatalog, IdShipmentTypeCatalog, IdBillToCatalog, IdOtherCustomerCatalog, None
    public init(rawValue: Int){
        switch(rawValue){
        case IdSurgeryTypeCatalog.hashValue:
            self = IdSurgeryTypeCatalog
        case IdSurgeryFamilyCatalog.hashValue:
            self = IdSurgeryFamilyCatalog
        case IdSurgerySetCatalog.hashValue:
            self = IdSurgerySetCatalog
        case IdSystemsCatalog.hashValue:
            self = IdSystemsCatalog
        case IdInstrumentalsCatalog.hashValue:
            self = IdInstrumentalsCatalog
        case IdTechsCatalog.hashValue:
            self = IdTechsCatalog
        case IdSalesRepresentativeCatalog.hashValue:
            self = IdSalesRepresentativeCatalog
        case IdPriceListCatalog.hashValue:
            self = IdPriceListCatalog
        case IdCustomerCatalog.hashValue:
            self = IdCustomerCatalog
        case IdDoctorCatalog.hashValue:
            self = IdDoctorCatalog
        case IdShipmentTypeCatalog.hashValue:
            self = IdShipmentTypeCatalog
        case IdBillToCatalog.hashValue:
            self = IdBillToCatalog
        case IdOtherCustomerCatalog.hashValue:
            self = IdOtherCustomerCatalog
        default:
            self = None
        }
        
    }
}

class SurgeryTypeTableViewController: UITableViewController {
    //View Variables
    @IBOutlet weak var techSwitch: UISwitch!

    //EditMode Varibales
    var showOrderDetail: Bool?
    var showButtons: Bool?
    var firstTime: Bool = true
    var delegate: SurgeryTypeDelegate?
    
    //Navigation Variables
    var nextViewController: AnyObject?
    var prevViewController: AnyObject?
    
    //Struct Variable
    var structVar: SurgeryTypeViewStruct?
    
    //TableView Variables
    var tableRecords =  [1,7,1]
    
    // MARK: - Init View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        setValueSalesRepresentative()
        loadData()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        executeDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tableRecords.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableRecords[section]
    }
    
    // MARK: - Style Methods
    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func cellBackgroundChange(cell: UITableViewCell?) {
        let customColorView: UIView = UIView()
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell?.selectedBackgroundView =  customColorView;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cellBackgroundChange(cell)
        
        if let showOrderDetail = showOrderDetail where showOrderDetail == false && indexPath.section == 1{
            switch(indexPath.row){
            case 0:
                callSegue(.CatalogSegue, catalogId: .IdSurgeryTypeCatalog)
            case 1:
                if let _ = structVar?.surgeryFamilyTypeNumber {
                    callSegue(.CatalogSetSegue, catalogId: .IdSurgerySetCatalog)
                } else {
                    SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Primero debe seleccionar el Tipo de Cirugía")
                }
            case 4:
                if let _ = structVar?.surgerySetNumber {
                    callSegue(.CalendarSetSegue, catalogId: .IdSurgerySetCatalog)
                } else {
                    SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Primero debe seleccionar el Tipo de Cirugía")
                }
            default: break
            }
        }
    }
    
    func callSegue(segueIdentifier: SelectSegueIdentifier, catalogId: CatalogPosition){
        performSegueWithIdentifier(segueIdentifier.rawValue, sender: catalogId.hashValue)
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch (SelectSegueIdentifier.init(rawValue: segue.identifier ?? "None")) ?? .None{
        case .CatalogSegue:
            if let destination = segue.destinationViewController as? UINavigationController, let controller = destination.topViewController as? CatalogSelectionTableViewController, let catalogId:Int  = sender as? Int {
                let params = getByCatalog(catalogId)
                controller.idCatalog = params.id
                controller.code = params.code
                controller.filterString = params.filter
                controller.delegate = self
                controller.delegateTime = self
                controller.delegateCalendar = self
                controller.delegatePending = self
                controller.delegateStatistics = self
                controller.delegateDismiss = self
            }
        case .CalendarSetSegue:
            if let destination = segue.destinationViewController as? UINavigationController, let controller = destination.topViewController as? CalendarScheduleViewController {
                controller.idCatalog = CatalogPosition.IdSurgerySetCatalog.hashValue
                controller.code = structVar?.surgerySetNumber
                controller.delegateCalendar = self
                controller.delegate = self
                controller.delegatePending = self
                controller.delegateStatistics = self
                controller.delegateDismiss = self
            }
        case .CatalogSetSegue:
            if let destination = segue.destinationViewController as? UINavigationController, let controller = destination.topViewController as? CatalogTrinseptionTableViewController, let catalogId:Int  = sender as? Int {
                let params = getByCatalog(catalogId)
                controller.idCatalog = params.id
                controller.code = params.code
                controller.filterString = params.filter
                controller.delegate = self
                controller.delegateTime = self
                controller.delegateCalendar = self
                controller.delegatePending = self
                controller.delegateStatistics = self
                controller.delegateDismiss = self
            }
        case .SearchOrdersSurgery:
            let splitViewController: UISplitViewController = segue.destinationViewController as! UISplitViewController
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
            splitViewController.delegate = splitViewController
        default: break
        }
    }
    
    // MARK: - Actions
    
    @IBAction func setRequiredTechnicalTapped(sender: AnyObject) {
        structVar?.technicalRequired = techSwitch.on
    }
    // MARK: - Navigation Functions
    
    func initView(){
        setNavigationButtons()
        setSwipes()
        setValueSalesRepresentative()
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func setSwipes() {
        let swipeView = UISwipeGestureRecognizer(target: self, action: "respondToSwipe:")
        swipeView.direction = .Right
        navigationController?.navigationBar.addGestureRecognizer(swipeView)
        
        let secondSwipeView = UISwipeGestureRecognizer(target: self, action: "respondToSwipe:")
        secondSwipeView.direction = .Right
        tableView.addGestureRecognizer(secondSwipeView)
        
        let swipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeNext:")
        swipeViewNext.direction = .Left
        navigationController?.navigationBar.addGestureRecognizer(swipeViewNext)
        
        let secondSwipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeNext:")
        secondSwipeViewNext.direction = .Left
        tableView.addGestureRecognizer(secondSwipeViewNext)
    }
    
    func respondToSwipe(gesture:UISwipeGestureRecognizer){
        navigationController?.popViewControllerAnimated(true)
    }
    
    func respondToSwipeNext(gesture:UISwipeGestureRecognizer){
        goNext()
    }
    
    func loadData(){
        setValues()
        tableView.reloadData()
    }
    
    func executeDelegates(){
        if !firstTime {
            setSwitchVauesToStruct()
            delegate?.userDidEnterSurgeryType(structVar)
        }
    }
    
    func setNavigationButtons(){
        if let showBarButtons = showButtons where showBarButtons == true {
            let buttonItemGoNext: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "goNext")
            let buttonItemGoExit = UIBarButtonItem(image: UIImage(named: "ic_option_exit"), style: UIBarButtonItemStyle.Plain, target: self, action: "goExit")
            navigationItem.rightBarButtonItems = [buttonItemGoNext,buttonItemGoExit]
        }
    }
    
    func setValues(){
        setStructValue()
        setInReadMode()
    }
    
    func setStructValue(){
        if let structVarEdit = structVar {
            if let folio = structVarEdit.folioOrder where !folio.isEmpty {
                setValueFolioView(folio)
            }else {
                setValueFolio("0000000000")
            }
            setValueSurgeryFamily(structVarEdit.surgeryTypeName, name: structVarEdit.surgeryFamilyTypeName)
            setValueSurgerySet(structVarEdit.instrumentalSetCode, name: structVarEdit.instrumentalSetName)
            setValueSurgeryDateTimeView(structVarEdit.surgeryDateTime)
            setValueImplantSystemView(structVarEdit.implantSystemName, detail: structVarEdit.implantSystemAvailability, requiered: structVarEdit.implantSystemRequired)
            setValueInstrumentalSetView(structVarEdit.instrumentalSetName, detail: structVarEdit.instrumentalSetAvailability, requiered: structVarEdit.instrumentalSetRequired)
            setValueTechnicalView(structVarEdit.technicalName, detail: structVarEdit.technicalAvailability, requiered:  structVarEdit.technicalRequired)
            setValueSalesRepresentativeView(structVarEdit.salesRepresentativeName)
            setValuePriceListView(structVarEdit.priceListName, price: structVarEdit.priceImplantSystem)
            structVar = structVarEdit
        }else {
            structVar = SurgeryTypeViewStruct()
            structVar?.folioOrder = ""
            //structVar?.implantSystemRequired = true
            //structVar?.instrumentalSetRequired = true
            structVar?.technicalRequired = true
            
            if showOrderDetail == false && firstTime {
                if !Reachability.isConnectedToNetwork() {
                    SCLAlertView().showWarning("Sin Internet", subTitle: "No cuenta con conexión a Internet!")
                }
            }
        }
    }
    
    func setInReadMode(){
        if showOrderDetail == true {
            var indexPath = NSIndexPath(forRow: 0, inSection: 1)
            var cell = tableView.cellForRowAtIndexPath(indexPath)
            cell!.accessoryType = UITableViewCellAccessoryType.None
        
            indexPath = NSIndexPath(forRow: 1, inSection: 1)
            cell = tableView.cellForRowAtIndexPath(indexPath)
        
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            indexPath = NSIndexPath(forRow: 4, inSection: 1)
            cell = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            techSwitch.enabled = false
        }
    }
    
    func setSwitchVauesToStruct(){
        structVar?.technicalRequired = techSwitch.on
    }
    
    func goOut(){
        //let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //nextViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("SearchingTableViewController")
        performSegueWithIdentifier(SelectSegueIdentifier.SearchOrdersSurgery.rawValue, sender: nil)
    }
    
    func goNext(){
        if isValidFields() {
            goNextScreen()
        }
    }
    
    func goNextScreen(){
        displayMessageBeforeContinue()
    }
    
    func rejectOrderBy(reason: SelectReason){
        let alert: SCLAlertView = SCLAlertView()
        alert.addButton("Aceptar"){
            alert.hideView()
            self.dropOrderBy(reason)
        }
        alert.showError(GeneralData.AlertTitles.addOrderTitle, subTitle: "Se detendrá la Creación de la Solicitud por \(reason)", showDoneButton: false)
    }
    
    func dropOrderBy(reason: SelectReason){
        SwiftSpinner.show("Deteniendo Creación de la Solicitud por \(reason)")
        
        if structVar != nil && structVar?.folioOrder != nil {
            dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
                self.structVar?.statusId = GeneralData.Status.rejected
                SwiftSpinner.hide()
                self.goOut()
            }
        }else{
            SwiftSpinner.delay(seconds: 1.0, completion: {
                SwiftSpinner.hide()
                self.goOut()
            })
        }
    }
    
    func displayMessageBeforeContinue(){
        goContact()
        /*
        let alert: SCLAlertView = SCLAlertView()
        
        alert.addButton(GeneralData.TextButtons.accept, action: {
            alert.hideView()
            self.goContact()
        })
        
        alert.addButton(GeneralData.TextButtons.reject, action: {
            alert.hideView()
            //self.dropOrderBy(.ReasonUnacceptedPrice)
        })
        
        alert.showNotice(GeneralData.AlertTitles.addOrderTitle, subTitle: "¿La información de la Solicitud de Cirugía es correcta?", showDoneButton: false)*/
    }
    
    func goExit(){
        SCLAlertView().showConfirm(GeneralData.AlertTitles.addOrderTitle, subTitle: "Esta apunto de perder los datos asignados hasta ahora. ¿Desea continuar?", buttonAcceptTarget: self, buttonAcceptSelector: "goOut")
    }
    
    func goContact(){
        print("rep vent Sol \(structVar?.surgerySetNumber)")
        let employeeId = UIDevice.currentDevice().name
        let salesRepresentative = SalesRepresentative.getName(employeeId) ?? employeeId
        setValueSalesRepresentativeView(salesRepresentative)
        structVar?.salesRepresentativeNumber = employeeId
        structVar?.salesRepresentativeName = salesRepresentative

        if let nextViewController = nextViewController as? ContactRequestSurgeryTableViewController{
            nextViewController.showOrderDetail = false
            nextViewController.showButtons = true
            nextViewController.firstTime = true
            nextViewController.folioOrder = structVar?.folioOrder
            nextViewController.prevViewController = self
            
            navigationController?.pushViewController(nextViewController, animated: true)
        } else {
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            nextViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("ContactRequestSurgeryTableViewController")
            (nextViewController as? ContactRequestSurgeryTableViewController)?.prevViewController = self
        }
    }
    
    func getByCatalog(idCatalog: Int) -> (id: Int?, code:String?, filter:String?) {
        var code: String?
        var filter: String?
        switch(CatalogPosition.init(rawValue: idCatalog)) {
        case .IdSurgeryTypeCatalog:
            code = structVar?.surgeryTypeNumber
        case .IdSurgerySetCatalog:
            code = structVar?.surgeryFamilyTypeName
            filter = structVar?.surgeryTypeName
        case .IdSalesRepresentativeCatalog:
            code = structVar?.salesRepresentativeNumber
        default:break
        }

        return (idCatalog, code, filter)
    }
    
    func isValidFields() -> Bool{
        guard let _ = structVar?.surgeryTypeNumber else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Tipo de Cirugía")
            return false
        }
        guard let _ = structVar?.surgeryDateTime else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar La Fecha y Hora de Cirugía")
            return false
        }
        return true
    }
    
}

// Date Time Selection Delegate
extension SurgeryTypeTableViewController: DateTimeSelectionDelegate {
    func userDidSelectDateTime(date:String, time: String) {
        setValueSurgeryTime("\(date) \(time)")
    }
}


//Catalog Delegate
extension SurgeryTypeTableViewController : CatalogDelegate {
    func userDidSelectElement(idCatalog: Int, code: String, description: String, detail: AnyObject?) {
        switch(CatalogPosition.init(rawValue: idCatalog)) {
        case .IdSurgeryTypeCatalog:
            setValueSurgeryType(code, name: description)
        case .IdSurgeryFamilyCatalog:
            setValueSurgeryFamily(structVar?.surgeryTypeName, name: description)
        case .IdSurgerySetCatalog:
            setValueSurgerySet(code, name: description)
            setImplantSystem(code)
            setInstrumentalSet(code)
            setValuePriceList(code)
            

        default: break
        }
    }
}


//Calendar Selection Delegate
extension SurgeryTypeTableViewController: CalendarSelectionDelegate {
    func userDidSelectCalendar(selectedDate:String) {
        setValueSurgeryDateTime(selectedDate)
    }
}


//Surgery Request Pending Delegate
extension SurgeryTypeTableViewController: SurgeryRequestPendingDelegate {
    func addPending(pending: PendingEnum) {
        if let structVarC = structVar, pendingsC = structVarC.pendings {
            if !pendingsC.contains(pending) {
                structVar!.pendings!.append(pending)
            }
        } else {
            structVar!.pendings = [pending]
        }
    }
}


//Surgery Request Statistics Delegate
extension SurgeryTypeTableViewController: SurgeryRequestStatisticsDelegate {
    func addToStatistics() {
        structVar?.statistics = true
    }
}

//Dismiss Surgery Request Delegate
extension SurgeryTypeTableViewController: DismissSurgeryRequestDelegate {
    func dismissSurgeryRequest() {
        goOut()
    }
}

//Update Struct
extension SurgeryTypeTableViewController {
    
    
    func setValueFolio(folioOrder: String?){
        setValueFolioView(folioOrder)
        structVar?.folioOrder = folioOrder ?? ""
    }
    
    func setValueSurgerySet(number: String?, name: String?){
        if let id = Int(number!), let element = Surgery.getElementById(id) {
            //print("surgery number \(element.claveSetCirugia), name \(name)")
        setValueSurgerySetView(name)
        structVar?.surgeryTypeNumber = element.claveSetCirugia
        structVar?.surgeryFamilyTypeNumber = element.claveSetCirugia
        structVar?.surgerySetNumber = number
        structVar?.surgerySetName = name
        }
    }
    
    func setValueSurgeryType(number: String?, name: String?){
        setValueSurgeryTypeView(name)
        structVar?.surgeryTypeNumber = number
        structVar?.surgeryTypeName = name
    }
    
    func setValueSurgeryFamily(number: String?, name: String?){
        guard let surgeryType = number, family = name else {return}
        setValueSurgeryTypeView("\(surgeryType)  - \(family)")
        structVar?.surgeryFamilyTypeNumber = surgeryType
        structVar?.surgeryFamilyTypeName = name
    }
    
    func setValueSurgeryDateTime(dateTime: String?){
        guard let dateTime = dateTime else { return }
        setValueSurgeryDateTimeView(dateTime)
        structVar?.surgeryDateTime = dateTime
    }
    
    func setValueSurgeryTime(time: String?){
        guard let time = time else { return }
        setValueSurgeryTimeView(time)
        structVar?.surgeryDateTime = time
    }
    
    func setValueImplantSystem(code: String?, name: String?, detail: String?){
        setValueImplantSystemView(code, detail: name, requiered: nil)
        structVar?.implantSystemCode = code
        structVar?.implantSystemName = name
        structVar?.implantSystemAvailability = detail
    }
    
    func setValueImplantSystemView(name: String?, detail: String?, requiered: Bool?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 3, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = name

    }
    
    func setValueInstrumentalSet(code: String?, name: String?, detail: String?){
        setValueInstrumentalSetView(code, detail: name, requiered: nil)
        structVar?.instrumentalSetCode = code
        structVar?.instrumentalSetName = name
        structVar?.instrumentalSetAvailability = detail
    }
    
    func setImplantSystem(code: String){
        if let id = Int(code), let element = Surgery.getElementById(id) {
            var description: String? = (element.implantSet?.implantesBase)
            if let adicional1 = element.implantSet?.implantesAdicional1 where !adicional1.isEmpty, let desc = description {
                description = "\(desc) + \(adicional1)"
            }
            setValueImplantSystem(element.implantSet?.implantesBase, name: description, detail: GeneralData.Messages.available)
        }else{
            setEmptyValueImplantSystem()
        }
    }
    
    func setInstrumentalSet(code: String){
        if let id = Int(code), element = Surgery.getElementById(id) {
            var description: String? = element.surgeryTypeSet?.instrumentalBase
            if let adicional1 = element.surgeryTypeSet?.adicional1 where !adicional1.isEmpty, let desc = description {
                description = "\(desc) + \(adicional1)"
            }
            if let adicional2 = element.surgeryTypeSet?.adicional2 where !adicional2.isEmpty, let desc = description {
                description = "\(desc) + \(adicional2)"
            }
            if let adicional3 = element.surgeryTypeSet?.adicional3 where !adicional3.isEmpty, let desc = description {
                description = "\(desc) + \(adicional3)"
            }
            if let adicional4 = element.surgeryTypeSet?.adicional4 where !adicional4.isEmpty, let desc = description {
                description = "\(desc) + \(adicional4)"
            }
            setValueInstrumentalSet(description, name: description, detail: GeneralData.Messages.available)
        }else{
            setEmptyValueInstrumentalSet()
        }
    }
    
    func setValueSalesRepresentative() {
        let employeeId = UIDevice.currentDevice().name
        let salesRepresentative = SalesRepresentative.getName(employeeId) ?? employeeId
        setValueSalesRepresentativeView(salesRepresentative)
        structVar?.salesRepresentativeNumber = employeeId
        structVar?.salesRepresentativeName = salesRepresentative
        
    }
    
    func setValuePriceList(code: String?){
        if let cod = code, id = Int(cod), element = Surgery.getElementById(id) {
            let formatCurrency: NSNumberFormatter = NSNumberFormatter()
            let code: String? = element.uuid?.description
            let description: NSNumber? = element.precio
            setValuePriceListView(code, price: description)
            structVar?.priceListCode = code
            formatCurrency.numberStyle = .CurrencyStyle
            structVar?.priceListName = formatCurrency.stringFromNumber(description!)
            structVar?.priceImplantSystem = description
        }else{
            setEmptyValueInstrumentalSet()
        }
    }
    
    
}


//Empty View
extension SurgeryTypeTableViewController {
    
    func setEmptyValueInstrumentalSet(){
        setValueInstrumentalSetView(GeneralData.Messages.description, detail: GeneralData.Messages.availability, requiered: nil)
        structVar?.instrumentalSetCode = nil
        structVar?.instrumentalSetName = nil
        structVar?.instrumentalSetAvailability = nil
    }
    
    func setEmptyValueImplantSystem(){
        setValueImplantSystemView(GeneralData.Messages.description, detail: GeneralData.Messages.availability, requiered: nil)
        
        structVar?.implantSystemCode = nil
        structVar?.implantSystemName = nil
        structVar?.implantSystemAvailability = nil
    }
    
}


//Update View
extension SurgeryTypeTableViewController {
    
    func setValueFolioView(folioOrder: String?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = folioOrder ?? "0000000000"
    }
    
    func setValueSurgeryDateTimeView(dateTime: String?){
        guard let dateTime:String = dateTime else { return }
        let indexPath: NSIndexPath = NSIndexPath(forRow: 4, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = dateTime
    }
    
    func setValueSurgeryTimeView(time: String?){
        guard let time = time else { return }
        let indexPath: NSIndexPath = NSIndexPath(forRow: 4, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = time
    }
    
    
    func setValuePriceListView(name: String?, price: NSNumber?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 2)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        if let price = price {
            let formatCurrency: NSNumberFormatter = NSNumberFormatter()
            formatCurrency.numberStyle = .CurrencyStyle
            let p = formatCurrency.stringFromNumber(price)
            cell?.detailTextLabel?.text = p
        }else{
            cell?.detailTextLabel?.text = "$"
        }
    }
    
    func setValueTechnicalView(name: String?, detail: String?, requiered: Bool?){
        guard let requiered = requiered else { return }
        techSwitch.on = requiered
    }
    
    func setValueSalesRepresentativeView(name: String?){
        guard let name = name else { return }
        let indexPath: NSIndexPath = NSIndexPath(forRow: 6, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = name
    }
    
    func setValueInstrumentalSetView(name: String?, detail: String?, requiered: Bool?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 2, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = name ?? "NO APLICA"
        
    }
    
    func setValueSurgeryTypeView(name: String?){
        guard let name = name else { return }
        let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = name
    }
    
    func setValueSurgerySetView(name: String?){
        guard let name = name else { return }
        let indexPath: NSIndexPath = NSIndexPath(forRow: 1, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.detailTextLabel?.text = name
    }
}



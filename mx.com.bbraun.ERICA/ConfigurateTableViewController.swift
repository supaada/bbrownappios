//
//  ConfigurateTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class ConfigurateTableViewController: UITableViewController, UITextFieldDelegate, TimeIntervalScheduleDelegate {
    struct TableView {
        struct SegueIdentifiers {
            static let TimeIntervalScheduleSegue = "goTimeIntervalShedule2"
        }
    }
    
    @IBOutlet weak var txtNameServerIp: UITextField!
    @IBOutlet weak var txtPortServer: UITextField!
    @IBOutlet weak var lblIdUser: UILabel!
    @IBOutlet weak var lblNameUser: UILabel!
    @IBOutlet weak var txtTimeIntervalSyncCatalogs: UILabel!
    @IBOutlet weak var txtTimeIntervalSyncOrders: UILabel!

    var timeIntervalCatalogIndex: Int?
    var timeIntervalOrdersIndex: Int?
    let timeIntervals: Array<TimeInterval>! = TimeInterval.allMainTimeIntervals()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if section == 0 {
            return 1
        }else if section == 1 {
            return 2
        }else{
            return 2
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        if indexPath.section == 2 {
            // this is where you set your color view
            let customColorView: UIView = UIView()
            
            customColorView.backgroundColor = GeneralData.Appearance.thirdColor
            cell!.selectedBackgroundView =  customColorView;
            
            self.performSegueWithIdentifier(TableView.SegueIdentifiers.TimeIntervalScheduleSegue, sender: indexPath.row)
        }
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == TableView.SegueIdentifiers.TimeIntervalScheduleSegue {
            let controller: TimeIntervalScheduleTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! TimeIntervalScheduleTableViewController
            controller.idSynchronization = sender as? Int
            controller.delegate = self
            controller.timeIntervals = self.timeIntervals
            
            if controller.idSynchronization == 0 {
                controller.timeIntervalScheduleIndex = self.timeIntervalCatalogIndex
            }else{
                controller.timeIntervalScheduleIndex = self.timeIntervalOrdersIndex
            }
        }
    }
    
    // MARK: - Text
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    @IBAction func saveSettingsTapped(sender: AnyObject) {
        self.saveSettings()
    }
    
    // MARK: - Time Interval Delegate
    
    func userDidSelectTimeIntervalSyncCatalogs(timeIntervalIndex: Int){
        self.timeIntervalCatalogIndex = timeIntervalIndex
        self.txtTimeIntervalSyncCatalogs.text = self.timeIntervals[self.timeIntervalCatalogIndex!].title
    }
    
    func userDidSelectTimeIntervalSyncOrders(timeIntervalIndex: Int){
        self.timeIntervalOrdersIndex = timeIntervalIndex
        self.txtTimeIntervalSyncOrders.text = self.timeIntervals[self.timeIntervalOrdersIndex!].title
        
    }
    
    // MARK - Business Functions
    
    func initView(){
        txtNameServerIp.delegate = self
        txtPortServer.delegate = self
    }
    
    func loadData(){
        let nameServerIp = NSUserDefaults.standardUserDefaults().stringForKey("nameServerIp")
        let portServer = NSUserDefaults.standardUserDefaults().integerForKey("portServer")
        let timeIntervalCatalogIndex = NSUserDefaults.standardUserDefaults().integerForKey("timeIntervalCatalog")
        let timeIntervalOrdersIndex = NSUserDefaults.standardUserDefaults().integerForKey("timeIntervalOrders")
        let user = User.getUserInfo()
        
        if(nameServerIp != nil){
            txtNameServerIp.text = nameServerIp
        }
        
        if(portServer > 0){
            txtPortServer.text = String(portServer)
        }
        
        if user != nil {
            if user!.id.integerValue > 0 {
                lblIdUser.text = user!.id.stringValue
                lblIdUser.reloadInputViews()
            }
            
            if !user!.name.isEmpty {
                lblNameUser.text = user!.name
            }
        }
        
        if timeIntervalCatalogIndex >= 0 {
            self.timeIntervalCatalogIndex = timeIntervalCatalogIndex
        }else{
            self.timeIntervalCatalogIndex = 1
        }
        
        self.txtTimeIntervalSyncCatalogs.text = self.timeIntervals[self.timeIntervalCatalogIndex!].title
        
        if timeIntervalOrdersIndex >= 0 {
            self.timeIntervalOrdersIndex = timeIntervalOrdersIndex
        }else{
            self.timeIntervalOrdersIndex = 1
        }
        
        self.txtTimeIntervalSyncOrders.text = self.timeIntervals[self.timeIntervalOrdersIndex!].title
        
        self.tableView.reloadData()
    }
    
    func saveSettings(){
        let nameServerIp = txtNameServerIp.text
        let portServer   = txtPortServer.text
        
        // Verificamos que no esten vacios
        if(nameServerIp!.isEmpty){
            // Se muestra mensaje de error
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre o IP del Servidor")
            return
        }
        
        //if(portServer.isEmpty){
        // Se muestra mensaje de error
        //displayAlertMessage("Debe ingresar el Puerto del Servidor",nil)
        //return
        //}
        
        // Resguardamos los valores en el dispositivo
        NSUserDefaults.standardUserDefaults().setValue(nameServerIp, forKey: "nameServerIp")
        NSUserDefaults.standardUserDefaults().setValue(Int(portServer!), forKey: "portServer")
        NSUserDefaults.standardUserDefaults().setValue(self.timeIntervalCatalogIndex, forKey: "timeIntervalCatalog")
        NSUserDefaults.standardUserDefaults().setValue(self.timeIntervalOrdersIndex, forKey: "timeIntervalOrders")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        // Se envia mensaje en caso de exito
        SCLAlertView().showSuccess(GeneralData.AlertTitles.infoTitle, subTitle: "Ajustes guardados correctamente")
    }
}

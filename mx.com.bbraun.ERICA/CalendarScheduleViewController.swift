//
//  CalendarScheduleViewController.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 04/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol CalendarSelectionDelegate{
    func userDidSelectCalendar(date:String)
}

class CalendarScheduleViewController: UIViewController, UIPopoverPresentationControllerDelegate, RDVCalendarViewDelegate {
    struct TableView {
        struct Predicate {
            static let ContainsDate: String = "%@ >= startDate AND %@ <= endDate"
        }
        
        struct SegueIdentifiers {
            static let DetailScheduleSegue = "goDetailSchedule"
        }
        
        struct ViewControllers {
            static let ScheduleDetailView = "ScheduleTableViewController"
        }
    }
    
    var calendarView: RDVCalendarView?
    var clearsSelectionOnViewWillAppear: Bool = true
    
    var idCatalog: Int?
    var code: String?
    var name: String?
    var elements: NSMutableArray?
    var popoverViewController: ScheduleTableViewController?
    var delegate: DateTimeSelectionDelegate?
    var delegateCalendar: CalendarSelectionDelegate?
    var delegatePendings: RequestPendingsDelegate?
    var delegatePending: SurgeryRequestPendingDelegate?
    var delegateStatistics: SurgeryRequestStatisticsDelegate?
    var delegateDismiss: DismissSurgeryRequestDelegate?
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    var selectedDate:NSDate?
    var selectedDateString:String?
    var notAvaliable:Bool?
    var inventoryAvailable:Bool = true
    var elementsAgenda: NSMutableArray = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dispatch_sync(GDCUtil.GlobalUserInteractiveQueue) {
        loadData()
        initView()
        //}
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if clearsSelectionOnViewWillAppear {
            calendarView?.deselectDayCellAtIndex(calendarView!.indexForSelectedDayCell(), animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goTimeSchedule" {
            if let controller: DateTimeSelectionViewController = segue.destinationViewController as? DateTimeSelectionViewController {
                controller.isPopover = false
                controller.isOrder = true
                controller.delegate = delegate
                controller.datePrevString = selectedDateString
                controller.datePrev = selectedDate
                controller.delegatePending = delegatePending
                controller.delegateStatistics = delegateStatistics
                controller.delegateDismiss = delegateDismiss
            }
        }
    }
    
    // MARK: - Calendar
    func calendarView(calendarView: RDVCalendarView!, configureDayCell dayCell: RDVCalendarDayCell!, atIndex index: Int) {
        let dayCell: CalendarDayCell = dayCell as! CalendarDayCell
        
        if containsDate(calendarView.dateForIndex(index)) {
            dayCell.dotMarker?.hidden = false
        }
    }
    
    func calendarView(calendarView: RDVCalendarView!, didSelectCellAtIndex index: Int) {
        let dayCell: CalendarDayCell = calendarView.dayCellForIndex(index) as! CalendarDayCell

        if containsDate(calendarView.dateForIndex(index)) {
            notAvaliable = true
            let subElements: NSMutableArray = getElementsByDate(calendarView.dateForIndex(index))
            showScheduleDetailView(dayCell, elements: subElements)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            selectedDate = calendarView.dateForIndex(index)
            selectedDateString = dateFormatter.stringFromDate(selectedDate!)
        } else {
            notAvaliable = false
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if let date = calendarView.dateForIndex(index){
                selectedDateString = dateFormatter.stringFromDate(date)
                selectedDate = dateFormatter.dateFromString(selectedDateString!)
            } else {
                selectedDateString = dateFormatter.stringFromDate(NSDate())
                selectedDate = dateFormatter.dateFromString(selectedDateString!)
            }
            if let selectedDate = selectedDate where selectedDate.isLessThanDate(NSDate(timeInterval: -60*60*25, sinceDate: NSDate())) {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar una fecha a futuro")
            } else {
                dateFormatter.dateFormat = "dd/MM/yyyy"
                selectedDateString = dateFormatter.stringFromDate(selectedDate!)
                if let date = selectedDateString {
                    delegateCalendar?.userDidSelectCalendar(date)
                }
                performSegueWithIdentifier("goTimeSchedule", sender: nil)
            }
        }
    }
    
    func getStringDate(date: NSDate) -> String{
        let strDate: String = dateFormatter.stringFromDate(date)
        
        return strDate
    }
    
    // MARK: - Presentation Controller
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    // MARK: Business Functions

    func initView(){
        let applicationFrame: CGRect = view.frame
        
        calendarView = RDVCalendarView(frame: applicationFrame)
        updateCalendar()
        view.addSubview(calendarView!)

    }
    
    func updateCalendar() {
        
        calendarView!.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        calendarView!.separatorStyle = RDVCalendarViewDayCellSeparatorType.Horizontal
        calendarView!.backgroundColor = UIColor.whiteColor()
        calendarView!.currentDayColor = GeneralData.Appearance.thirdColor
        calendarView!.delegate = self
        
        let closeButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_cancel"), style: UIBarButtonItemStyle.Plain, target: self, action: "cancelTapped")
        
        let buttonItemAccept: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_today"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptItemTapped")
        navigationItem.rightBarButtonItem = buttonItemAccept
        navigationItem.leftBarButtonItem = closeButton
        
        calendarView!.registerDayCellClass(CalendarDayCell)
        
    }
    
    func cancelTapped() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func acceptItemTapped() {
        
        if let selectedDate = selectedDate where selectedDate.isLessThanDate(NSDate(timeInterval: -60*60*25, sinceDate: NSDate())) {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar una fecha a futuro")
            
        } else if notAvaliable  == true{
            let alert: SCLAlertView = SCLAlertView()
            
            alert.addButton("Pendiente Confirmar Agenda", action: {
                alert.hideView()
                self.delegatePending?.addPending(PendingEnum.InstrumentalSetAgendaConfirmation)
                self.goNext()
                SCLAlertView().showWarning("Alerta", subTitle: "Comunicate con el coordinador de ERICA para confirmar disponibilidad")
            })
            
            alert.addButton("Guardar para Estadística", action: {
                alert.hideView()
                self.delegateStatistics?.addToStatistics()
                self.goNext()
            })
            
            alert.addButton("Cambiar Fecha Cirugia", action: {
                alert.hideView()
            })
            
            alert.showNotice("Agenda No Disponible", subTitle: "¿Que desea hacer?", showDoneButton: false)
        } else {
            goNext()
        }

    }
    
    func goNext() {
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if let selectedDate = selectedDate {
            selectedDateString = dateFormatter.stringFromDate(selectedDate)
            delegateCalendar?.userDidSelectCalendar(selectedDateString!)
            performSegueWithIdentifier("goTimeSchedule", sender: nil)
        }
    }
    
    func loadData(){
        if Reachability.isConnectedToNetwork() {
            SwiftSpinner.show("Sincronizando Agenda e Inventario")
            dispatch_async(GDCUtil.GlobalUserInitiatedQueue) {
                
                Surgery.getScheduleBySetSystem(self.code){ (dData) -> Void in
                    let json = JSON(data:dData)
                    print("json load agenda \(json)")
                    
                    if let inventoryJson = json["listaInventarios"].array where inventoryJson.count > 0 {
                        self.inventoryAvailable = false
                    }
                    
                    if let agendaJson = json["listaAgendas"].array where agendaJson.count > 0 {
                        var instrumentalInventoryDictionary = [String:Set<String>]()
                        if let instrumentalInventory = json["listaInstrumentos"].array where  instrumentalInventory.count > 0 {
                            for item in instrumentalInventory {
                                if let claveSet = item["descripcionset"].string, descripcionSet = item["claveset"].string {
                                    if let _ = instrumentalInventoryDictionary[claveSet] {
                                        instrumentalInventoryDictionary[claveSet]!.insert(descripcionSet)
                                    } else {
                                        let newSet : Set<String> = [descripcionSet]
                                        instrumentalInventoryDictionary[claveSet] = newSet
                                    }
                                }
                            }
                        }
                        
                        let dateFormatterGeneral: NSDateFormatter = NSDateFormatter()
                        let dateFormatter: NSDateFormatter = NSDateFormatter()
                        dateFormatterGeneral.dateFormat = "yyyy-MM-dd"
                        dateFormatter.dateFormat = "HH:mm:ss"

                        var agendaInstrumental = [String:[String:Set<NSDate>]]()
                        var agendaDictionary = [String:[NSDate:[NSDictionary]]]()
                        
                        for item in agendaJson {
                            let date1 = dateFormatterGeneral.dateFromString(item["fechaInicio"].string ?? "2015-12-29")!
                            let date2 = dateFormatterGeneral.dateFromString(item["fechaFin"].string ?? "2016-01-05")!
                            
                            let startTime = item["horaInicio"].string ?? ""
                            let endTime = item["horaFin"].string ?? ""
                            let setId = item["descripcionSet"].string ?? ""
                            let setDesc = item["numSetInstrumental"].string ?? ""
                            
                            let dateFormat = NSDateFormatter()
                            var strStartDate: String = "\(startTime) del "
                            var strEndDate: String = "\(endTime) del "
                            
                            dateFormat.dateFormat = "d 'de' MMMM"
                            strStartDate += dateFormat.stringFromDate(date1)
                            strEndDate += dateFormat.stringFromDate(date2)
                            
                            if date1.isLessThanDate(date2) {
                                if let agendaI = agendaInstrumental[setId] {
                                    if let _ = agendaI[setDesc] {}else {
                                        let newDaySet = Set<NSDate>()
                                        agendaInstrumental[setId]![setDesc] = newDaySet
                                    }
                                }else {
                                    let newDaySet = Set<NSDate>()
                                    let newSet = [setDesc:newDaySet]
                                    agendaInstrumental[setId] = newSet
                                }
                                
                                var currentdate = date1
                                while currentdate.isLessThanDate(date2) || currentdate.isEqualDate(date2){
                                    agendaInstrumental[setId]![setDesc]!.insert(currentdate)

                                    if let agendaD = agendaDictionary[setId] {
                                        if let _ = agendaD[currentdate] { } else {
                                            let newDictionary = [NSDictionary]()
                                            agendaDictionary[setId]![currentdate] = newDictionary
                                        }
                                    } else {
                                        let newDictionary = [NSDictionary]()
                                        let newSet = [currentdate:newDictionary]
                                        agendaDictionary[setId] = newSet
                                    }
                                    let dictionary = ["startDate":currentdate, "endDate": currentdate, "startTime": strStartDate,"endTime": strEndDate, "setId":setId, "setDesc":setDesc]
                                    agendaDictionary[setId]?[currentdate]?.append(dictionary)

                                    currentdate = currentdate.addDays(1)
                                }
                                
                            }
                        }
                        
                        if agendaDictionary.count > 0  && instrumentalInventoryDictionary.count > 0 && agendaInstrumental.count > 0 {
                            for (intrumental, inventory) in instrumentalInventoryDictionary {
                                if let agendaInstrumentalIntrumental = agendaInstrumental[intrumental] {
                                    if inventory.count == 1 {
                                        for (_,events) in agendaDictionary[intrumental]! {
                                            for item in events {
                                                self.elementsAgenda.addObject(item)
                                            }
                                        }
                                    } else if inventory.count > 1  && agendaInstrumentalIntrumental.count > 1 {
                                        var intersections:Set<NSDate> = []
                                        for (setDesc, events) in agendaInstrumentalIntrumental {
                                            var intersection:Set<NSDate> = []
                                            for (otherDesc, otherEvents) in agendaInstrumentalIntrumental {
                                                if setDesc != otherDesc {
                                                    intersection = intersection.union(events.intersect(otherEvents))
                                                }
                                            }
                                            intersections = intersections.union(intersection)
                                        }
                                        for intersection in intersections {
                                            var intersectionCount = 0
                                            for (setDesc, events) in agendaInstrumentalIntrumental {
                                                if events.contains(intersection) {
                                                    for (otherDesc, otherEvents) in agendaInstrumentalIntrumental {
                                                        if setDesc != otherDesc && otherEvents.contains(intersection) {
                                                            intersectionCount++
                                                        }
                                                    }
                                                }
                                            }
                                            if intersectionCount == inventory.count {
                                                for item in agendaDictionary[intrumental]![intersection]! {
                                                    self.elementsAgenda.addObject(item)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        self.elementsAgenda = []
                        self.elements = []
                    }
                }
            /*elementsAgenda = []
            let dictionary2 = ["setId":"S4","setDesc":"S4-1","startDate":NSDate(timeIntervalSinceNow: 60*60*24*2), "endDate": NSDate(timeIntervalSinceNow: 60*60*24*7), "startTime" :  "12:00","endTime" : "12:00"]
            let dictionary3 = ["setId":"MOTOR","setDesc":"MOTOR-1","startDate":NSDate(timeIntervalSinceNow: 60*60*24*1), "endDate": NSDate(timeIntervalSinceNow: 60*60*24*6), "startTime" :  "09:00","endTime" : "09:00"]
            let dictionary4 = ["setId":"ADICIONAL LUMBAR","setDesc":"ADILUM-1","startDate":NSDate(timeIntervalSinceNow: 60*60*24*5), "endDate": NSDate(timeIntervalSinceNow: 60*60*24*8), "startTime" :  "12:00","endTime" : "12:00"]

            elementsAgenda.addObject(dictionary2)
            elementsAgenda.addObject(dictionary3)
            elementsAgenda.addObject(dictionary4)*/
            
                //print("errorMessage \(errorMessage)")
                //if errorMessage.isEmpty {
                    //dispatch_async(GDCUtil.GlobalMainQueue) {
                    //self.saveSynchronizeDate()
                    SwiftSpinner.delay(seconds: 3.0, completion: {
                        SwiftSpinner.hide()
                        self.elements = self.elementsAgenda
                        if self.inventoryAvailable {
                            SCLAlertView().showSuccess(GeneralData.AlertTitles.syncCatalogTitle, subTitle: "Sincronización SATISFACTORIA")
                        } else {
                            SCLAlertView().showWarning("Sin Componentes", subTitle: "Comuniquese con el coordinador ERICA para validar disponibilidad")
                            self.delegatePending?.addPending(PendingEnum.ComponentsInventoryConfirmation)
                        }
                        self.calendarView?.reloadData()
                    })
                    //}
                //}

            }
        } else {
            SCLAlertView().showWarning("Sin Internet", subTitle: "No se puede validar Agenda e Inventario")
            delegatePending?.addPending(PendingEnum.InstrumentalSetAgendaConfirmation)
            delegatePending?.addPending(PendingEnum.ComponentsInventoryConfirmation)
            delegatePending?.addPending(PendingEnum.OffLine)
        }
    }
    
    func containsDate(date: NSDate) -> Bool{
        let elements: NSMutableArray = getElementsByDate(date)
        if elements.count > 0 {
            return true
        }
        return false
    }
    
    func getElementsByDate(date: NSDate) -> NSMutableArray{
        let predicate: NSPredicate? = NSPredicate(format: TableView.Predicate.ContainsDate, date, date)
        if let elements = elements {
        return  NSMutableArray(array: elements.filteredArrayUsingPredicate(predicate!))
        }else {
            return []
        }
    }
    
    func showScheduleDetailView(dayCell: CalendarDayCell, elements: NSMutableArray){
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        popoverViewController = mainStoryBoard.instantiateViewControllerWithIdentifier(TableView.ViewControllers.ScheduleDetailView) as? ScheduleTableViewController
        
        popoverViewController?.preferredContentSize = CGSizeMake(350,200)
        popoverViewController?.modalPresentationStyle = UIModalPresentationStyle.Popover
        popoverViewController?.popoverPresentationController!.delegate = self
        
        popoverViewController?.popoverPresentationController!.sourceRect = CGRectMake(20,20,0,0)
        popoverViewController?.popoverPresentationController!.sourceView = dayCell
        popoverViewController?.code = code
        popoverViewController?.name = name
        popoverViewController?.elements = elements
        
        presentViewController(popoverViewController!, animated: true, completion: nil)
    }
}

//
//  PaymentCondition.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit


class PaymentCondition {
    struct PaymentConditionStruct {
        static let elements: [PaymentCondition] = [ PaymentCondition(id: 1, title: "A Crédito", code: "CRD"),
            PaymentCondition(id: 2, title: "De Contado", code: "CON") ]
    }
    
    let id: Int
    let title: String
    let code: String
    
    init(id: Int, title: String, code: String) {
        self.id = id
        self.title = title
        self.code = code
    }
    
    class func allPaymentConditionTypes() -> Array<PaymentCondition> {
        return PaymentConditionStruct.elements
    }
    
    class func getPaymentConditionByCode(code: String) -> PaymentCondition? {
        var paymentCondition: PaymentCondition?
        
        for item in PaymentConditionStruct.elements{
            if item.code == code{
                paymentCondition = item as PaymentCondition
            }
        }
        
        return paymentCondition
    }
    
    class func getPaymentConditionById(id: String) -> PaymentCondition? {
        var paymentCondition: PaymentCondition?
        
        for item in PaymentConditionStruct.elements{
            if item.id == Int(id){
                paymentCondition = item as PaymentCondition
            }
        }
        
        return paymentCondition
    }
}
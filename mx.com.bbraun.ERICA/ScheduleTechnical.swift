//
//  ScheduleTechnical.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(ScheduleTechnical)
class ScheduleTechnical: NSManagedObject {
    struct ScheduleTechnicalProperties {
        static let idTechnicalFilter = "technical.number == %@"
    }

    @NSManaged var endDateTime: NSDate
    @NSManaged var startDateTime: NSDate
    @NSManaged var technical: TechnicalInstrumentalist

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ScheduleTechnical))
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        for item in array {
            var arrayString =  (item as! String).characters.split {$0 == "|"}.map { String($0) }
            let technical: TechnicalInstrumentalist? = TechnicalInstrumentalist.getElementById(arrayString[0])
            
            if technical != nil {
                let newRow: ScheduleTechnical = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleTechnical)) as! ScheduleTechnical
                
                newRow.technical = technical!
                newRow.startDateTime = dateFormatter.dateFromString(arrayString[1])!
                newRow.endDateTime = dateFormatter.dateFromString(arrayString[2])!
            }
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func saveData(){
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(TechnicalInstrumentalist))
        for item in array {
            let techinicalInstrumentalist: TechnicalInstrumentalist = item as! TechnicalInstrumentalist
            
            for index in 1...10 {
                let newRow: ScheduleTechnical = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleTechnical)) as! ScheduleTechnical
                newRow.technical = techinicalInstrumentalist
                newRow.startDateTime = NSDate()
                newRow.endDateTime = NSDate.addDays(NSDate(), daysToAdd: index)!
            }
        }
        CoreDataHelper.saveContext()
    }
    
    class func getScheduleByTechnical(instrumentalSetCode: String) -> NSMutableArray{
        let schedule: NSMutableArray = NSMutableArray()
        var row: ScheduleTechnical?
        var dictionary: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(ScheduleTechnical), filter: ScheduleTechnicalProperties.idTechnicalFilter, value: instrumentalSetCode)
        
        let dateFormatterGeneral: NSDateFormatter = NSDateFormatter()
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        dateFormatterGeneral.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "HH:mm"
        
        for item in array{
            dictionary = NSMutableDictionary()
            row = item as? ScheduleTechnical
            
            let strDate1: String = dateFormatterGeneral.stringFromDate(row!.startDateTime)
            let strDate2: String = dateFormatterGeneral.stringFromDate(row!.endDateTime)
            let date1: NSDate = dateFormatterGeneral.dateFromString(strDate1)!
            let date2: NSDate = dateFormatterGeneral.dateFromString(strDate2)!
            
            dictionary!.setValue(date1, forKey: "startDate")
            dictionary!.setValue(date2, forKey: "endDate")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.startDateTime), forKey: "startTime")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.endDateTime), forKey: "endTime")
            
            schedule.addObject(dictionary!)
        }
        
        return schedule
    }
}

//
//  ScheduleImplantSystem.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(ScheduleImplantSystem)
class ScheduleImplantSystem: NSManagedObject {
    struct ScheduleImplantSystemProperties {
        static let idImplantFilter = "implantSystem.code == %@"
    }

    @NSManaged var endDateTime: NSDate
    @NSManaged var startDateTime: NSDate
    @NSManaged var implantSystem: ImplantSystem

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ScheduleImplantSystem))
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        for item in array {
            var arrayString =  (item as! String).characters.split {$0 == "|"}.map { String($0) }
            let implantSystem: ImplantSystem? = ImplantSystem.getElementById(arrayString[0])
            
            if implantSystem != nil {
                let newRow: ScheduleImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleImplantSystem)) as! ScheduleImplantSystem
                
                newRow.implantSystem = implantSystem!
                newRow.startDateTime = dateFormatter.dateFromString(arrayString[1])!
                newRow.endDateTime = dateFormatter.dateFromString(arrayString[2])!
            }
            
        }
        
        CoreDataHelper.saveContext()
    }
   
    class func saveData(){
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(ImplantSystem))
        for item in array {
            let implantSystem: ImplantSystem = item as! ImplantSystem
            
            for index in 1...10 {
                let newRow: ScheduleImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleImplantSystem)) as! ScheduleImplantSystem
                newRow.implantSystem = implantSystem
                newRow.startDateTime = NSDate()
                newRow.endDateTime = NSDate.addDays(NSDate(), daysToAdd: index)!
            
            }
            
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func getScheduleByImplantSystem(implantSystemCode: String) -> NSMutableArray{
        let schedule: NSMutableArray = NSMutableArray()
        var row: ScheduleImplantSystem?
        var dictionary: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(ScheduleImplantSystem), filter: ScheduleImplantSystemProperties.idImplantFilter, value: implantSystemCode)
        
        let dateFormatterGeneral: NSDateFormatter = NSDateFormatter()
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        dateFormatterGeneral.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "HH:mm"
        
        for item in array{
            dictionary = NSMutableDictionary()
            row = item as? ScheduleImplantSystem
            
            let strDate1: String = dateFormatterGeneral.stringFromDate(row!.startDateTime)
            let strDate2: String = dateFormatterGeneral.stringFromDate(row!.endDateTime)
            let date1: NSDate = dateFormatterGeneral.dateFromString(strDate1)!
            let date2: NSDate = dateFormatterGeneral.dateFromString(strDate2)!
            
            dictionary!.setValue(date1, forKey: "startDate")
            dictionary!.setValue(date2, forKey: "endDate")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.startDateTime), forKey: "startTime")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.endDateTime), forKey: "endTime")
            
            schedule.addObject(dictionary!)
        }
        
        return schedule
    }
}

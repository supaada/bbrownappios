//
//  BillTo.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit


class BillTo {
    struct BillToStruct {
        static let elements: [BillTo] = [ BillTo(id: 1, title: "Mismo Hospital", code: "MHP"),
            BillTo(id: 2, title: "Venta Mostrador", code: "PVM"),
            BillTo(id: 3, title: "Otro Cliente", code: "AOC") ]
    }
    
    let id: Int
    let title: String
    let code: String
    
    init(id: Int, title: String, code: String) {
        self.id = id
        self.title = title
        self.code = code
    }
    
    class func allBillToTypes() -> Array<BillTo> {
        return BillToStruct.elements
    }
    
    class func getBillToByCode(code: String) -> BillTo? {
        var billTo: BillTo?
        
        for item in BillToStruct.elements{
            if item.code == code{
                billTo = item as BillTo
            }
        }
        
        return billTo
    }
    
    class func getBillToById(id: String) -> BillTo? {
        var billTo: BillTo?
        
        for item in BillToStruct.elements{
            if item.id == Int(id){
                billTo = item as BillTo
            }
        }
        
        return billTo
    }
}
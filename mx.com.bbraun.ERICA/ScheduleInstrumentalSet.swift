//
//  ScheduleInstrumentalSet.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(ScheduleInstrumentalSet)
class ScheduleInstrumentalSet: NSManagedObject {
    struct ScheduleInstrumentalSetProperties {
        static let idInstrumentalFilter = "instrumentalSet.code == %@"
    }

    @NSManaged var endDateTime: NSDate
    @NSManaged var startDateTime: NSDate
    @NSManaged var instrumentalSet: InstrumentalSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ScheduleInstrumentalSet))
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        for item in array {
            var arrayString =  (item as! String).characters.split {$0 == "|"}.map { String($0) }
            let instrumentalSet: InstrumentalSet? = InstrumentalSet.getElementById(arrayString[0])
            
            if instrumentalSet != nil {
                let newRow: ScheduleInstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleInstrumentalSet)) as! ScheduleInstrumentalSet
                
                newRow.instrumentalSet = instrumentalSet!
                newRow.startDateTime = dateFormatter.dateFromString(arrayString[1])!
                newRow.endDateTime = dateFormatter.dateFromString(arrayString[2])!
            }
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func saveData(){
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(InstrumentalSet))
        for item in array {
            let instrumentalSet: InstrumentalSet = item as! InstrumentalSet
            
            for index in 1...10 {
                let newRow: ScheduleInstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(ScheduleInstrumentalSet)) as! ScheduleInstrumentalSet
                newRow.instrumentalSet = instrumentalSet
                newRow.startDateTime = NSDate()
                newRow.endDateTime = NSDate.addDays(NSDate(), daysToAdd: index)!
            }
        }
        CoreDataHelper.saveContext()
    }
    
    class func getScheduleByInstrumentalSet(instrumentalSetCode: String) -> NSMutableArray{
        let schedule: NSMutableArray = NSMutableArray()
        var row: ScheduleInstrumentalSet?
        var dictionary: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(ScheduleInstrumentalSet), filter: ScheduleInstrumentalSetProperties.idInstrumentalFilter, value: instrumentalSetCode)
        
        let dateFormatterGeneral: NSDateFormatter = NSDateFormatter()
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        dateFormatterGeneral.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "HH:mm"
        
        for item in array{
            dictionary = NSMutableDictionary()
            row = item as? ScheduleInstrumentalSet
            
            let strDate1: String = dateFormatterGeneral.stringFromDate(row!.startDateTime)
            let strDate2: String = dateFormatterGeneral.stringFromDate(row!.endDateTime)
            let date1: NSDate = dateFormatterGeneral.dateFromString(strDate1)!
            let date2: NSDate = dateFormatterGeneral.dateFromString(strDate2)!
            
            dictionary!.setValue(date1, forKey: "startDate")
            dictionary!.setValue(date2, forKey: "endDate")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.startDateTime), forKey: "startTime")
            dictionary!.setValue(dateFormatter.stringFromDate(row!.endDateTime), forKey: "endTime")
            
            schedule.addObject(dictionary!)
        }
        
        return schedule
    }
}

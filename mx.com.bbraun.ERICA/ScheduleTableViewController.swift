//
//  ScheduleTableViewController.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 04/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class ScheduleTableViewController: UITableViewController {
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "ScheduleCell"
        }
    }
    
    @IBOutlet weak var labelTitle: UILabel!

    var code: String?
    var name: String?
    var elements: NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements?.count ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ScheduleCell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) as! ScheduleCell
        let elementDic: NSDictionary = self.elements?.objectAtIndex(indexPath.row) as! NSDictionary
        let dateInit: NSDate = elementDic.valueForKey("startDate") as! NSDate
        let dateEnd: NSDate = elementDic.valueForKey("endDate") as! NSDate
        let timeInit: String = elementDic.valueForKey("startTime") as! String
        let timeEnd: String = elementDic.valueForKey("endTime") as! String
        let idSet: String = elementDic.valueForKey("setId") as! String
        let descriptionSet: String = elementDic.valueForKey("setDesc") as! String
        cell.configureCell(dateInit, timeInit: timeInit, dateEnd: dateEnd, timeEnd: timeEnd, idSet: idSet, descriptionSet: descriptionSet)

        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 64.0
    }

    func initView(){
        tableView.tableFooterView = UIView(frame: CGRectZero)
        //if let code = code, codeNumber = Int(code), surgery:Surgery = Surgery.getElementById(codeNumber) {
            labelTitle.text = "Lista Eventos:"// \(surgery.descripcionSetCirugia)"
        //}
    }
    
    func loadData(){
        if self.elements != nil && self.code != nil {
            self.tableView.reloadData()
        }
    }
    
    func getStringDate(date: NSDate) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.stringFromDate(date)
    }
}

class ScheduleCell: UITableViewCell{
    @IBOutlet weak var labelDateInit: UILabel!
    @IBOutlet weak var labelDateEnd: UILabel!
    @IBOutlet weak var labelSet: UILabel!
    
    func configureCell(dateInit: NSDate, timeInit: String, dateEnd: NSDate, timeEnd: String, idSet: String, descriptionSet: String){
        labelSet.text = "\(idSet), \(descriptionSet)"
        labelDateInit.text = "Desde \(timeInit)"
        labelDateEnd.text = "Hasta \(timeEnd)"
    }
    
    func getStringDate(date: NSDate, time: String) -> String{
        let dateFormatter = NSDateFormatter()
        var strFecha: String = "\(time) del "
        
        dateFormatter.dateFormat = "d 'de' MMMM"
        strFecha += dateFormatter.stringFromDate(date)
        
        return strFecha
    }
}

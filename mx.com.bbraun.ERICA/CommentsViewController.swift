//
//  CommentsViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol CommentsDelegate{
    func userDidEnteredComments(comments: String)
}

class CommentsViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var textComments: UITextView!

    var showOrderDetail: Bool?
    var comments: String?
    var delegate: CommentsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITextView
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textComments.textColor == UIColor.lightGrayColor() {
            textComments.text = nil
            textComments.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textComments.text.isEmpty {
            textComments.text = "Ingrese sus observaciones"
            textComments.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if self.showOrderDetail == true{
            return false
        }
        
        return true
    }
    
    // MARK: - Button Actions
    
    @IBAction func closeTapped(sender: AnyObject) {
        self.goOut()
    }
    
    func acceptTapped() {
        self.acceptComments()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.setNavigationButtons()
        
        textComments.delegate = self
        
        textComments.text = "Ingrese sus observaciones"
        textComments.textColor = UIColor.lightGrayColor()
    }
    
    func setNavigationButtons(){
        if self.showOrderDetail == false {
            let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptTapped")
            
            self.navigationItem.rightBarButtonItems = [buttonItem]
        }
    }
    
    func loadData(){
        self.setValues()
    }
    
    func setValues(){
        self.setStructValue()
        self.setInReadMode()
    }
    
    func setStructValue(){
        if self.comments != nil {
            self.textComments.text = self.comments
        }
    }
    
    func setInReadMode(){
        if self.showOrderDetail == true {
            self.textComments.editable = false
        }
    }
    
    func acceptComments(){
        if self.didEnteredComments() {
            self.delegate?.userDidEnteredComments(self.textComments.text)
            self.goOut()
        }
    }
    
    func didEnteredComments() -> Bool {
        if self.textComments.text.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar alguna observación")
            return false
        }
        
        return true
    }
    
    func goOut(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

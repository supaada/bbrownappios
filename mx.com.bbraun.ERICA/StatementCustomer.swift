//
//  StatementCustomer.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(StatementCustomer)
class StatementCustomer: NSManagedObject {
    struct StatementCustomerProperties {
        static let idFilterCustomerNumber = "customer.customerNumber == %@"
    }
    
    @NSManaged var assignmetNumber: String
    @NSManaged var currencyUpdate: String
    @NSManaged var daysDelayAfeterDue: String
    @NSManaged var docReferenciaNumber: String
    @NSManaged var expirationDate: NSDate
    @NSManaged var invoice: String
    @NSManaged var postingDateDoc: NSDate
    @NSManaged var updateAmount: NSNumber
    @NSManaged var customer: Customer
    
    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(StatementCustomer))
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        for item in array {
            //            var arrayString =  split(item as! String) {$0 == "|"}
            let dictionary = item as! NSDictionary
            
            let temp = dictionary["postingDateDoc"] as! String
            let customer: Customer? = Customer.getElementById(dictionary["customerName"] as! String)
            
            if let cust = customer,
                let invoice = dictionary["invoice"] as? String,
                let docReferenciaNumber = dictionary["docReferenciaNumber"] as? String,
                let assignmentNumber = dictionary["assignmetNumber"] as? String,
                let amount = dictionary["updateAmount"] as? String,
                let concurrency =  dictionary["currencyUpdate"] as? String ,
                let dF = dateFormatter.dateFromString(temp),
                let expiration = dictionary["expirationDate"] as? String {
                    
                    let newRow: StatementCustomer = CoreDataHelper.getRowEntity(NSStringFromClass(StatementCustomer)) as! StatementCustomer
                    dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                    //print("Validar estado de Cuenta")
                    newRow.customer = cust
                    newRow.invoice = invoice
                    newRow.docReferenciaNumber = docReferenciaNumber
                    newRow.assignmetNumber = assignmentNumber
                    newRow.updateAmount = NSNumber(double: amount.doubleValue)
                    newRow.currencyUpdate = concurrency
                    newRow.postingDateDoc = dF
                    //print("expiration\(expiration)")
                    if expiration.rangeOfString("Contado") != nil {
                    
                    }else{
                        if let exp =  NSDate.addDays(dF, daysToAdd: Int(expiration)!){
                            newRow.expirationDate = exp
                        }
                    }
                    newRow.daysDelayAfeterDue = dictionary["daysDelayAfeterDue"] as! String
                    CoreDataHelper.saveContext()
            }
        }
    }
    
    class func getAllElements() -> NSMutableArray{
        let statementCustomers: NSMutableArray = NSMutableArray()
        var statementCustomerDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(StatementCustomer))
        
        for item in array{
            statementCustomerDic = NSMutableDictionary()
            
            statementCustomerDic!.setValue((item as! StatementCustomer).customer.name, forKey: "description")
            statementCustomerDic!.setValue((item as! StatementCustomer).invoice, forKey: "code")
            statementCustomerDic!.setValue((item as! StatementCustomer).docReferenciaNumber, forKey: "detail")
            statementCustomerDic!.setValue(false, forKey: "flag")

            statementCustomers.addObject(statementCustomerDic!)
        }
        
        return statementCustomers
    }
    
    class func getAllElementsByCustomer(customerNumber: String) -> NSMutableArray{
        let statementCustomers: NSMutableArray = NSMutableArray()
        var statementCustomer: StatementCustomer?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(StatementCustomer), filter: StatementCustomerProperties.idFilterCustomerNumber, value: customerNumber)
        
        for item in array{
            statementCustomer = item as? StatementCustomer
            
            statementCustomers.addObject(statementCustomer!)
        }
        
        return statementCustomers
    }
}

//
//  ConfiguracionWS.swift
//  SFVM
//
//  Created by Administrador Prospectiva on 13/03/15.
//  Copyright (c) 2015 Prospectiva en Tecnologia e Integradora de Sistemas SA de CV. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class ConfigWS {
    var SOAP_ACTION = ""
    
    let NAMESPACE                = "http://webservice.mobilep.com.mx"
    let WS_CONTEXTO              = "MobilepWS/services"
    let CATALOGO_WS_NOMBRE       = "ClienteWS"
    let PEDIDO_WS_NOMBRE         = "PedidoWS"
    let INVENTARIO_WS_NOMBRE     = "InventarioWS";
    let TRACKING_WS_NOMBRE       = "TrackingWS";
    let D_SALDO_WS_NOMBRE        = "DetalleSaldoWS";
    let ERROR_NETWORK_CONNECTION = "ERROR: No hay conexión a Internet"
    
    var URL                      = "";
    
    //protected String[] _propertyNames;
    //protected Object[] _propertyValues;
    //protected Activity contexto;
    
    func getUrl(wsName : String) -> String{
        let nameServerIp: String? = NSUserDefaults.standardUserDefaults().stringForKey("nameServerIp")
        let portServer: Int? = NSUserDefaults.standardUserDefaults().integerForKey("portServer")
        
        if nameServerIp == nil && portServer == 0{
            let url: String = "http://localhost:8080/" + WS_CONTEXTO + "/\(wsName)"
            return url;
        }else if nameServerIp != nil && portServer == 0 {
            let url: String = "http://\(nameServerIp):8080/" + WS_CONTEXTO + "/\(wsName)"
            return url;
        }else if nameServerIp == nil && portServer > 0 {
            let url: String = "http://localhost:\(portServer)/" + WS_CONTEXTO + "/\(wsName)"
            return url;
        }else if nameServerIp != nil && portServer > 0 {
            let url: String = "http://\(nameServerIp!):\(portServer!)/" + WS_CONTEXTO + "/\(wsName)"
            return url;
        }
        
        return "";
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        return true
    }
}

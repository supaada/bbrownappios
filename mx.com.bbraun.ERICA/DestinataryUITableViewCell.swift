//
//  DestinataryUITableViewCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/5/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class DestinataryUITableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var shipmentTypeLabel: UILabel!
    @IBOutlet weak var destinataryLabel: UILabel!
    
    func config(doctorName:String?, shipmentType:String?, destinatary:String?, destinataryId:String?){
        doctorNameLabel.text = doctorName
        shipmentTypeLabel.text = shipmentType
        destinataryLabel.text = destinatary
    }
}

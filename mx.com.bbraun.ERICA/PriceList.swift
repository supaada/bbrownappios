//
//  PriceList.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 27/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(PriceList)
class PriceList: NSManagedObject {
    struct PriceListProperties {
        static let idFilter = "code == %@"
    }

    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var priceListImplantSystem: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(PriceList))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            //var arrayString =  split(item as! String) {$0 == "|"}
            let dictionary = item as! NSDictionary
            let newRow: PriceList = CoreDataHelper.getRowEntity(NSStringFromClass(PriceList)) as! PriceList
            
            newRow.code = dictionary["code"] as! String
            newRow.name = dictionary["name"] as! String
        }
        CoreDataHelper.saveContext()
       // print("Price List")

    }
    
    class func getElementById(code: String) -> PriceList?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(PriceList), filter: PriceListProperties.idFilter, value: code)
        let element: PriceList? = array.last as? PriceList
        
        return element
    }
    
    class func getAllElementsT() -> NSArray{
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(PriceList))
        return array
    }
}

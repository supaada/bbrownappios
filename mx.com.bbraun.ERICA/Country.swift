//
//  Country.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(Country)
class Country: NSManagedObject {
    struct CountryProperties {
        static let idFilter = "code == %@"
        static let nameFilter = "name CONTAINS %@"
        static let stateNameFilter = "state.name CONTAINS %@"
    }
    class CountryDic:NSObject {
        init(code : String, name : String) {
            self.code = code
            self.name = name
        }
        var code: String
        var name: String
        
        override func isEqual(object: AnyObject?) -> Bool {
            if let object = object as? CountryDic {
                return code == object.code
            } else {
                return false
            }
        }
        
        override var hash: Int {
            return code.hashValue
        }
    }

    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var state: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(Country))
    }
    
    class func saveData(array: NSArray){
        let countries:NSMutableSet = NSMutableSet()
        for item in array {
            let dictionary = item as! NSDictionary
            let country:CountryDic = CountryDic(code: dictionary["countryCode"] as! String, name: dictionary["countryName"] as! String)
            countries.addObject(country)
        }
        for country in countries {
            saveInfo(country as! CountryDic)
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func saveInfo(row: CountryDic){
        let newRow: Country = CoreDataHelper.getRowEntity(NSStringFromClass(Country)) as! Country
        newRow.code = row.code
        newRow.name = row.name
    }
    
    class func getElementById(code: String) -> Country?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(Country), filter: CountryProperties.idFilter, value: code)
        let element: Country? = array.last as? Country
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Country))
        let countries: NSMutableArray = NSMutableArray()
        for itemC in array{
            let country: Country = itemC as! Country
            let countryDic: NSMutableDictionary = NSMutableDictionary()
            let states: NSMutableArray = NSMutableArray()
            
            countryDic.setValue(country.code, forKeyPath: "code")
            countryDic.setValue(country.name, forKeyPath: "name")
            
            for itemS in country.state{
                let state: State = itemS as! State
                let stateDic: NSMutableDictionary = NSMutableDictionary()
                
                stateDic.setValue(state.code, forKeyPath: "code")
                stateDic.setValue(state.name, forKeyPath: "name")
                
                states.addObject(stateDic)
            }
            
            countryDic.setValue(states, forKeyPath: "states")
            
            countries.addObject(countryDic)
        }

        return countries
    }
    
    class func getElementsByStateDescription(description: String) -> NSMutableArray{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(Country), filter: CountryProperties.stateNameFilter, value: description)
        let countries: NSMutableArray = NSMutableArray()
        let predicate: NSPredicate = NSPredicate(format: CountryProperties.nameFilter, description, description)
        
        for itemC in array{
            let country: Country = itemC as! Country
            let countryDic: NSMutableDictionary = NSMutableDictionary()
            let states: NSMutableArray = NSMutableArray()
            var state: NSSet?
            
            countryDic.setValue(country.code, forKeyPath: "code")
            countryDic.setValue(country.name, forKeyPath: "name")
            
            state = country.state.filteredSetUsingPredicate(predicate)
            
            if state != nil {
                for itemS in state!{
                    let state: State = itemS as! State
                    let stateDic: NSMutableDictionary = NSMutableDictionary()
                
                    stateDic.setValue(state.code, forKeyPath: "code")
                    stateDic.setValue(state.name, forKeyPath: "name")
                
                    states.addObject(stateDic)
                }
            
                if states.count > 0 {
                    countryDic.setValue(states, forKeyPath: "states")
                    countries.addObject(countryDic)
                }
            }
        }
        
        return countries
    }

}

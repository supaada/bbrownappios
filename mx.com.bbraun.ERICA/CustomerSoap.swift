//
//  CustomerSoap2.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 19/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class CustomerSoap: NSObject, SOAPHelperDelegate {
    let ACTION_WS_NAME = "obtenerClientes"
    let CLIENTE_WS_NANE = "ClienteWS"
    
    var result:NSDictionary?
    var errorMessage:String = ""
    
    func getCustomers(in0: String) {
        if !ConfigWS().isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        
        var error:NSError?
        var soap = SOAPHelper()
        let url: String = ConfigWS().getUrl(CLIENTE_WS_NANE)
        
        soap.delegate = self
        
        soap.setValueParameter(in0, forKey: "in0")
        
        var xmlDoc:AEXMLElement?
        do {
            xmlDoc = try soap.syncRequestURL(url,
                        soapAction: ACTION_WS_NAME)
        } catch var error1 as NSError {
            error = error1
            xmlDoc = nil
        }
        
        if let err = error {
            self.errorMessage = "Falló la conexion del Web Service (\(self.CLIENTE_WS_NANE)): \(err.localizedDescription)"
            
            print(err.localizedDescription)
        }else{
            if xmlDoc != nil && xmlDoc!.children.count > 0 {
                var element: AEXMLElement = xmlDoc!["ns1:out"]
                
                if element.name != AEXMLElement.errorElementName {
                    setResult(element)
                }else{
                    element = xmlDoc!["faultstring"]
                    
                    if element.name != AEXMLElement.errorElementName {
                        let response: String = element.stringValue
                        
                        self.errorMessage = "Falló la conexion del Web Service (\(self.CLIENTE_WS_NANE)): \(response)"
                    }else{
                        self.errorMessage = "Falló la conexion del Web Service (\(self.CLIENTE_WS_NANE)): \(AEXMLElement.errorElementName)"
                    }
                }
            }else{
                self.errorMessage = "Falló la conexion del Web Service (\(self.CLIENTE_WS_NANE)): Respuesta Nula o Vacia"
            }
        }
    }
    
    internal func setResult(element: AEXMLElement){
        let children = element.children
        let mutableArray: NSMutableArray = NSMutableArray()
        var array: NSArray?
        var dictionary: NSDictionary?
        let response: String = children.first!.children.first!.stringValue
        
        if ValidityUtil.isErrorValidityActivate(response) {
            self.errorMessage = ValidityUtil.getErroMessage(response)
        }else{
            for childArrayOfString in children {
                let arrayOfString: NSMutableArray = NSMutableArray()
                
                for childString in childArrayOfString.children {
                    arrayOfString.addObject(childString.stringValue)
                }
                
                let arrayString : NSArray = NSArray(array: arrayOfString)
                let arrayDictionary: NSDictionary = ["ns1:string" : arrayString]
                
                mutableArray.addObject(arrayDictionary)
            }
            
            array = NSArray(array: mutableArray)
            dictionary = ["ns1:ArrayOfString" : array!]
            result = ["ns1:out" : dictionary!]
        }
    }
    
    //SOAPHelperDelegate
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeSendingURLRequest request: NSMutableURLRequest) -> NSMutableURLRequest {
        //println(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))
        
        return request
    }
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeParsingResponseString data: NSData) -> NSData {
        //println(NSString(data: data, encoding: NSUTF8StringEncoding))
        
        return data
    }
}
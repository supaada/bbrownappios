//
//  GDCUtil.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class GDCUtil{
    class var GlobalMainQueue: dispatch_queue_t {
        return dispatch_get_main_queue()
    }
    
    class var GlobalUserInteractiveQueue: dispatch_queue_t {
        return dispatch_get_global_queue(Int(QOS_CLASS_USER_INTERACTIVE.rawValue), 0)
    }
    
    class var GlobalUserInitiatedQueue: dispatch_queue_t {
        return dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)
    }
    
    class var GlobalUtilityQueue: dispatch_queue_t {
        return dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.rawValue), 0)
    }
    
    class var GlobalBackgroundQueue: dispatch_queue_t {
        return dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)
    }
}
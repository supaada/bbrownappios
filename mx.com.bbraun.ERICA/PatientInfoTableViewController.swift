//
//  PatientInfoTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol PatientInfoDelegate {
    func userDidEnterPatientInfo(structVar: PatientInfoViewStruct?)
}

class PatientInfoTableViewController: UITableViewController, StateCountryDelegate {
    @IBOutlet weak var textPatientName: UITextField!
    @IBOutlet weak var textRfc: UITextField!
    @IBOutlet weak var textStreet: UITextField!
    @IBOutlet weak var textExtNumber: UITextField!
    @IBOutlet weak var textDeptoNumber: UITextField!
    @IBOutlet weak var textSuburb: UITextField!
    @IBOutlet weak var textZipCode: UITextField!
    @IBOutlet weak var textTown: UITextField!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var textEmail: UITextField!
    
    struct TableView {
        struct SegueIdentifiers {
            static let CreateOrderSegue = "goCreateOrder2"
            static let StateCountrySegue = "goStateCountry"
        }
    }
    
    var showOrderDetail: Bool?
    var showButtons: Bool?
    var firstTime: Bool = true
    var delegate: PatientInfoDelegate?
    
    //Para Lista
    var nextViewController: AnyObject?
    var prevViewController: AnyObject?
    
    //Estructura de la vista
    var structVar: PatientInfoViewStruct?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == TableView.SegueIdentifiers.CreateOrderSegue {
            let controller = segue.destinationViewController as! CreateRequestSurgeryTableViewController
            
            controller.surgeryTypeStruct = ((self.prevViewController as? ContactRequestSurgeryTableViewController)?.prevViewController as? SurgeryTypeTableViewController)?.structVar
            controller.contactRequestSurgeyStruct = (self.prevViewController as? ContactRequestSurgeryTableViewController)?.structVar
            controller.billInfo = self.structVar
            controller.showOrderDetail = false
        }
        
        if segue.identifier == TableView.SegueIdentifiers.StateCountrySegue {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! StateCountryTableViewController
            
            controller.countryCode = self.structVar?.countryCode
            controller.stateCode = self.structVar?.stateCode
            controller.delegate = self
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if showOrderDetail == true {
            return false
        }
        
        return true
    }
    
    // MARK: - Business Functions
    
    func initView(){
        setNavigationButtons()
        textPatientName.delegate = self
        textRfc.delegate = self
        textStreet.delegate = self
        textExtNumber.delegate = self
        textDeptoNumber.delegate = self
        textSuburb.delegate = self
        textZipCode.delegate = self
        textTown.delegate = self
        textEmail.delegate = self
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        setValues()
        tableView.reloadData()
    }
    
    func setNavigationButtons(){
        /*if self.firstTime {
            if self.showButtons == true {
                var buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_next"), style: UIBarButtonItemStyle.Plain, target: self, action: "goNextScreen")
            
                self.navigationItem.rightBarButtonItems = [buttonItem]
            
                buttonItem = UIBarButtonItem(image: UIImage(named: "ic_option_exit"), style: UIBarButtonItemStyle.Plain, target: self, action: "goExit")
            
                self.navigationItem.rightBarButtonItems?.append(buttonItem)
            }
        }else{*/
            //if showButtons == true {
                let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptInfo")
            
                self.navigationItem.rightBarButtonItems = [buttonItem]
        
                let buttonExit = UIBarButtonItem(image: UIImage(named: "ic_option_exit"), style: UIBarButtonItemStyle.Plain, target: self, action: "goExit")
        
                self.navigationItem.leftBarButtonItem = buttonExit
    }
    
    func setValues(){
        self.setStructValue()
        self.setInReadMode()
    }
    
    func setStructValue(){
        if self.structVar == nil {
            self.structVar = PatientInfoViewStruct()
        }else{
            self.setValuesFromStruct()
        }
    }
    
    func setValuesFromStruct(){
        self.setValuePatientNameView(self.structVar!.name)
        self.setValueTaxIdView(self.structVar!.taxId)
        self.setValueStreetView(self.structVar!.street)
        self.setValueBuildingNumberView(self.structVar!.buildingNumber)
        self.setValueApparmentView(self.structVar!.apparment)
        self.setValueLocalityView(self.structVar!.locality)
        self.setValuePostalCodeView(self.structVar!.postalCode)
        self.setValueTownView(self.structVar!.town)
        self.setValueStateView(self.structVar!.stateName)
        self.setValueCountryView(self.structVar!.countryName)
        self.setValueEmailView(self.structVar!.email)
    }
    
    func setValuePatientName(name: String?){
        self.setValuePatientNameView(name)
        
        self.structVar?.name = name
    }
    
    func setValuePatientNameView(name: String?){
        if name != nil {
            self.textPatientName.text = name
        }
    }
    
    func setValueTaxId(taxId: String?){
        self.setValueTaxIdView(taxId)
        
        self.structVar?.taxId = taxId
    }
    
    func setValueTaxIdView(taxId: String?){
        if taxId != nil {
            self.textRfc.text = taxId
        }
    }
    
    func setValueStreet(street: String?){
        self.setValueStreetView(street)
        
        self.structVar?.street = street
    }
    
    func setValueStreetView(street: String?){
        if street != nil {
            self.textStreet.text = street
        }
    }
    
    func setValueBuildingNumber(buildingNumber: String?){
        self.setValueBuildingNumberView(buildingNumber)
        
        self.structVar?.buildingNumber = buildingNumber
    }
    
    func setValueBuildingNumberView(buildingNumber: String?){
        if buildingNumber != nil {
            self.textExtNumber.text = buildingNumber
        }
    }
    
    func setValueApparment(apparment: String?){
        self.setValueApparmentView(apparment)
        
        self.structVar?.apparment = apparment
    }
    
    func setValueApparmentView(apparment: String?){
        if apparment != nil {
            self.textDeptoNumber.text = apparment
        }
    }
    
    func setValueLocality(locality: String?){
        self.setValueLocalityView(locality)
        
        self.structVar?.locality = locality
    }
    
    func setValueLocalityView(locality: String?){
        if locality != nil {
            self.textSuburb.text = locality
        }
    }
    
    func setValuePostalCode(postalCode: String?){
        self.setValuePostalCodeView(postalCode)
        
        self.structVar?.postalCode = postalCode
    }
    
    func setValuePostalCodeView(postalCode: String?){
        if postalCode != nil {
            self.textZipCode.text = postalCode
        }
    }
    
    func setValueTown(town: String?){
        self.setValueTownView(town)
        
        self.structVar?.town = town
    }
    
    func setValueTownView(town: String?){
        if town != nil {
            self.textTown.text = town
        }
    }
    
    func setValueState(code: String?, name: String?){
        self.setValueStateView(name)
        
        self.structVar?.stateCode = code
        self.structVar?.stateName = name
    }
    
    func setValueStateView(name: String?){
        if name != nil {
            self.labelState.text = name
        }
    }
    
    func setValueCountry(code: String?, name: String?){
        self.setValueCountryView(name)
        
        self.structVar?.countryCode = code
        self.structVar?.countryName = name
    }
    
    func setValueCountryView(name: String?){
        if name != nil {
            self.labelCountry.text = name
        }
    }
    
    func setValueEmail(email: String?){
        self.setValueEmailView(email)
        
        self.structVar?.email = email
    }
    
    func setValueEmailView(email: String?){
        if email != nil {
            self.textEmail.text = email
        }
    }
    
    func setInReadMode(){
        if showOrderDetail == true {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 6, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            self.textPatientName.enabled = false
            self.textRfc.enabled = false
            self.textStreet.enabled = false
            self.textExtNumber.enabled = false
            self.textDeptoNumber.enabled = false
            self.textSuburb.enabled = false
            self.textZipCode.enabled = false
            self.textTown.enabled = false
            self.textEmail.enabled = false
        }
    }
    
    func confirmSaveInfoPatient(){
        goOut()
    }
    
    func setTextFieldValuesToStruct(){
        self.structVar?.name = self.textPatientName.text
        self.structVar?.taxId = self.textRfc.text
        self.structVar?.street = self.textStreet.text
        self.structVar?.buildingNumber = self.textExtNumber.text
        self.structVar?.apparment = self.textDeptoNumber.text
        self.structVar?.locality = self.textSuburb.text
        self.structVar?.postalCode = self.textZipCode.text
        self.structVar?.town = self.textTown.text
        self.structVar?.email = self.textEmail.text
    }
    
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func goClose(){
        //print("delegate \(delegate)")
        delegate?.userDidEnterPatientInfo(structVar)
//        navigationController?.popViewControllerAnimated(true)
        goOut()
    }
    
    func goNextScreen(){
        if self.isValidFields() {
            SCLAlertView().showConfirm(GeneralData.AlertTitles.confirmTitle, subTitle: "¿Los datos fiscales son Correctos?", buttonAcceptTarget: self, buttonAcceptSelector: "goCreateOrder")
        }
    }
    
    func goExit(){
        SCLAlertView().showConfirm(GeneralData.AlertTitles.addOrderTitle, subTitle: "Esta apunto de perder los datos asignados hasta ahora. ¿Desea continuar?", buttonAcceptTarget: self, buttonAcceptSelector: "goOut")
    }
    
    func goCreateOrder(){
        self.performSegueWithIdentifier(TableView.SegueIdentifiers.CreateOrderSegue, sender: -1)
    }
    
    func acceptInfo(){
        if self.isValidFields() {
            SCLAlertView().showConfirm(GeneralData.AlertTitles.confirmTitle, subTitle: "¿Los datos fiscales son Correctos?", buttonAcceptTarget: self, buttonAcceptSelector: "goClose")
        }
    }
    
    func isValidFields() -> Bool{
        if self.textPatientName.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre / Razón Social")
            return false
        }
        
        if self.textRfc.text!.isEmpty || !GeneralData.containsRFCMatch(textRfc.text!){
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Estructura RFC: 3 o 4 letras, 6 números, 2 letras o números y 1 número")
            return false
        }
        
        if self.textStreet.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre de la Calle")
            return false
        }
        
        if self.textExtNumber.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el No. Exterior")
            return false
        }
        
        if self.textSuburb.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Colonia")
            return false
        }
        
        if self.textZipCode.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Código Postal")
            return false
        }
        
        if self.textTown.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Delegación ó Municipio")
            return false
        }
        
        if self.labelState?.text == nil {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del Estado")
            return false
        }else if self.labelState?.text == "Nombre del Estado" {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del Estado")
            return false
        }
        
        if self.labelCountry.text == nil {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del País")
            return false
        }else if self.labelState?.text == "Nombre del País" {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del País")
            return false
        }
        
        if self.textEmail.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Estructura de Correo Electrónico: correo@dominio.com")
            return false
        }
        
        self.setTextFieldValuesToStruct()
        return true
    }
    
    // MARK: - Business Delegate Functions
    
    func userDidSelectStateAndCountry(countryCode: String, countryName: String, stateCode: String, stateName: String) {
        self.setValueState(stateCode, name: stateName)
        self.setValueCountry(countryCode, name: countryName)
    }
}


extension PatientInfoTableViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textPatientName {
            if let text = textField.text where !text.isEmpty {
                textRfc.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre / Razón Social")
            }
        } else if textField == textRfc {
            if let text = textField.text where !text.isEmpty && GeneralData.containsRFCMatch(text){
                textStreet.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Estructura RFC: 3 o 4 letras, 6 números, 2 letras o números y 1 número")
            }
        } else if textField == textStreet {
            if let text = textField.text where !text.isEmpty {
                textExtNumber.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre de la Calle")
            }
        } else if textField == textExtNumber {
            if let text = textField.text where !text.isEmpty {
                textDeptoNumber.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el No. Exterior")
            }
        } else if textField == textDeptoNumber {
            textSuburb.becomeFirstResponder()
        } else if textField == textSuburb {
            if let text = textField.text where !text.isEmpty {
                textZipCode.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Colonia")
            }
        } else if textField == textZipCode {
            if let text = textField.text where !text.isEmpty {
                textTown.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Código Postal")
            }
        } else if textField == textTown {
            if let text = textField.text where !text.isEmpty {
                textField.resignFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Delegación ó Municipio")
            }
        } else if textField == textEmail {
            if let text = textField.text where !text.isEmpty {
                textField.resignFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Estructura de Correo Electrónico: correo@dominio.com")
            }
        } else {
            textField.resignFirstResponder()
        }
        
        
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if showOrderDetail == true{
            return false
        }
        
        return true
    }
}

//
//  SurgerySumaryUITableViewCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/5/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class SurgerySumaryUITableViewCell: UITableViewCell {
    @IBOutlet weak var surgeryTypeLabel: UILabel!
    @IBOutlet weak var surgeryDateLabel: UILabel!
    @IBOutlet weak var surgerySetLabel: UILabel!
    @IBOutlet weak var technicalInstrumentalistLabel: UILabel!
    
    func config(surgeryType:String?, surgeryDate:String?, surgerySet:String?, technicalInstrumentalist:Bool?){
        surgeryTypeLabel.text = surgeryType
        surgeryDateLabel.text = surgeryDate
        surgerySetLabel.text = surgerySet
        technicalInstrumentalistLabel.text = technicalInstrumentalist ?? true ? "":"No se requiere Instrumentista"
    }
}

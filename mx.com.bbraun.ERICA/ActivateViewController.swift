//
//  ActivateViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class ActivateViewController: UIViewController {

    @IBOutlet weak var userTextField: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func activateUserTapped(sender: AnyObject) {
        textFieldShouldReturn(userTextField)
    }
    
    // MARK: - Business Functions
    
    func initView(){
        userTextField.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goActiveMenu" {
            let splitViewController: UISplitViewController = segue.destinationViewController as! UISplitViewController
            
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
            
            splitViewController.delegate = splitViewController
        }
    }
    
    func goOut(){

        performSegueWithIdentifier("goActiveMenu", sender: nil)
    }
    
    func activateDevice(){
        guard let user = userTextField.text where !user.isEmpty else{
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Usuario para activar el dispositivo")
                return
        }
        
        SwiftSpinner.show("Activando Dispositivo")
        guard Reachability.isConnectedToNetwork() else {
            dispatch_async(GDCUtil.GlobalMainQueue) {
                SwiftSpinner.hide()
            }
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe tener acceso a internet para activar el dispositivo")
            return
        }
        
        NSUserDefaults.standardUserDefaults().setValue(user, forKey: UserDefaults.UserLoggedIn.rawValue)
        NSUserDefaults.standardUserDefaults().synchronize()
        
        goOut()
        
        dispatch_async(GDCUtil.GlobalMainQueue) {
            SwiftSpinner.hide()
        }
 
    }
}

extension ActivateViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if userTextField == textField{
            activateDevice()
        }
        
        return true;
    }
    
}

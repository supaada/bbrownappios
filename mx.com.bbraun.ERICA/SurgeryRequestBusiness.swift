//
//  SurgeryRequestBusiness.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 16/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class SurgeryRequestBusiness {
    
    class func loadAgendaInventory(idSetsurgeryType:String, array:NSMutableArray) {
        //InstrumentalSetAgenda
        //let array:NSMutableArray = []
        DataManager.getCatalogWithSuccess(URL.InstrumentalSetAgenda) { (dData, error) -> Void in
            let json = JSON(data:dData)
            print("existe la agenda \(json.array?.count)")
            
            if let catalogArray = json.array {
                for appDic in catalogArray {
                    
                    if let uuid:String = appDic["id_agenda_set_instrumental"].string, let claveSetInstrumental:String = appDic["clave_set_instrumental"].string, let setIntrumental:String = appDic["set_intrumental"].string, let fechaInicio = appDic["fecha_hora_inicio"].string, let fechaFin = appDic["fecha_hora_fin"].string {
                        
                        let dictionary: NSDictionary = ["uuid":uuid,"claveSetInstrumental":claveSetInstrumental, "setIntrumental":setIntrumental, "fechaInicio":fechaInicio, fechaFin:"fechaFin"]
                        array.addObject(dictionary)
                    }
                }
            }
        }
    }
    
    class func getFolioOrderFromWS() -> NSMutableDictionary{
        let response: NSMutableDictionary = NSMutableDictionary()
        //var webService: FolioGeneratorSoap = FolioGeneratorSoap()
            
        //webService.getFolioOrder("Simulando Creacion del Folio")
        print("getFolioOrderFromWS")
        DataManager.getFolioCatalogWithSuccess{(dData) -> Void in
            let json = JSON(data:dData)
            //var array:NSMutableArray = []
            if let catalogArray = json.array {
                for appDic in catalogArray {
                    if let folio:String = appDic["AUTO_INCREMENT"].string {
                        response.setValue(folio, forKey: "folioOrder")
                    }
                }
            }else{
                response.setValue("No se pudo conectar al servicio", forKey: "errorMessage")

            }
            //print(":')")
        }
        return response
    }
    
    class func rejectOrder(surgeryType: SurgeryTypeViewStruct, reason: String) -> NSMutableDictionary{
        let response: NSMutableDictionary = NSMutableDictionary()
        let webService: FolioGeneratorSoap = FolioGeneratorSoap()
        let recordDateTime: NSDate = NSDate()
        
        let id: NSNumber = OrderSurgery.saveData(surgeryType, contactRequestSurgery: nil, patientInfo: nil, comments: reason, recordDateTime: recordDateTime)
        
        if ConfigWS().isConnectedToNetwork() {
            webService.getFolioOrder("Simulando Rechazo de la Order de Cirugia con Numero: \(surgeryType.folioOrder) y Motivo de rechazo: \(reason)")
        
            if webService.errorMessage.isEmpty {
                let folioOrder: String? = webService.result!["ns1:out"] as? String
                
                response.setValue(folioOrder, forKey: "folioOrder")
                
                if folioOrder != nil {
                    OrderSurgery.setSentOrder(id)
                }
            }else{
                response.setValue(webService.errorMessage, forKey: "errorMessage")
            }
        }else{
           response.setValue(webService.errorMessage, forKey: "warningMessage")
        }
        
        return response
    }
    
    class func createOrder(surgeryType: SurgeryTypeViewStruct, contactRequestSurgery: ContactRequestSurgeryViewStruct?, patientInfo: PatientInfoViewStruct?, comments: String?) -> NSMutableDictionary{
        let response: NSMutableDictionary = NSMutableDictionary()
        let recordDateTime: NSDate = NSDate()
        
        let id: NSNumber = OrderSurgery.saveData(surgeryType, contactRequestSurgery: contactRequestSurgery, patientInfo: patientInfo, comments: comments, recordDateTime: recordDateTime)
        
        if Reachability.isConnectedToNetwork() {
            print("createOrder")
            DataManager.createOrder(surgeryType, contactRequestSurgery: contactRequestSurgery, patientInfo: patientInfo, comments: comments){(dData) -> Void in
                let json = JSON(data:dData)
                print("Create Order Middleware")
                print(json)
                print("Folio \(json["ordenCirugiaRespuesta"]["folioSolicitud"].int)")
                if let folioInt = json["ordenCirugiaRespuesta"]["folioSolicitud"].int {
                    if folioInt != 0 {
                        print("updateSentOrder")
                        OrderSurgery.updateSentOrder2(id, folioOrder: String(folioInt), /*status: GeneralData.Status.created, */recordDateTime: recordDateTime, comments: comments)
                    } else {
                        response.setValue("Warning|La Solicitud de fue rechazada", forKey: "warningMessage")
                        OrderSurgery.updateSentOrder(id, folioOrder: String(folioInt), status: GeneralData.Status.rejected, recordDateTime: recordDateTime, comments: comments)
                    }
                }else{
                    response.setValue("Warning|No se pudó enviar la Solicitud y queda pendiente para una Sincronización posterior", forKey: "warningMessage")
                }
            }
        }
        return response
    }
    
    class func synchronizeOrder(surgeryType: SurgeryTypeViewStruct, contactRequestSurgery: ContactRequestSurgeryViewStruct?, patientInfo: PatientInfoViewStruct?, comments: String?) -> NSMutableDictionary{
        let response: NSMutableDictionary = NSMutableDictionary()
        let recordDateTime: NSDate = NSDate()

        if Reachability.isConnectedToNetwork() {
            print("synchronizeOrder")
            DataManager.getFolioCatalogWithSuccess{(dData) -> Void in
                let json = JSON(data:dData)
                if let catalogArray = json.array {
                    for appDic in catalogArray {
                        if let folio:String = appDic["AUTO_INCREMENT"].string {
                            print("updateSentOrder")
                            OrderSurgery.updateSentOrder(surgeryType.id!, folioOrder: folio, status: GeneralData.Status.created, recordDateTime: recordDateTime, comments: nil)
                        }
                    }
                }else{
                    response.setValue("Warning|No se pudó enviar la Orden y queda pendiente para una Sincronización posterior", forKey: "warningMessage")
                }
            }
        }else{
            response.setValue("Warning|No se pudó enviar la Orden y queda pendiente para una Sincronización posterior", forKey: "warningMessage")
        }
        return response
    }
    
    class func synchronizeOrderRequest(requestId:String?, surgeryType: SurgeryTypeViewStruct, contactRequestSurgery: ContactRequestSurgeryViewStruct?, patientInfo: PatientInfoViewStruct?, comments: String?) -> NSMutableDictionary{
        print("entering -> synchronizeOrderRequest")

        
        guard let requestId = requestId else { return [:] }
        
        let response: NSMutableDictionary = NSMutableDictionary()
        let recordDateTime: NSDate = NSDate()
        
        if Reachability.isConnectedToNetwork() {
            DataManager.getOrderStatus(requestId){ (dData) -> Void in
                let json = JSON(data:dData)
                print("json \n\(json)")
                
                print("\(json["estatusSolicitud"])")
                GeneralData.Status.created
                
                if let status = json["estatusSolicitud"].int  where status == 3 {
                    OrderSurgery.updateSentOrder3(surgeryType.id!, folioOrder: requestId, status: GeneralData.Status.created, recordDateTime: recordDateTime)
                }else if let pendings = json["listaPendientes"].array, status = json["estatusSolicitud"].int  where pendings.count == 0 && status == 2  {
                    OrderSurgery.updateSentOrder3(surgeryType.id!, folioOrder: requestId, status: GeneralData.Status.created, recordDateTime: recordDateTime)
                } else if let status = json["estatusSolicitud"].int  where status == 1 {
                    OrderSurgery.updateSentOrder3(surgeryType.id!, folioOrder: requestId, status: GeneralData.Status.rejected, recordDateTime: recordDateTime)
                }
            }
        }else{
            response.setValue("Warning|No se pudó enviar la Orden y queda pendiente para una Sincronización posterior", forKey: "warningMessage")
        }
        return response
    }
    
    class func getValuePatientInfoStruct(patientInfo: PatientInformation?) -> PatientInfoViewStruct? {
        var patientInfoStruct: PatientInfoViewStruct?
        
        if patientInfo != nil {
            patientInfoStruct = PatientInfoViewStruct()
            
            patientInfoStruct!.name = patientInfo!.name
            patientInfoStruct!.taxId = patientInfo!.taxId
            patientInfoStruct!.street = patientInfo!.street
            patientInfoStruct!.buildingNumber = patientInfo!.buildingNumber
            patientInfoStruct!.apparment = patientInfo!.apparment
            patientInfoStruct!.locality = patientInfo!.locality
            patientInfoStruct!.postalCode = patientInfo!.postalCode
            patientInfoStruct!.town = patientInfo!.town
            patientInfoStruct!.stateName = patientInfo!.state
            patientInfoStruct!.countryName = patientInfo!.country
            patientInfoStruct!.email = patientInfo!.email
        }
        
        return patientInfoStruct
    }
    
    class func getValueContactRequestSurgeryStruct(order: OrderSurgery) -> ContactRequestSurgeryViewStruct? {
        return getValueContactRequestSurgeryStruct(order, toSynchronize: false)
    }
    
    class func getValueContactRequestSurgeryStruct(order: OrderSurgery, toSynchronize: Bool) -> ContactRequestSurgeryViewStruct? {
        var contactRequestSurgeryStruct: ContactRequestSurgeryViewStruct?
        
        var folioOrderVar = ""
        if let folioOrder = order.folioOrder where folioOrder.isEmpty && !toSynchronize {
            folioOrderVar = folioOrder
        }else {
            folioOrderVar = "0000000000"
        }
        
        if true {
            let billTo: BillTo? = order.billToId == nil ? nil : BillTo.getBillToById(order.billToId!)
            let paymentCondition: PaymentCondition? = order.paymentConditionId == nil ? nil : PaymentCondition.getPaymentConditionById(order.paymentConditionId!)
            
            contactRequestSurgeryStruct = ContactRequestSurgeryViewStruct()
            
            contactRequestSurgeryStruct!.folioOrder = folioOrderVar
            contactRequestSurgeryStruct!.customerName = order.customerName
            contactRequestSurgeryStruct!.customerNumber = order.customerNumber
            contactRequestSurgeryStruct!.doctorName = order.doctorName
            contactRequestSurgeryStruct!.patientName = order.patientName
            contactRequestSurgeryStruct!.shipmentType = order.shipmentType
            contactRequestSurgeryStruct!.billToTitle = billTo?.title
            contactRequestSurgeryStruct!.billToCode = billTo?.code
            contactRequestSurgeryStruct!.otherCustomerNumber = order.otherCustomerNumber
            contactRequestSurgeryStruct!.otherCustomerName = order.otherCustomer
            contactRequestSurgeryStruct!.paymentConditionTitle = paymentCondition?.title
            contactRequestSurgeryStruct!.paymentConditionCode = paymentCondition?.code
            contactRequestSurgeryStruct!.hasDepositInfo = order.depositInfo != nil
            
            if contactRequestSurgeryStruct!.hasDepositInfo == true {
                let dateFormatter: NSDateFormatter = NSDateFormatter()
                
                dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                
                contactRequestSurgeryStruct!.bank = order.depositInfo?.bank
                if let deposit = order.depositInfo, date = deposit.depositDate {
                    contactRequestSurgeryStruct!.depositDate = dateFormatter.stringFromDate(date)
                }
                contactRequestSurgeryStruct!.referenceNumber = order.depositInfo?.referenceNumber
                contactRequestSurgeryStruct!.transactionNumber = order.depositInfo?.transactionNumber
            }
        }
        
        return contactRequestSurgeryStruct
    }
    
    class func getValueSurgeryTypeStruct(order: OrderSurgery) -> SurgeryTypeViewStruct? {
        return getValueSurgeryTypeStruct(order, toSynchronize: false)
    }
    
    class func getValueSurgeryTypeStruct(order: OrderSurgery, toSynchronize: Bool) -> SurgeryTypeViewStruct? {
        var surgeryTypeStruct: SurgeryTypeViewStruct?
        var folioOrderVar = ""
        if let folioOrder = order.folioOrder where folioOrder.isEmpty && !toSynchronize {
            folioOrderVar = folioOrder
        }else {
            folioOrderVar = "0000000000"
        }
        
        if true {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            
            surgeryTypeStruct = SurgeryTypeViewStruct()
            surgeryTypeStruct?.folioOrder = folioOrderVar
            surgeryTypeStruct!.id = order.id
            surgeryTypeStruct!.surgeryTypeName = order.surgeryType
            surgeryTypeStruct!.surgeryTypeNumber = order.surgeryTypeNumber
            
            surgeryTypeStruct!.surgeryFamilyTypeName = order.surgeryFamilyName
            surgeryTypeStruct!.surgeryFamilyTypeNumber = order.surgeryFamilyNumber
            surgeryTypeStruct!.surgerySetName = order.surgerySetName
            surgeryTypeStruct!.surgerySetNumber = order.surgerySetNumer
            if let surgeryDate = order.surgeryDateTime {
                surgeryTypeStruct!.surgeryDateTime = dateFormatter.stringFromDate(surgeryDate)
            }
            surgeryTypeStruct!.implantSystemCode = order.implantSystemCode
            surgeryTypeStruct!.implantSystemName = order.implantSystem
            surgeryTypeStruct!.implantSystemAvailability = order.implantSystemAvailability
            surgeryTypeStruct!.instrumentalSetCode = order.instrumentalSetCode
            surgeryTypeStruct!.instrumentalSetName = order.instrumentalSet
            surgeryTypeStruct!.instrumentalSetAvailability = order.instrumentalSetAvailability
            surgeryTypeStruct!.technicalNumber = order.technicalNumber
            surgeryTypeStruct!.technicalName = order.technical
            surgeryTypeStruct!.technicalAvailability = order.technicalAvailability
            surgeryTypeStruct!.salesRepresentativeNumber = order.salesRepresentativeNumber
            surgeryTypeStruct!.salesRepresentativeName = order.salesRepresentative
            surgeryTypeStruct!.priceListCode = order.priceListCode
            surgeryTypeStruct!.priceListName = order.priceList
            surgeryTypeStruct!.priceImplantSystem = order.priceImplantSystem
        }
        
        return surgeryTypeStruct
    }
        
    class func getOrderFromWebServiceResult(result: NSDictionary) -> NSArray{
        let response: NSDictionary = result["ns1:out"] as! NSDictionary
        let arrayOfString: NSArray = response["ns1:string"] as! NSArray
        
        return arrayOfString
    }
    
    class func updateOrder(id: NSNumber, order: NSArray){
        let folioOrder: String? = order[1] as? String
        let recordDate: String? = order[2] as? String
        let recordTime: String? = order[3] as? String
        
        if folioOrder != nil {
            OrderSurgery.updateSentOrder(id, folioOrder: folioOrder!, status: GeneralData.Status.created, recordDate: recordDate, recordTime: recordTime, comments: nil)
        }
    }
    
    class func updateOrder(id: NSNumber, folioOrder: String, comments: String?){
        OrderSurgery.updateSentOrder(id, folioOrder: folioOrder, status: GeneralData.Status.rejected, recordDate: nil, recordTime: nil, comments: comments)
    }
}
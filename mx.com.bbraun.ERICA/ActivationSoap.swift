//
//  ActivationSoap2.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 19/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class ActivationSoap: NSObject, SOAPHelperDelegate {
    let ACTION_WS_NAME = "activarDispositivo"
    let ACTIVATE_WS_NANE = "ActivacionWS"
    
    var result:NSDictionary?
    var errorMessage:String = ""
    
    func activateDevice(in0: String) {
        if !ConfigWS().isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        
        var error:NSError?
        var soap = SOAPHelper()
        let url: String = ConfigWS().getUrl(ACTIVATE_WS_NANE)
        
        soap.delegate = self
        
        soap.setValueParameter(in0, forKey: "in0")
        
        var xmlDoc:AEXMLElement?
        do {
            xmlDoc = try soap.syncRequestURL(url,
                        soapAction: ACTION_WS_NAME)
        } catch var error1 as NSError {
            error = error1
            xmlDoc = nil
        }
        
        if let err = error {
            self.errorMessage = "Falló la conexion del Web Service (\(self.ACTIVATE_WS_NANE)): \(err.localizedDescription)"
            
            print(err.localizedDescription)
        }else{
            if xmlDoc != nil && xmlDoc!.children.count > 0 {
                var element: AEXMLElement = xmlDoc!["ns1:out"]
                
                if element.name != AEXMLElement.errorElementName {
                    setResult(element)
                }else{
                    element = xmlDoc!["faultstring"]
                    
                    if element.name != AEXMLElement.errorElementName {
                        let response: String = element.stringValue
                        
                        self.errorMessage = "Falló la conexion del Web Service (\(self.ACTIVATE_WS_NANE)): \(response)"
                    }else{
                        self.errorMessage = "Falló la conexion del Web Service (\(self.ACTIVATE_WS_NANE)): \(AEXMLElement.errorElementName)"
                    }
                }
            }else{
                self.errorMessage = "Falló la conexion del Web Service (\(self.ACTIVATE_WS_NANE)): Respuesta Nula o Vacia"
            }
        }
    }
    
    internal func setResult(element: AEXMLElement){
        let response: String = element.stringValue
        print(response)
        
        
        if response.indexOf("ERROR") >= 0 {
            var splitMessage = response.characters.split {$0 == "|"}.map { String($0) }
            
            if splitMessage.count > 1 {
                self.errorMessage = splitMessage[1] as String
            }
        }else{
            result = ["ns1:out" : response]
        }
    }
    
    //SOAPHelperDelegate
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeSendingURLRequest request: NSMutableURLRequest) -> NSMutableURLRequest {
        //println(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))
        
        return request
    }
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeParsingResponseString data: NSData) -> NSData {
        //println(NSString(data: data, encoding: NSUTF8StringEncoding))
        
        return data
    }
}
//
//  ValidityUtil.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit

class ValidityUtil{
    class func isErrorValidityActivateArray(arrayError: [String]?) -> Bool{
        var exito: Bool = true
        let messageError: String = GeneralData.Erros.ERROR + GeneralData.Erros.INVALIDATE_USR_ERROR
        
        if arrayError != nil && arrayError!.count > 0 {
            exito = exito && arrayError![0] == messageError
        }
        
        return exito
    }
    
    class func isErrorValidityActivate(errorMessage: String?) -> Bool{
        if errorMessage != nil {
            let splitErrorMessage = (errorMessage!).characters.split {$0 == "|"}.map { String($0) }
        
            return isErrorValidityActivateArray(splitErrorMessage)
        }
        
        return true
    }
    
    class func getErroMessage(errorMessage: String?) -> String{
        let anotherErroMessage: String = "La vigencia para el Grupo de Vendedores [$0] ha terminado, solo puede sincronizar PEDIDOS PENDIENTES. Contacte a su Administrador"
        let groupSellers = getGroupSellers(errorMessage!)
        
        return anotherErroMessage.replace("$0", withString: groupSellers)
    }
    
    class func getGroupSellers(errorMessage: String?) -> String{
        let splitErrorMessage = (errorMessage!).characters.split {$0 == "|"}.map { String($0) }
        
        return getGroupSellersArray(splitErrorMessage)
    }
    
    class func getGroupSellersArray(errorMessageArray: [String]?) -> String{
        var groupSellers: String = ""
        
        if errorMessageArray != nil && errorMessageArray!.count > 1{
            groupSellers = errorMessageArray![1]
        }
        
        return groupSellers
    }
}
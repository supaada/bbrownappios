//
//  OrdersTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class OrdersTableViewController: UITableViewController {
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "OrderCell"
            static let EmptyCell = "EmptyOrderCell"
        }
        
        struct SegueIdentifiers {
            static let SyinchronizeSegue = "goSynchronizeViewController"
            static let AddOrder = "goAddNewOrder"
            static let ViewOrderDetailSegue = "goViewOrderDetail"
        }
        
        static let Sections: [String] = ["Generadas", "Pendientes"]
    }
    
    var pendingOrders: NSMutableArray?
    var createdOrders: NSMutableArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return TableView.Sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var num: Int = -1
        
        if section == 0{
            if self.createdOrders != nil {
                num = self.createdOrders!.count == 0 ? 1 : self.createdOrders!.count
            }else{
                num = 1
            }
        }else{
            if self.pendingOrders != nil{
                num = self.pendingOrders!.count == 0 ? 1 : self.pendingOrders!.count
            }else{
                num = 1
            }
        }
        
        return num
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (createdOrders == nil || createdOrders!.count == 0) && indexPath.section == 0{
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.EmptyCell, forIndexPath: indexPath) 
            
            // this is where you set your color view
            let customColorView: UIView = UIView()
            
            customColorView.backgroundColor = GeneralData.Appearance.thirdColor
            cell.selectedBackgroundView =  customColorView;
            
            return cell
        }else if (pendingOrders == nil || pendingOrders!.count == 0) &&  indexPath.section == 1{
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.EmptyCell, forIndexPath: indexPath) 
            
            // this is where you set your color view
            let customColorView: UIView = UIView()
            
            customColorView.backgroundColor = GeneralData.Appearance.thirdColor
            cell.selectedBackgroundView =  customColorView;
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) as! OrderCell
            
            if indexPath.section == 0{
                cell.configureCell(createdOrders![indexPath.row] as! NSMutableDictionary, createdOrder: true)
            }else{
                cell.configureCell(pendingOrders![indexPath.row] as! NSMutableDictionary, createdOrder: false)
            }
            
            // this is where you set your color view
            let customColorView: UIView = UIView()
            
            customColorView.backgroundColor = GeneralData.Appearance.thirdColor
            cell.selectedBackgroundView =  customColorView;
            
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TableView.Sections[section]
    }


    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0{
            if createdOrders != nil && createdOrders!.count > 0{
                self.performSegueWithIdentifier(TableView.SegueIdentifiers.ViewOrderDetailSegue, sender: createdOrders![indexPath.row])
            }
        }else{
            if pendingOrders != nil && pendingOrders!.count > 0{
                self.performSegueWithIdentifier(TableView.SegueIdentifiers.ViewOrderDetailSegue, sender: pendingOrders![indexPath.row])
            }
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == TableView.SegueIdentifiers.AddOrder {
            let nvc = segue.destinationViewController as! UINavigationController
            let addNewViewController = nvc.visibleViewController as! SurgeryTypeTableViewController
            addNewViewController.showOrderDetail = false
            addNewViewController.showButtons = true
            addNewViewController.firstTime = true
        }else if segue.identifier == TableView.SegueIdentifiers.ViewOrderDetailSegue{
            let element: NSMutableDictionary = sender as! NSMutableDictionary
            let nvc = segue.destinationViewController as! UINavigationController
            let showOrderDetailViewController = nvc.visibleViewController as! CreateRequestSurgeryTableViewController
            showOrderDetailViewController.showOrderDetail = true
            showOrderDetailViewController.id = element.valueForKey("id") as? NSNumber
        }
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        self.refreshScreen()
    }
    
    func loadOrders(){
        self.createdOrders = OrderSurgery.getAllCreatedOrders()
        self.pendingOrders = OrderSurgery.getAllPendingOrders()
    }
    
    func refreshScreen(){
        self.loadOrders()
        self.tableView.reloadData()
    }
    
    func confirmDeleteAll(){
        SCLAlertView().showConfirm(GeneralData.AlertTitles.syncCatalogTitle, subTitle: "Se borrarán todas las Ordenes creadas en el Dispositivo. ¿Desea continuar con el proceso?", buttonAcceptTarget: self, buttonAcceptSelector: "deleteAll")
    }
    
    func deleteAll(){
        SwiftSpinner.show("Borrando Historial")
        
        dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
            OrderSurgery.deleteData()
            
            NSUserDefaults.standardUserDefaults().setValue(0, forKey: "idSequence")
            
            SwiftSpinner.delay(seconds: 2.0, completion: {
                dispatch_async(GDCUtil.GlobalMainQueue) {
                    self.refreshScreen()
                    SwiftSpinner.hide()
                }
            })
        }
    }
}

class OrderCell: UITableViewCell{
    @IBOutlet weak var numberIdOrderLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var totalAmountOrderLabel: UILabel!
    
    func configureCell(order: NSDictionary, createdOrder: Bool){
        let surgeryType: String = order.valueForKey("surgeryType") as! String
        let patientName: String = order.valueForKey("patientName") as! String
        let detail: String = createdOrder ? order.valueForKey("folioOrder") as! String : order.valueForKey("customerName") as! String
        
        
        numberIdOrderLabel!.text = surgeryType
        customerNameLabel!.text = patientName
        totalAmountOrderLabel!.text = detail
        
        preservesSuperviewLayoutMargins = false
        layoutMargins = UIEdgeInsetsZero
    }
}
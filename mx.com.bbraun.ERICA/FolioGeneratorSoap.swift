//
//  BalanceDetailSoap.swift
//  SFVM
//
//  Created by Administrador Prospectiva on 31/03/15.
//  Copyright (c) 2015 Prospectiva en Tecnologia e Integradora de Sistemas SA de CV. All rights reserved.
//

import Foundation
import UIKit

class FolioGeneratorSoap: NSObject, SOAPHelperDelegate{
    let ACTION_WS_NAME = "registrarGeoreferencia"
    let FOLIO_GENERATOR_WS_NANE = "TrackingWS"
    
    var result:NSDictionary?
    var errorMessage:String = ""
    
    func getFolioOrder(in0: String) {
        if !ConfigWS().isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        
        var error:NSError?
        var soap = SOAPHelper()
        let url: String = ConfigWS().getUrl(FOLIO_GENERATOR_WS_NANE)
        
        soap.delegate = self
        
        soap.setValueParameter(in0, forKey: "in0")
        
        var xmlDoc:AEXMLElement?
        do {
            xmlDoc = try soap.syncRequestURL(url,
                        soapAction: ACTION_WS_NAME)
        } catch var error1 as NSError {
            error = error1
            xmlDoc = nil
        }
        
        if let err = error {
            self.errorMessage = "Falló la conexion del Web Service (\(self.FOLIO_GENERATOR_WS_NANE)): \(err.localizedDescription)"
            
            print(err.localizedDescription)
        }else{
            if xmlDoc != nil && xmlDoc!.children.count > 0 {
                var element: AEXMLElement = xmlDoc!["ns1:out"]
                
                if element.name != AEXMLElement.errorElementName {
                    setResult(element)
                }else{
                    element = xmlDoc!["faultstring"]
                    
                    if element.name != AEXMLElement.errorElementName {
                        let response: String = element.stringValue
                        
                        self.errorMessage = "Falló la conexion del Web Service (\(self.FOLIO_GENERATOR_WS_NANE)): \(response)"
                    }else{
                        self.errorMessage = "Falló la conexion del Web Service (\(self.FOLIO_GENERATOR_WS_NANE)): \(AEXMLElement.errorElementName)"
                    }
                }
            }else{
                self.errorMessage = "Falló la conexion del Web Service (\(self.FOLIO_GENERATOR_WS_NANE)): Respuesta Nula o Vacia"
            }
        }
    }
    
    internal func setResult(element: AEXMLElement){
        let response: String = element.stringValue
        print(response)
        
        if ValidityUtil.isErrorValidityActivate(response) {
            self.errorMessage = ValidityUtil.getErroMessage(response)
        }else{
            result = ["ns1:out" : response]
        }
    }
    
    //SOAPHelperDelegate
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeSendingURLRequest request: NSMutableURLRequest) -> NSMutableURLRequest {
        //println(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))
        
        return request
    }
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeParsingResponseString data: NSData) -> NSData {
        //println(NSString(data: data, encoding: NSUTF8StringEncoding))
        
        return data
    }
}
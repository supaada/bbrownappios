//
//  MasterViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit
import CoreData

enum CellIdentifiers:String {
    case MenuCell = "MenuCell"
}


class MenuSidePanelTableViewController: UITableViewController {

    let menus: Array<MenuStruct>! = Menu.allMainMenu()
    var collapseDetailViewController: Bool  = true
    
    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch(MenuSegue.init(rawValue: identifier)){
            case .OrdersSegue:
                let controller: OrdersTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! OrdersTableViewController
            
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            case .SearchSegue:
                let controller: SearchingTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! SearchingTableViewController
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            case .SynchronizeSegue:
                let controller: SynchronizeTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! SynchronizeTableViewController
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            case .NewOrderSegue:
                let controller: SurgeryTypeTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! SurgeryTypeTableViewController
                
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
                
                controller.showOrderDetail = false
                controller.showButtons = true
                controller.firstTime = true
            case .LogoutSegue:
                print("prepareForSegueMenu LogoutSegue")
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn")
                NSUserDefaults.standardUserDefaults().synchronize()
            default: break
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.MenuCell.rawValue, forIndexPath: indexPath) as! MenuCell
        
        cell.configureForMenu(menus[indexPath.row])
        let customColorView: UIView = UIView()
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        
        return cell
    }
    
    // Mark: Table View Delegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedMenu = menus[indexPath.row]
        let areThereCatalogs = Surgery.isThereInfoCatalog()
        
        collapseDetailViewController = false
        if selectedMenu.forward == MenuSegue.LogoutSegue.rawValue {
            //Logout
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn")
            NSUserDefaults.standardUserDefaults().synchronize()
            performSegueWithIdentifier(selectedMenu.forward, sender: nil)
        }else{
            if !areThereCatalogs {
                SCLAlertView().showWarning(GeneralData.AlertTitles.infoTitle, subTitle: "No existen Catalogos en el movil, necesita recuperarlos")
                performSegueWithIdentifier(MenuSegue.SynchronizeSegue.rawValue, sender: areThereCatalogs)
            }else{
                performSegueWithIdentifier(selectedMenu.forward, sender: nil)
            }
        }
    }
    
    // MARK: - Business Functions
    
    func initView(){
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 5, width: 155, height: 35))
        imageView.contentMode = .ScaleAspectFit
        
        let image = UIImage(named: "logo_bbraun_white")
        imageView.image = image
        
        
        navigationItem.titleView = imageView
    }
}

class MenuCell: UITableViewCell {
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabelView: UILabel!
    @IBOutlet weak var menuDescriptionLabelView: UILabel!
    
    func configureForMenu(menu: MenuStruct) {
        menuImageView.image = menu.image
        menuTitleLabelView.text = menu.title
        menuDescriptionLabelView.text = menu.description
        
        preservesSuperviewLayoutMargins = false
        layoutMargins = UIEdgeInsetsZero
    }
}


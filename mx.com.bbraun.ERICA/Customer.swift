//
//  Customer.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(Customer)
class Customer: NSManagedObject {
    struct CustomerProperties {
        static let idFilter = "customerNumber == %@"
    }

    @NSManaged var country: String
    @NSManaged var customerNumber: String
    @NSManaged var locked: NSNumber
    @NSManaged var name: String
    @NSManaged var number: String
    @NSManaged var postalCode: String
    @NSManaged var region: String
    @NSManaged var state: String
    @NSManaged var street: String
    @NSManaged var taxId: String
    @NSManaged var totalCredit: NSNumber
    @NSManaged var town: String
    @NSManaged var statement: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(Customer))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            let dictionary = item as! NSDictionary
            let newRow: Customer = CoreDataHelper.getRowEntity(NSStringFromClass(Customer)) as! Customer
            
            newRow.customerNumber = dictionary["customerNumber"] as! String
            newRow.name = dictionary["name"] as! String
            newRow.street = dictionary["street"] as! String
            newRow.number = dictionary["number"] as! String
            newRow.town = dictionary["town"] as! String
            newRow.region = dictionary["region"] as! String
            newRow.postalCode = dictionary["postalCode"] as! String
            newRow.state = dictionary["state"] as! String
            newRow.country = dictionary["country"] as! String
            newRow.taxId = dictionary["taxId"] as! String

            let totalCredit = dictionary["totalCredit"] as! String
            
            newRow.totalCredit = NSNumber(double: 100000)
            let locked = dictionary["locked"] as! String
            newRow.locked =  locked == "1" || locked == "true" || locked == "SI"
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func getElementById(customerNumber: String) -> Customer?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(Customer), filter: CustomerProperties.idFilter, value: customerNumber)
        let element: Customer? = array.last as? Customer
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let customers: NSMutableArray = NSMutableArray()
        var customerDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Customer))
        
        for item in array{
            customerDic = NSMutableDictionary()
            
            customerDic!.setValue((item as! Customer).name, forKey: "description")
            customerDic!.setValue((item as! Customer).customerNumber, forKey: "code")
            customerDic!.setValue((item as! Customer).totalCredit, forKey: "detail")
            customerDic!.setValue((item as! Customer).locked, forKey: "flag")
            
            customers.addObject(customerDic!)
        }
        
        return customers
    }
    
    class func getAllElementsDestination() -> NSMutableArray{
        let customers: NSMutableArray = NSMutableArray()
        var customerDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(Customer))
        
        for item in array{
            customerDic = NSMutableDictionary()
            guard let item = item as? Customer else { continue }
            customerDic!.setValue(item.name, forKey: "name")
            customerDic!.setValue(item.customerNumber, forKey: "clientNumber")
            customerDic!.setValue("\(item.street) \(item.number), \(item.postalCode), \(item.state), \(item.country) " , forKey: "destination")
            customerDic!.setValue(item.totalCredit, forKey: "detail")
            customerDic!.setValue(item.locked, forKey: "flag")
            
            customers.addObject(customerDic!)
        }
        
        return customers
    }
}

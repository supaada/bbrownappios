//
//  DestinationCatalogTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/25/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

public protocol CatalogDestinationDelegate{
    func userDidSelectElementDestination(code: String, description: String, detail: String)
}

class DestinationCatalogTableViewController: UITableViewController ,UISearchBarDelegate {
        
        @IBOutlet weak var searchBar: UISearchBar!
        
        struct TableView {
            struct CellIdentifiers {
                static let CatalogCellCDestination = "CatalogCellCDestination"
                
            }
            static let CatalogTitles: [String] = ["Tipo de Cirugía","Familia de Set de Cirugía","Set de Cirugía", "Sistemas de Implantes", "Sets de Instrumental", "Técnicos", "Representante de Ventas", "Precio de Lista", "Lugar de Entrega", "Doctor", "Tipo de Envío", "Facturar a ", "Cliente a Facturar", "Ninguno"]
            
            struct Predicate {
                static let FullDescription: String = "name CONTAINS[cd] %@ OR clientNumber CONTAINS[cd] %@ OR destination CONTAINS[cd] %@"
                static let DescriptionCode: String = "description CONTAINS[cd] %@ OR code CONTAINS[cd] %@"
                static let Description: String = "description CONTAINS[cd] %@"
            }
            
        }
        
        var idCatalog: Int?
        var elements: NSArray?
        var filteredElements: NSArray?
        var filterString: String?
        var code: String?
        var delegate: CatalogDestinationDelegate?
        var delegateAdd: DestinationInfoDelegate?

        var cell: UITableViewCell?
        var name: String?
        var detail: AnyObject?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            initView()
            
        }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goDestinationAdd" {
            if let controller: DestinationTableViewController = segue.destinationViewController as? DestinationTableViewController{
                controller.delegate = delegateAdd
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    // MARK: - Table view data source
        
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
        
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let elements = filteredElements {
            return elements.count
        }else{
            return 0
        }
    }
        
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = getAndConfigureCell(indexPath, element: filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary)
                
        // this is where you set your color view
        let customColorView: UIView = UIView()
                
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
                
        /*if let codePresent = code, filtered = filteredElements, element = filtered.objectAtIndex(indexPath.row) as? NSMutableDictionary, let codeElement = element.valueForKey("clientNumber") as? String where codePresent == codeElement{
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }*/
                
        return cell
    }
        
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        let element: NSDictionary = filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary
        code = element.valueForKey("clientNumber") as? String
        let description: String = element.valueForKey("destination") as! String
        detail = element.valueForKey("name")
        name = description
        self.cell = cell
        acceptItem(cell, description: description, detail: detail)
            
    }
        
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdCustomerCatalog:
                return 50.0
            default: break
            }
        }
        return 44.0
    }
    
    // MARK: - Search Bar
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let predicate = NSPredicate(format: TableView.Predicate.FullDescription, searchText, searchText, searchText)
        filteredElements = searchText.isEmpty ? elements : NSArray(array: elements!.filteredArrayUsingPredicate(predicate))
                tableView.reloadData()
    }
        
    // MARK: - Butons Actions
    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
        
    // MARK: - Business Functions
    func initView(){
        title = TableView.CatalogTitles[idCatalog!]
        searchBar.delegate = self
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
        
    func loadData(){
        elements = getElementsByCatalog(idCatalog)
        filteredElements = elements
        tableView.reloadData()
        unselectItems()
    }
        
    func getElementsByCatalog(idCatalog: Int?) -> NSArray? {
        let elements = Customer.getAllElementsDestination()
        return elements
    }
        
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
        
    func getAndConfigureCell(indexPath: NSIndexPath, element: NSDictionary) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.CatalogCellCDestination, forIndexPath: indexPath) as! CatalogCellCDestination
        if let name = element.valueForKey("name") as? String,  let id = element.valueForKey("clientNumber") as? String, let destination = element.valueForKey("destination") as? String {
            cell.configure(name, clientId: id, destination: destination)
        }
        return cell
    }
        
    func acceptItemTapped(){
        if let cellPresent = cell, namePresent = name {
            acceptItem(cellPresent, description: namePresent, detail: detail)
        }else{
            goOut()
        }
    }
        
    func acceptItem(cell: UITableViewCell, description: String, detail: AnyObject?){
        unselectItems()
        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        if let cod = code, detail = detail as? String {
            delegate?.userDidSelectElementDestination(cod, description: description, detail:detail)
        }
        goOut()
    }
        
    func unselectItems(){
        let numRows: Int = tableView.numberOfRowsInSection(0);
            
        for indexRow in 0 ..< numRows {
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}

class CatalogCellCDestination: UITableViewCell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var clientNumberLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
        
    func configure(name: String, clientId:String, destination:String){
        nameLabel.text = name
        clientNumberLabel.text = clientId
        destinationLabel.text = destination
    }
}


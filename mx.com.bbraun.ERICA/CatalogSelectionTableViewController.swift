//
//  SurgeryTypeSelectionTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

public protocol CatalogDelegate{
    func userDidSelectElement(idCatalog: Int, code: String, description: String, detail: AnyObject?)
}

class CatalogSelectionTableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    struct TableView {
        struct CellIdentifiers {
            static let CellA = "CatalogCellA"
            static let CellB = "CatalogCellB"
            static let CellC = "CatalogCellC"
            
        }
        static let CatalogTitles: [String] = ["Tipo de Cirugía","Familia de Set de Cirugía","Set de Cirugía", "Sistemas de Implantes", "Sets de Instrumental", "Técnicos", "Representante de Ventas", "Precio de Lista", "Clientes", "Doctor", "Tipo de Envío", "Facturar a ", "Otros Clientes", "Ninguno"]
        
        struct Predicate {
            static let DescriptionCode: String = "description CONTAINS[cd] %@ OR code CONTAINS[cd] %@"
            static let Description: String = "description CONTAINS[cd] %@"
        }
        
        struct SegueIdentifiers {
            static let CalendarScheduleSegue = "goCalendarSchedule"
        }
    }
    
    var idCatalog: Int?
    var elements: NSArray?
    var filteredElements: NSArray?
    var filterString: String?
    var code: String?
    var delegate: CatalogDelegate?
    var delegateTime: DateTimeSelectionDelegate?
    var delegateCalendar: CalendarSelectionDelegate?
    var delegatePending: SurgeryRequestPendingDelegate?
    var delegateStatistics: SurgeryRequestStatisticsDelegate?
    var delegateDismiss: DismissSurgeryRequestDelegate?
    var cell: UITableViewCell?
    var name: String?
    var detail: AnyObject?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredElements != nil {
            return filteredElements!.count
        }else{
            return 0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = getAndConfigureCell(indexPath, element: filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary)

        // this is where you set your color view
        let customColorView: UIView = UIView()
            
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        
        if let codePresent = code, filtered = filteredElements, element = filtered.objectAtIndex(indexPath.row) as? NSMutableDictionary, let codeElement = element.valueForKey("code") as? String where codePresent == codeElement{
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        let element: NSDictionary = filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary
        code = element.valueForKey("code") as? String
        let description: String = element.valueForKey("description") as! String
        detail = element.valueForKey("detail")
        name = description
        self.cell = cell
        
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdDoctorCatalog, .IdSalesRepresentativeCatalog, .IdOtherCustomerCatalog:
                acceptItem(cell, description: description, detail: detail)
            case .IdCustomerCatalog:
                if let detail = detail as? String {
                    acceptItem(cell, description: detail, detail: description)
                }
            case .IdSurgeryTypeCatalog:
                acceptItemSegue(cell, description: description, detail: detail, segue:SelectSegueIdentifier.CatalogInseptionSegue, catalog:  CatalogPosition.IdSurgeryFamilyCatalog)
            case .IdSurgerySetCatalog:
                code = element.valueForKey("uuid") as? String
                if let _ = code {print("key found")} else { code = element.valueForKey("code") as? String}
                let description: String = element.valueForKey("description") as! String
                detail = element.valueForKey("code")
                name = description
                acceptItem(cell, description: description, detail: detail)
            case .IdShipmentTypeCatalog:
                if code == "Local" {
                acceptItemSegue(cell, description: description, detail: detail, segue:SelectSegueIdentifier.CatalogInseptionSegue, catalog:  CatalogPosition.IdShipmentTypeCatalog)
                } else {
                    acceptItem(cell, description: description, detail: detail)
                }
            default: break
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSystemsCatalog, .IdInstrumentalsCatalog, .IdTechsCatalog, .IdCustomerCatalog, .IdPriceListCatalog:
                return 64.0
            case .IdDoctorCatalog, .IdShipmentTypeCatalog,  .IdSurgeryTypeCatalog, .IdSurgerySetCatalog, .IdSalesRepresentativeCatalog, .IdOtherCustomerCatalog:
                return 35.0
            default: break
            }
        }
        return 44.0
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SelectSegueIdentifier.CalendarScheduleSegue.rawValue {
            if let controller: CalendarScheduleViewController = segue.destinationViewController as? CalendarScheduleViewController{
                controller.idCatalog = self.idCatalog
                controller.code = self.code
                controller.name = self.name
            }
        }else if segue.identifier == SelectSegueIdentifier.CatalogInseptionSegue.rawValue {
            if let controller = segue.destinationViewController as? CatalogInseptionTableViewController, let catalogId:Int  = sender as? Int {
                controller.idCatalog = catalogId
                controller.code = code
                controller.filterString = nil
                controller.delegate = delegate
                controller.delegateTime = delegateTime
                controller.delegateCalendar = delegateCalendar
                controller.delegatePending = delegatePending
                controller.delegateStatistics = delegateStatistics
                controller.delegateDismiss = delegateDismiss
            }
        }
    }
    
    // MARK: - Search Bar
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        var predicate: NSPredicate?
        
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSurgeryTypeCatalog, .IdSystemsCatalog, .IdInstrumentalsCatalog, .IdTechsCatalog, .IdSalesRepresentativeCatalog, .IdPriceListCatalog, .IdCustomerCatalog:
                predicate = NSPredicate(format: TableView.Predicate.DescriptionCode, searchText, searchText)
            case .IdDoctorCatalog, .IdShipmentTypeCatalog:
                predicate = NSPredicate(format: TableView.Predicate.Description, searchText)
            default: break
            }
        }
        
        if predicate != nil {
            filteredElements = searchText.isEmpty ? elements : NSArray(array: elements!.filteredArrayUsingPredicate(predicate!))
            tableView.reloadData()
        }
    }
    
    // MARK: - Butons Actions

    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        title = TableView.CatalogTitles[idCatalog!]
        searchBar.delegate = self
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        setNavigationButtons()
    }
    
    func loadData(){
        elements = getElementsByCatalog(idCatalog)
        filteredElements = elements
        tableView.reloadData()
        unselectItems()
    }
    
    func setNavigationButtons(){
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSystemsCatalog, .IdInstrumentalsCatalog, .IdTechsCatalog:
                let buttonItemAccept: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptItemTapped")
                let buttonItemCalendar = UIBarButtonItem(image: UIImage(named: "ic_option_calendar"), style: UIBarButtonItemStyle.Plain, target: self, action: "goCalendar")
                navigationItem.rightBarButtonItems = [buttonItemAccept, buttonItemCalendar]
            default: break
            }
        }
    }
    
    func getElementsByCatalog(idCatalog: Int?) -> NSArray? {
        var elements: NSArray?
        
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSurgeryTypeCatalog:
                elements = Surgery.getAllElementsSurgeryTypes()
            case .IdSurgerySetCatalog:
                if let cod = code {
                    elements = Surgery.getElementsSetCirugiaId(cod)
                }
            case .IdSystemsCatalog:
                if let filter = filterString {
                    elements = RelationshipSurgeryTypeImplantSystem.getAllElementsBySurgeryType(filter)
                }else{
                    elements = ImplantSystem.getAllElements()
                }
            case .IdInstrumentalsCatalog:
                if let filter = filterString {
                    elements = RelationshipSurgeryTypeInstrumentalSet.getAllElementsBySurgeryType(filter)
                }else{
                    elements = InstrumentalSet.getAllElements()
                }
            case .IdTechsCatalog:
                elements = TechnicalInstrumentalist.getAllElementsBis()
            case .IdSalesRepresentativeCatalog:
                elements = SalesRepresentative.getAllElements()
            case .IdPriceListCatalog:
                if let filter = filterString {
                    elements = PriceListImplantSystem.getAllPriceListByImplantSystem(filter)
                }
            case .IdCustomerCatalog:
                elements = Customer.getAllElements()
            case .IdDoctorCatalog:
                elements = Doctor.getAllElements()
            case .IdShipmentTypeCatalog:
                elements = ShipmentType.getAllElements()
            case .IdOtherCustomerCatalog:
                elements = Customer.getAllElements()
            default: break
            }
        }
        
        return elements
    }
    
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func goCalendar(){
        unselectItems()
        performSegueWithIdentifier(SelectSegueIdentifier.CalendarScheduleSegue.rawValue, sender: nil)
    }
    
    func getAndConfigureCell(indexPath: NSIndexPath, element: NSDictionary) -> UITableViewCell{
        
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSystemsCatalog, .IdInstrumentalsCatalog, .IdTechsCatalog, .IdPriceListCatalog:
                let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.CellA, forIndexPath: indexPath) as! CatalogCellA
                if let description = element.valueForKey("description") as? String, code = element.valueForKey("code") as? String {
                    cell.configure(description, code: code, detail: true)//element.valueForKey("detail")!)
                }
                return cell
            case .IdDoctorCatalog, .IdShipmentTypeCatalog, .IdSurgeryTypeCatalog, .IdSurgerySetCatalog, .IdSalesRepresentativeCatalog, .IdCustomerCatalog, .IdOtherCustomerCatalog:
                let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.CellC, forIndexPath: indexPath) as! CatalogCellC
                if let description = element.valueForKey("description") as? String {
                    cell.configure(description)
                }
                return cell
            default: break
            }
        }
        return UITableViewCell()
    }
    
    func acceptItemTapped(){
        if let cellPresent = cell, namePresent = name {
            acceptItem(cellPresent, description: namePresent, detail: detail)
        }else{
            goOut()
        }
    }
    
    // MARK: Accept
    func acceptItemSegue(cell: UITableViewCell, description: String, detail: AnyObject?, segue:SelectSegueIdentifier, catalog:CatalogPosition){
        unselectItems()
        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        if let cat = idCatalog, let cod = code {
            delegate?.userDidSelectElement(cat, code: cod, description: description, detail: detail ?? "")
        }
        performSegueWithIdentifier(segue.rawValue, sender: catalog.hashValue)
    }
    
    func acceptItem(cell: UITableViewCell, description: String, detail: AnyObject?){
        unselectItems()
        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        if let cat = idCatalog, let cod = code {
            delegate?.userDidSelectElement(cat, code: cod, description: description, detail: detail ?? "")
        }
        goOut()
    }
    
    func unselectItems(){
        let numRows: Int = tableView.numberOfRowsInSection(0);
        
        for indexRow in 0 ..< numRows{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}

class CatalogCellA: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    func configure(description: String, code: String, detail: AnyObject){
        descriptionLabel.text = description
        codeLabel.text = code
        
        if detail is String {
            detailLabel.text = detail as? String
        }else if detail is NSNumber {
            let format: NSNumberFormatter = NSNumberFormatter()
            
            format.numberStyle = .CurrencyStyle
            detailLabel.text = format.stringFromNumber(detail as! NSNumber)
        }else if detail is Bool{
            detailLabel.text = GeneralData.Messages.available//(detail as? Bool) == true ? GeneralData.Messages.available : GeneralData.Messages.unavailable
        }
        
        if detailLabel.text == GeneralData.Messages.unavailable {
            descriptionLabel.textColor = GeneralData.Appearance.redColorText
            codeLabel.textColor = GeneralData.Appearance.redColorText
            detailLabel.textColor = GeneralData.Appearance.redColorText
        }else{
            descriptionLabel.textColor = GeneralData.Appearance.mainColorText
            codeLabel.textColor = GeneralData.Appearance.thirdColorText
            detailLabel.textColor = GeneralData.Appearance.thirdColorText
        }
    }
}

class CatalogCellB: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    func configure(description: String, code: String){
        descriptionLabel.text = description
        codeLabel.text = code
    }
}

class CatalogCellC: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(description: String){
        descriptionLabel.text = description
    }
}

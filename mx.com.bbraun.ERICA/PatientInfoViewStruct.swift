//
//  PatientInfoViewStruct.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

struct PatientInfoViewStruct {
    var apparment: String?
    var buildingNumber: String?
    var countryCode: String?
    var countryName: String?
    var email: String?
    var locality: String?
    var name: String?
    var postalCode: String?
    var stateCode: String?
    var stateName: String?
    var street: String?
    var taxId: String?
    var town: String?
    var destinationId: String?
}
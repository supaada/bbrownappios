//
//  DataSource.swift
//  SFVM
//
//  Created by Familia Ortiz Soto on 12/03/15.
//  Copyright (c) 2015 Prospectiva en Tecnologia e Integradora de Sistemas SA de CV. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper{
    struct CoreDataHelperProperties {
        static let manObjCxt = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    }
    
    class func isThereInfoCatalog(entityName: String) -> Bool{
        var error: NSError?
        let fetchRequest: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        
        let count: Int! = CoreDataHelperProperties.manObjCxt!.countForFetchRequest(fetchRequest, error: &error) as Int
        
        if let result = count {
            return result > 0
        }
        
        print("No se puedo ejecutar la consulta: \(error), \(error!.userInfo)")
        return false
    }
    
    class func deleteFrom(entityName: String){
        print("Borrando todos los objetos de la Entidad: \(entityName)")
        var error:NSError?
        let fetchRequest: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        
        var objectsIds: [AnyObject]!
        do {
            objectsIds = try CoreDataHelperProperties.manObjCxt!.executeFetchRequest(fetchRequest)
        } catch let error1 as NSError {
            error = error1
            objectsIds = nil
        }
        
        for objectId: AnyObject in objectsIds{
            CoreDataHelperProperties.manObjCxt!.deleteObject(objectId as! NSManagedObject)
        }
        
        objectsIds.removeAll(keepCapacity: false)
        
        saveContext()
    }
    
    class func deleteRow(row: NSManagedObject){
        CoreDataHelperProperties.manObjCxt!.deleteObject(row)
        
        saveContext()
    }
    
    class func createRowEntity(entityName: String) -> NSManagedObject{
        let entity: NSEntityDescription! = NSEntityDescription.entityForName(entityName, inManagedObjectContext: CoreDataHelperProperties.manObjCxt!)!
        let row: NSManagedObject! = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: CoreDataHelperProperties.manObjCxt!)
        
        return row
    }
    
    class func saveContext(){
        var error : NSError? = nil
        
        if CoreDataHelperProperties.manObjCxt?.hasChanges != nil{
            let isCommited: Bool
            do {
                try CoreDataHelperProperties.manObjCxt!.save()
                isCommited = true
            } catch let error1 as NSError {
                error = error1
                isCommited = false
            }
            
            if !isCommited {
                print("No se pudo guardar los cambios en el ManageObjectCOntext: \(error?.userInfo)")
            }
        }
    }
    
    class func getRowEntity(entityName: String) -> AnyObject{
        let newRowEntity: AnyObject = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: CoreDataHelperProperties.manObjCxt!)
        
        return newRowEntity
    }
    
    class func fetchAllEntity(entityName: String) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func fetchFilterEntityString(entityName: String, filter: String, value: String) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        let predicate: NSPredicate! = NSPredicate(format: filter, value)
        
        request.predicate = predicate
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func fetchFilterEntityNumber(entityName: String, filter: String, value: NSNumber) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        let predicate: NSPredicate! = NSPredicate(format: filter, value)
        
        request.predicate = predicate
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func fetchFilterEntityDate(entityName: String, filter: String, value: NSDate) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        let predicate: NSPredicate! = NSPredicate(format: filter, value)
        
        request.predicate = predicate
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func fetchFilterEntityAndPredicates(entityName: String, filtersPredicates: [NSPredicate]) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        let predicate: NSCompoundPredicate! = NSCompoundPredicate(andPredicateWithSubpredicates: filtersPredicates)
        
        request.predicate = predicate
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func fetchFilterEntityOrPredicates(entityName: String, filtersPredicates: [NSPredicate]) -> [AnyObject] {
        let request: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        var rows: [AnyObject] = [AnyObject]()
        let predicate: NSCompoundPredicate! = NSCompoundPredicate(orPredicateWithSubpredicates: filtersPredicates)
        
        request.predicate = predicate
        
        if let fetchResults = try? CoreDataHelperProperties.manObjCxt!.executeFetchRequest(request) {
            rows = fetchResults
        }
        
        return rows
    }
    
    class func countForFetchByFilterString(entityName: String, filter: String, value: String) -> Int{
        var error: NSError?
        let fetchRequest: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        let predicate: NSPredicate! = NSPredicate(format: filter, value)
        
        fetchRequest.predicate = predicate
        
        let result = CoreDataHelperProperties.manObjCxt!.countForFetchRequest(fetchRequest, error: &error) as Int?
        
        if let count = result {
            return count
        }
        
        return 0
    }
    
    class func countForFetchByFilterNumber(entityName: String, filter: String, value: NSNumber) -> Int{
        var error: NSError?
        let fetchRequest: NSFetchRequest! = NSFetchRequest(entityName: entityName)
        let predicate: NSPredicate! = NSPredicate(format: filter, value)
        
        fetchRequest.predicate = predicate
        
        let result = CoreDataHelperProperties.manObjCxt!.countForFetchRequest(fetchRequest, error: &error) as Int?
        
        if let count = result {
            return count
        }
        
        return 0
    }
}

//
//  DestinationTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/25/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol DestinationInfoDelegate {
    func userDidEnterDestinationInfo(structVar: DestinationInfoStruct?)
}

class DestinationTableViewController: UITableViewController {
        @IBOutlet weak var textPatientName: UITextField!
        @IBOutlet weak var textStreet: UITextField!
        @IBOutlet weak var textExtNumber: UITextField!
        @IBOutlet weak var textDeptoNumber: UITextField!
        @IBOutlet weak var textSuburb: UITextField!
        @IBOutlet weak var textZipCode: UITextField!
        @IBOutlet weak var textTown: UITextField!
        @IBOutlet weak var labelState: UILabel!
        @IBOutlet weak var labelCountry: UILabel!
    
        struct TableView {
            struct SegueIdentifiers {
                static let CreateOrderSegue = "goCreateOrder2"
                static let StateCountrySegue = "goStateCountry2"
            }
        }
        
        var showOrderDetail: Bool?
        var showButtons: Bool?
        var firstTime: Bool = true
        var delegate: DestinationInfoDelegate?
        
        //Para Lista
        var nextViewController: AnyObject?
        var prevViewController: AnyObject?
        
        //Estructura de la vista
        var structVar: DestinationInfoStruct?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            initView()
        }
        
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(true)
            loadData()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        // MARK: - Table view data source
        
        override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 7
        }
        
        override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            
            // this is where you set your color view
            let customColorView: UIView = UIView()
            
            customColorView.backgroundColor = GeneralData.Appearance.thirdColor
            cell!.selectedBackgroundView =  customColorView;
            
            if indexPath.row == 5 {
                performSegueWithIdentifier(TableView.SegueIdentifiers.StateCountrySegue , sender: nil)
            }
        }
        
        // MARK: - Navigation
        
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if segue.identifier == TableView.SegueIdentifiers.StateCountrySegue {
                if let navigation = segue.destinationViewController as? UINavigationController , let controller = navigation.topViewController as? StateCountryTableViewController {
                        controller.countryCode = structVar?.countryCode
                        controller.stateCode = structVar?.stateCode
                        controller.delegate = self
                }
            }
        }
        
        override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
            if showOrderDetail == true {
                return false
            }
            return true
        }
    
        
        // MARK: - Business Functions
        
        func initView(){
            setNavigationButtons()
            
            textPatientName.delegate = self
            textStreet.delegate = self
            textExtNumber.delegate = self
            textDeptoNumber.delegate = self
            textSuburb.delegate = self
            textZipCode.delegate = self
            textTown.delegate = self
            tableView.tableFooterView = UIView(frame: CGRectZero)
            
        }
        
        func loadData(){
            setValues()
            tableView.reloadData()
        }
    
        func setNavigationButtons(){
            let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptInfo")
                    
            navigationItem.rightBarButtonItems = [buttonItem]
        }
        
        func setValues(){
            setStructValue()
            setInReadMode()
        }
        
        func setStructValue(){
            if self.structVar == nil {
                self.structVar = DestinationInfoStruct()
            }else{
                self.setValuesFromStruct()
            }
        }
        
        func setValuesFromStruct(){
            setValuePatientNameView(structVar?.name)
            setValueStreetView(structVar?.street)
            setValueBuildingNumberView(structVar?.buildingNumber)
            setValueApparmentView(structVar?.apparment)
            setValueLocalityView(structVar?.locality)
            setValuePostalCodeView(structVar?.postalCode)
            setValueTownView(structVar?.town)
            setValueStateView(structVar?.stateName)
            setValueCountryView(structVar?.countryName)
        }
        
        func setValuePatientName(name: String?){
            self.setValuePatientNameView(name)
            
            self.structVar?.name = name
        }
        
        func setValuePatientNameView(name: String?){
            if name != nil {
                self.textPatientName.text = name
            }
        }

        func setValueStreet(street: String?){
            self.setValueStreetView(street)
            
            self.structVar?.street = street
        }
        
        func setValueStreetView(street: String?){
            if street != nil {
                self.textStreet.text = street
            }
        }
        
        func setValueBuildingNumber(buildingNumber: String?){
            self.setValueBuildingNumberView(buildingNumber)
            
            self.structVar?.buildingNumber = buildingNumber
        }
        
        func setValueBuildingNumberView(buildingNumber: String?){
            if buildingNumber != nil {
                self.textExtNumber.text = buildingNumber
            }
        }
        
        func setValueApparment(apparment: String?){
            self.setValueApparmentView(apparment)
            
            self.structVar?.apparment = apparment
        }
        
        func setValueApparmentView(apparment: String?){
            if apparment != nil {
                self.textDeptoNumber.text = apparment
            }
        }
        
        func setValueLocality(locality: String?){
            self.setValueLocalityView(locality)
            
            self.structVar?.locality = locality
        }
        
        func setValueLocalityView(locality: String?){
            if locality != nil {
                self.textSuburb.text = locality
            }
        }
        
        func setValuePostalCode(postalCode: String?){
            self.setValuePostalCodeView(postalCode)
            
            self.structVar?.postalCode = postalCode
        }
        
        func setValuePostalCodeView(postalCode: String?){
            if postalCode != nil {
                self.textZipCode.text = postalCode
            }
        }
        
        func setValueTown(town: String?){
            self.setValueTownView(town)
            
            self.structVar?.town = town
        }
        
        func setValueTownView(town: String?){
            if town != nil {
                self.textTown.text = town
            }
        }
        
        func setValueState(code: String?, name: String?){
            self.setValueStateView(name)
            
            self.structVar?.stateCode = code
            self.structVar?.stateName = name
        }
        
        func setValueStateView(name: String?){
            if name != nil {
                self.labelState.text = name
            }
        }
        
        func setValueCountry(code: String?, name: String?){
            self.setValueCountryView(name)
            
            self.structVar?.countryCode = code
            self.structVar?.countryName = name
        }
        
        func setValueCountryView(name: String?){
            if name != nil {
                self.labelCountry.text = name
            }
        }

        
        func setInReadMode(){
            if showOrderDetail == true {
                let indexPath: NSIndexPath = NSIndexPath(forRow: 6, inSection: 0)
                let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
                
                cell!.accessoryType = UITableViewCellAccessoryType.None
                self.textPatientName.enabled = false
                self.textStreet.enabled = false
                self.textExtNumber.enabled = false
                self.textDeptoNumber.enabled = false
                self.textSuburb.enabled = false
                self.textZipCode.enabled = false
                self.textTown.enabled = false
            }
        }
        
        func confirmSaveInfoPatient(){
            goOut()
        }
        
        func setTextFieldValuesToStruct(){
            structVar?.name = textPatientName.text
            structVar?.street = textStreet.text
            structVar?.buildingNumber = textExtNumber.text
            structVar?.apparment = textDeptoNumber.text
            structVar?.locality = textSuburb.text
            structVar?.postalCode = textZipCode.text
            structVar?.town = textTown.text
            structVar?.destinationId = "0"
        }
        
        func goOut(){
            dismissViewControllerAnimated(true, completion: nil)
        }
        
        func goClose(){
            delegate?.userDidEnterDestinationInfo(structVar)
            dismissViewControllerAnimated(true, completion: nil)
        }
        
        func goNextScreen(){
            if isValidFields() {
                SCLAlertView().showConfirm(GeneralData.AlertTitles.confirmTitle, subTitle: "¿Los datos del Lugar de Entrega son Correctos?", buttonAcceptTarget: self, buttonAcceptSelector: "goCreateOrder")
            }
        }
        
        func goExit(){
            SCLAlertView().showConfirm(GeneralData.AlertTitles.addOrderTitle, subTitle: "Esta apunto de perder los datos asignados hasta ahora. ¿Desea continuar?", buttonAcceptTarget: self, buttonAcceptSelector: "goOut")
        }
        
        func goCreateOrder(){
            performSegueWithIdentifier(TableView.SegueIdentifiers.CreateOrderSegue, sender: -1)
        }
        
        func acceptInfo(){
            if isValidFields() {
                SCLAlertView().showConfirm(GeneralData.AlertTitles.confirmTitle, subTitle: "¿Los datos del Lugar de Entrega son Correctos?", buttonAcceptTarget: self, buttonAcceptSelector: "goClose")
            }
        }
        
        func isValidFields() -> Bool{
            /*guard let textPatientName = textPatientName.text where !textPatientName.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre del Lugar de Entrega")
                return false
            }*/

            guard let textStreet = textStreet.text where !textStreet.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre de la Calle")
                return false
            }
            
           guard let textExtNumber = textExtNumber.text where !textExtNumber.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el No. Exterior")
                return false
            }
            
            guard let textSuburb = textSuburb.text where !textSuburb.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Colonia")
                return false
            }
            
            guard let textZipCode = textZipCode.text where !textZipCode.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Código Postal")
                return false
            }
            
            guard let textTown = textTown.text where !textTown.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Delegación ó Municipio")
                return false
            }
            
            guard let labelState = labelState.text where !labelState.isEmpty && labelState != "Nombre del Estado" else {                 SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del Estado")
                return false
            }
            
            guard let labelCountry = labelCountry.text where !labelCountry.isEmpty && labelCountry != "Nombre del País" else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Nombre del País")
                return false
            }

            structVar?.name = textPatientName.text
            structVar?.street = textStreet
            structVar?.buildingNumber = textExtNumber
            structVar?.apparment = textDeptoNumber.text
            structVar?.locality = textSuburb
            structVar?.postalCode = textZipCode
            structVar?.town = textTown
            structVar?.destinationId = "0"
            return true
        }
    
}

extension DestinationTableViewController: StateCountryDelegate {
    
    func userDidSelectStateAndCountry(countryCode: String, countryName: String, stateCode: String, stateName: String) {
        print("si lo regresa pops \(countryName) - \(stateName)")
        setValueState(stateCode, name: stateName)
        setValueCountry(countryCode, name: countryName)
    }
}

extension DestinationTableViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textPatientName {
            //if let text = textField.text where !text.isEmpty {
                textStreet.becomeFirstResponder()
            /*} else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre del Lugar de Entrega")
            }*/
        } else if textField == textStreet {
            if let text = textField.text where !text.isEmpty {
                textExtNumber.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Nombre de la Calle")
            }
        } else if textField == textExtNumber {
            if let text = textField.text where !text.isEmpty {
                textDeptoNumber.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el No. Exterior")
            }
        } else if textField == textDeptoNumber {
            textSuburb.becomeFirstResponder()
        } else if textField == textSuburb {
            if let text = textField.text where !text.isEmpty {
                textZipCode.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Colonia")
            }
        } else if textField == textZipCode {
            if let text = textField.text where !text.isEmpty {
                textTown.becomeFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Código Postal")
            }
        } else if textField == textTown {
            if let text = textField.text where !text.isEmpty {
                textField.resignFirstResponder()
            } else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre de la Delegación ó Municipio")
            }
        } else {
            textField.resignFirstResponder()
        }
        
        
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if showOrderDetail == true{
            return false
        }
        
        return true
    }
}





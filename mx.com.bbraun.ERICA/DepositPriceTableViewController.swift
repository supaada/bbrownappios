//
//  DepositPriceTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class DepositPriceTableViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var textNameBank: UITextField!
    @IBOutlet weak var textReferenceNumber: UITextField!
    @IBOutlet weak var textTransactionNumber: UITextField!
    
    struct TableView {
        struct SegueIdentifiers {
            static let CatalogSegue = "goCatalogSelection3"
        }
        
        static let IdPriceListCatalog: Int = 7
    }
    
    var showOrderDetail: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }else{
            return 1
        }
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;
        
        if self.showOrderDetail == false {
            if indexPath.row == 0 && indexPath.section == 1 {
                self.performSegueWithIdentifier(TableView.SegueIdentifiers.CatalogSegue, sender: nil)
            }
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller: CatalogSelectionTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! CatalogSelectionTableViewController
        
        controller.idCatalog = TableView.IdPriceListCatalog
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if self.showOrderDetail == true{
            return false
        }
        
        return true
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.textNameBank.delegate = self
        self.textReferenceNumber.delegate = self
        self.textTransactionNumber.delegate = self
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        if self.showOrderDetail == true {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 1)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            self.textNameBank.enabled = false
            self.textReferenceNumber.enabled = false
            self.textTransactionNumber.enabled = false
        }
        
        self.tableView.reloadData()
    }
}

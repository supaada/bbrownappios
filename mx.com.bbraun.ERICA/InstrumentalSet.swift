//
//  InstrumentalSet.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(InstrumentalSet)
class InstrumentalSet: NSManagedObject {
    struct InstrumentalSetProperties {
        static let idFilter = "code == %@"
    }

    @NSManaged var availabilityStatus: String
    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var updateDateTime: NSDate
    @NSManaged var schedule: NSSet
    @NSManaged var surgeryType: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(InstrumentalSet))
    }
    
    class func saveDataV(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //dateFormatter.dateFormat = "dd\\.MM\\.yyyy HH:mm:ss a"
        
        for item in array {
            let dictionary: NSDictionary = item as! NSDictionary
            //var arrayString =  split(item as! String) {$0 == "|"}
            let newRow: InstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(InstrumentalSet)) as! InstrumentalSet
            //var updateDate:String = dictionary["updateDate"] as! String
            //var updateTime:String = dictionary["updateTime"] as! String
            //var dateTimeString: String = "\(updateDate) \(updateTime)"
            //dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            //newRow.updateDateTime = dateFormatter.dateFromString(dateTimeString)!
            newRow.code = dictionary["code"] as! String
            newRow.name = dictionary["name"] as! String
            newRow.availabilityStatus = "Disponible"//dictionary["availabilityStatus"] as! String
        }
        CoreDataHelper.saveContext()
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "dd\\.MM\\.yyyy HH:mm:ss a"
        
        for item in array {
            let dictionary: NSDictionary = item as! NSDictionary
            //var arrayString =  split(item as! String) {$0 == "|"}
            let newRow: InstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(InstrumentalSet)) as! InstrumentalSet
            let updateDate:String = dictionary["updateDate"] as! String
            let updateTime:String = dictionary["updateTime"] as! String
            let dateTimeString: String = "\(updateDate) \(updateTime)"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            newRow.updateDateTime = dateFormatter.dateFromString(dateTimeString)!
            newRow.code = dictionary["code"] as! String
            newRow.name = dictionary["name"] as! String
            newRow.availabilityStatus = dictionary["availabilityStatus"] as! String
        }
        CoreDataHelper.saveContext()
    }
    
    class func getElementById(code: String) -> InstrumentalSet?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(InstrumentalSet), filter: InstrumentalSetProperties.idFilter, value: code)
        let element: InstrumentalSet? = array.last as? InstrumentalSet
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let instrumentalSets: NSMutableArray = NSMutableArray()
        var instrumentalSetsDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(InstrumentalSet))
        
        for item in array{
            instrumentalSetsDic = NSMutableDictionary()
            let flag: Bool = (item as! InstrumentalSet).availabilityStatus == "1" || (item as! InstrumentalSet).availabilityStatus == "true" || (item as! InstrumentalSet).availabilityStatus == "No Disponible"
            
            instrumentalSetsDic!.setValue((item as! InstrumentalSet).name, forKey: "description")
            instrumentalSetsDic!.setValue((item as! InstrumentalSet).code, forKey: "code")
            instrumentalSetsDic!.setValue(false ? GeneralData.Messages.unavailable: GeneralData.Messages.available, forKey: "detail")
            instrumentalSetsDic!.setValue(false, forKey: "flag")
            
            instrumentalSets.addObject(instrumentalSetsDic!)
        }
        
        return instrumentalSets
    }
    
    class func getAllElementsT() -> NSArray{
        return CoreDataHelper.fetchAllEntity(NSStringFromClass(InstrumentalSet))
    }
}

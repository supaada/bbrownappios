//
//  SOAPHelper.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 18/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

protocol SOAPHelperDelegate {
    func soapHelper(soapHelper: SOAPHelper!, didBeforeSendingURLRequest request: NSMutableURLRequest) -> NSMutableURLRequest
    func soapHelper(soapHelper: SOAPHelper!, didBeforeParsingResponseString data: NSData) -> NSData
}

class SOAPHelper: NSObject, NSURLConnectionDataDelegate {
    // Static Attribute
    static let attributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema", "xmlns:soap" : "http://schemas.xmlsoap.org/soap/envelope/"]
    
    // SOAP request Elements
    var soapRequest: AEXMLDocument! = AEXMLDocument()
    var envelope: AEXMLElement?
    var header: AEXMLElement?
    var body: AEXMLElement?
    
    // SOAP Request Parameters
    var mutableData:NSMutableData  = NSMutableData()
    var parameters: NSMutableDictionary = NSMutableDictionary()
    var errorSoap: NSError?
    var delegate: SOAPHelperDelegate?
    var result: AEXMLElement?
    
    internal override init(){
        envelope = soapRequest.addChild(name: "soap:Envelope", attributes: SOAPHelper.attributes)
        header = envelope?.addChild(name: "soap:Header")
        body = envelope?.addChild(name: "soap:Body")
    }
    
    func syncRequestURL(url: String, soapAction: String) throws -> AEXMLElement{
        var error: NSError! = NSError(domain: "Migrator", code: 0, userInfo: nil)
        setSoapRequest(soapAction)
        
        sendSoapRequest(url)
        
        error = errorSoap
        
        if let value = result {
            return value
        }
        throw error
    }
    
    func setValueParameter(value: AnyObject?, forKey: String){
        parameters.setValue(value, forKey: forKey)
    }
    
    internal func setSoapRequest(soapAction: String){
        setChild(body!, key: soapAction, value: parameters)
    }
    
    internal func setChild(element: AEXMLElement, key: String, value: AnyObject){
        if value is NSMutableDictionary {
            let child: AEXMLElement = element.addChild(name: key)
            let items: NSMutableDictionary = value as! NSMutableDictionary
            
            for item in items {
                let key: String = item.key as! String
                let value: AnyObject = item.value
                
                setChild(child, key: key, value: value)
            }
        }else if value is NSMutableArray {
            let child: AEXMLElement = element.addChild(name: key)
            let items: NSMutableArray = value as! NSMutableArray
            
            for item in items {
                setChild(child, key: key, value: item)
            }
        }else if value is String {
            element.addChild(name: key, value: value as? String)
        }
    }
    
    internal func sendSoapRequest(urlString: String){
        let url = NSURL(string: urlString)
        var theRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        var response: NSURLResponse?
        var urlData: NSData?
        let soapMessage: String = soapRequest.xmlString
        let msgLength: String = String(soapMessage.characters.count)
        
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(msgLength, forHTTPHeaderField: "Content-Length")
        theRequest.HTTPMethod = "POST"
        theRequest.timeoutInterval = 7.0
        theRequest.HTTPBody = soapMessage.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) // or false
        
        if delegate != nil {
            theRequest = delegate!.soapHelper(self, didBeforeSendingURLRequest: theRequest)
        }
        
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(theRequest, returningResponse: &response)
        } catch let error as NSError {
            errorSoap = error
            urlData = nil
        }
        
        mutableData.length = 0
        
        if delegate != nil && urlData != nil {
            urlData = delegate!.soapHelper(self, didBeforeParsingResponseString: urlData!)
        }
        
        if urlData != nil {
            mutableData.appendData(urlData!)
        }
        
        parseResponse()
    }
    
    internal func parseResponse(){
        do {
            let xmlDoc = try AEXMLDocument(xmlData: mutableData)
            result = xmlDoc.root["soap:Body"].children.first
        } catch _ {
            print("description: \(errorSoap?.localizedDescription)\ninfo: \(errorSoap?.userInfo)")
        }
    }
    
    // NSURLConnectionDelegate
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        errorSoap = error
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        mutableData.length = 0
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        var dat: NSData! = data
        
        dat = delegate?.soapHelper(self, didBeforeParsingResponseString: data)
        
        mutableData.appendData(dat)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        parseResponse()
    }
}
//
//  PaymentConditionsTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol PaymentConditionsDelegate{
    func userDidSelectPaymentCondition(idPaymentCondition: Int, title: String)
}

class PaymentConditionsTableViewController: UITableViewController {
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "PaymentConditionCell"
        }
    }
    
    var idPaymentCodition: Int?
    var paymentConditionTypes: Array<PaymentCondition>?
    var delegate: PaymentConditionsDelegate?
    var destinataryValidClient: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if paymentConditionTypes != nil {
            return paymentConditionTypes!.count
        }else{
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) 
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        cell.textLabel?.text = self.paymentConditionTypes![indexPath.row].title
        
        if idPaymentCodition != nil {
            if idPaymentCodition == paymentConditionTypes![indexPath.row].id {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        idPaymentCodition = paymentConditionTypes![indexPath.row].id
        let titlePaymentCondition: String = paymentConditionTypes![indexPath.row].title
        
        //self.unselectItems()
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        delegate?.userDidSelectPaymentCondition(idPaymentCodition!, title: titlePaymentCondition)
        goOut()
    }

    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        if let destinataryValidClient = destinataryValidClient where destinataryValidClient == true {
            paymentConditionTypes = PaymentCondition.allPaymentConditionTypes()
        } else {
            paymentConditionTypes = [PaymentCondition.getPaymentConditionById("2")!]
        }
        tableView.reloadData()
    }
    
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func unselectItems(){
        for indexRow in 0 ..< paymentConditionTypes!.count{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}

//
//  CatalogTrinseptionTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/3/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class CatalogTrinseptionTableViewController: UITableViewController{
    struct TableView {
        struct CellIdentifiers3 {
            static let CellA3 = "CatalogCellA3"
            static let CellB3 = "CatalogCellB3"
            static let CellC3 = "CatalogCellC3"
            
        }
        
        static let CatalogTitles3: [String] = ["Tipo de Cirugía","Familia de Set de Cirugía","Set de Tipo de Cirugía", "Sistemas de Implantes", "Sets de Instrumental", "Técnicos", "Representante de Ventas", "Precio de Lista", "Clientes", "Doctor", "Tipo de Envío", "Facturar a ", "Otros Clientes", "Ninguno"]
        
        struct Predicate3 {
            static let DescriptionCode: String = "description CONTAINS[cd] %@ OR code CONTAINS[cd] %@"
            static let Description: String = "description CONTAINS[cd] %@"
        }
    }
    
    var idCatalog: Int?
    var elementsInstrumental: NSArray?
    var elementsImplant: NSArray?
    var baseIntrumental: String?
    var baseImplant: String?
    var filterString: String?
    var code: String?
    var surgerySetId: String?
    var delegate: CatalogDelegate?
    var delegateTime: DateTimeSelectionDelegate?
    var delegateCalendar: CalendarSelectionDelegate?
    var delegatePending: SurgeryRequestPendingDelegate?
    var delegateStatistics: SurgeryRequestStatisticsDelegate?
    var delegateDismiss: DismissSurgeryRequestDelegate?
    var cell: UITableViewCell?
    var name: String?
    var detail: AnyObject?
    var instrumentalOnly: Bool?
    var implantOnly: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let elementsInstrumental = elementsInstrumental {
                return elementsInstrumental.count
            }else{
                return 0
            }
        } else {
            if let implants = elementsImplant {
                return implants.count
            } else {
                return 0
            }
        }
        
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Sets de Instrumental"
        } else {
            return "Sets de Implantes"
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var element:NSDictionary?
        if indexPath.section == 0 {
            element = elementsInstrumental?.objectAtIndex(indexPath.row) as? NSDictionary
        } else if indexPath.section == 1 {
            element = (elementsImplant?.objectAtIndex(indexPath.row) as? NSDictionary)
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers3.CellC3, forIndexPath: indexPath) as! CatalogCellC3
        if let element = element, codeDescription = element.objectForKey("code") as? String {
            cell.configure(codeDescription)
            if let code = baseIntrumental where code == codeDescription && indexPath.section == 0{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            if let code = baseImplant where code == codeDescription && indexPath.section == 1{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        let customColorView: UIView = UIView()
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        var element: NSDictionary?
        if indexPath.section == 0 {
            element = elementsInstrumental?.objectAtIndex(indexPath.row) as? NSDictionary
        }else if indexPath.section == 1 {
            element = elementsImplant?.objectAtIndex(indexPath.row) as? NSDictionary
        }
        guard let _ = element else { return }
        
        if cell.accessoryType == UITableViewCellAccessoryType.None {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }else if cell.accessoryType == UITableViewCellAccessoryType.Checkmark {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
    }
    
    func goImplanteSolo() {
        implantOnly = true
        instrumentalOnly = false
        unselectItems(0)
        selectBase(1)
        acceptItemSegue()
    }
    
    func goInstrumentalSolo() {
        implantOnly = false
        instrumentalOnly = true
        unselectItems(1)
        selectBase(0)
        acceptItemSegue()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let idCatalogPresent = idCatalog {
            switch (CatalogPosition.init(rawValue: idCatalogPresent)){
            case .IdSystemsCatalog, .IdInstrumentalsCatalog, .IdTechsCatalog, .IdPriceListCatalog:
                return 64.0
            case .IdDoctorCatalog, .IdShipmentTypeCatalog, .IdSurgerySetCatalog:
                return 35.0
            default: break
            }
        }
        return 44.0
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SelectSegueIdentifier.CatalogTrinseptionSegue.rawValue {
            if let controller: CalendarScheduleViewController = segue.destinationViewController as? CalendarScheduleViewController{
                controller.idCatalog = idCatalog
                controller.code = surgerySetId
                controller.name = name
                controller.delegateCalendar = delegateCalendar
                controller.delegate = delegateTime
                controller.delegatePending = delegatePending
                controller.delegateStatistics = delegateStatistics
                controller.delegateDismiss = delegateDismiss
            }
        }else if segue.identifier == SelectSegueIdentifier.DateTimeSelection3Segue.rawValue {
            let controller: DateTimeSelectionViewController = segue.destinationViewController as! DateTimeSelectionViewController
            controller.isPopover = false
            controller.isOrder = true
            controller.delegate = delegateTime
        }
    }
    
    // MARK: - Butons Actions
    
    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        title = TableView.CatalogTitles3[idCatalog ?? 1]
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        setNavigationButtons()
    }
    
    func loadData(){
        getElements()
        tableView.reloadData()
    }
    
    func setNavigationButtons(){
        let buttonItemAccept: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptItemSegue")
        navigationItem.rightBarButtonItems = [buttonItemAccept]
    }
    
    func getElements() {
        if let codeDel = code {
            let elementsTuple = Surgery.getElementsSetCirugia(codeDel)
            elementsInstrumental = elementsTuple.setsIntrumental
            elementsImplant = elementsTuple.setsImplants
            baseIntrumental = elementsTuple.baseIntrumental
            baseImplant = elementsTuple.baseImplant
        }else {
            elementsInstrumental = []
            elementsImplant = []
        }
        
    }
    
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Accept
    func acceptItemSegue() {
        let code = findCode()
        if !code.isEmpty {
            unselectItems(0)
            unselectItems(1)
            surgerySetId = code
            performSegueWithIdentifier(SelectSegueIdentifier.CatalogTrinseptionSegue.rawValue, sender: CatalogPosition.IdSurgerySetCatalog.hashValue)
        } else {
            selectBase(0)
            selectBase(1)
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Combinación no aprobada por Área Comercial")
        }
    }
    
    func findCode() -> String {
        let instrumentals = getItems(0)
        let implants = getItems(1)
        var element: NSDictionary?
        element = Surgery.getSurgerySetBySets(implants, setsInstrumental: instrumentals, baseSent: code!)
        if let code = element?.objectForKey("uuid") as? Int, description = element?.objectForKey("description") as? String {
            delegate?.userDidSelectElement(CatalogPosition.IdSurgerySetCatalog.rawValue, code: "\(code)", description: description, detail: detail ?? "")
            return "\(code)"
        }
        return ""
    }
    
    func unselectItems(section:Int){
        let numRows: Int = tableView.numberOfRowsInSection(section);
        
        for indexRow in 0 ..< numRows{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: section)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }
    }
    
    func selectBase(section:Int) {
        let numRows: Int = tableView.numberOfRowsInSection(section);
        var element:NSDictionary?
        
        for indexRow in 0 ..< numRows{
            if section == 0 {
                element = elementsInstrumental?[indexRow] as? NSDictionary
            } else if section == 1 {
                element = elementsImplant?[indexRow] as? NSDictionary
            }
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: section)

            if let cell = tableView.cellForRowAtIndexPath(indexPath), let element = element, elementCode = element.objectForKey("code") as? String, code = code where code == elementCode {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
    }
    
    func getItems(section:Int) -> Set<String> {
        let numRows: Int = tableView.numberOfRowsInSection(section);
        var items = Set<String>()
        for indexRow in 0 ..< numRows{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: section)
            if let cell  = tableView.cellForRowAtIndexPath(indexPath) where cell.accessoryType == UITableViewCellAccessoryType.Checkmark {
                if section == 0 {
                    if let element = elementsInstrumental![indexRow] as? NSDictionary, code = element.objectForKey("code") as? String {
                        items.insert(code)
                    }
                }else if section == 1 {
                    if let element = elementsImplant![indexRow] as? NSDictionary, code = element.objectForKey("code") as? String {
                        items.insert(code)
                    }
                }
            }
        }
        return items
    }
}

class CatalogCellA3: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    func configure(description: String, code: String, detail: AnyObject){
        descriptionLabel.text = description
        codeLabel.text = code
        
        if detail is String {
            detailLabel.text = detail as? String
        }else if detail is NSNumber {
            let format: NSNumberFormatter = NSNumberFormatter()
            
            format.numberStyle = .CurrencyStyle
            detailLabel.text = format.stringFromNumber(detail as! NSNumber)
        }else if detail is Bool{
            detailLabel.text = GeneralData.Messages.available//(detail as? Bool) == true ? GeneralData.Messages.available : GeneralData.Messages.unavailable
        }
        
        if detailLabel.text == GeneralData.Messages.unavailable {
            descriptionLabel.textColor = GeneralData.Appearance.redColorText
            codeLabel.textColor = GeneralData.Appearance.redColorText
            detailLabel.textColor = GeneralData.Appearance.redColorText
        }else{
            descriptionLabel.textColor = GeneralData.Appearance.mainColorText
            codeLabel.textColor = GeneralData.Appearance.thirdColorText
            detailLabel.textColor = GeneralData.Appearance.thirdColorText
        }
    }
}

class CatalogCellB3: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    func configure(description: String, code: String){
        descriptionLabel.text = description
        codeLabel.text = code
    }
}

class CatalogCellC3: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(description: String){
        descriptionLabel.text = description
    }
}

//
//  DataManager.swift
//  TopApps
//
//  Created by Dani Arnaout on 9/2/14.
//  Edited by Eric Cerney on 9/27/14.
//  Copyright (c) 2014 Ray Wenderlich All rights reserved.
//

import Foundation

extension NSMutableURLRequest {
    func setBodyContent(contentMap: Dictionary<String, String>) {
        var firstOneAdded = false
        var contentBodyAsString = String()
        let contentKeys:Array<String> = Array(contentMap.keys)
        for contentKey in contentKeys {
            if !firstOneAdded {
                contentBodyAsString = contentBodyAsString + contentKey + "=" + contentMap[contentKey]!
                firstOneAdded = true
            } else {
                contentBodyAsString = contentBodyAsString + "&" + contentKey + "=" + contentMap[contentKey]!
            }
        }
        contentBodyAsString = contentBodyAsString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
//        contentBodyAsString = contentBodyAsString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        self.HTTPBody = contentBodyAsString.dataUsingEncoding(NSUTF8StringEncoding)
    }
}

enum URL:String {
    case Doctors = "catalogo_medico.php", SurgeryType = "orden_cirugia.php", ImplantSystem = "sets_implante_combinacion.php", InstrumentalSet = "sets_intrumental_combinacion.php", TechnicallInstrumentalist = "catalogo_tecnico_intrumentista.php", SalesRepresentative = "catalogo_representate_venta.php", PriceList = "catalogo_lista_precios.php", PriceListImplantSystem = "catalogo_precio_sistema_implantes.php", Customer = "catalogo_clientes.php", ShipmentType = "catalogo_tipo_envio.php", Country = "catalogo_pais_estado.php", BankStatement = "catalogo_estado_cuenta.php", SurgeryTypeSet = "tipo_set_cirugia.php", InstrumentalSetAgenda = "agenda_set_instrumental.php", NewOrder = "folios.php", NewRequest = "solicitudes.php", AllCatalogos = "catalogos", Order = "ordenCirugia", OrderStatus = "s1/consultarEstatus/", SyncronizeAgenda = "s2/SincronizaAgendas/"
}
enum Domain:String {
    case URL = "http://172.30.165.67:8080/BBraunMWServices3/services/rest/"
    case URLAlt = "http://172.30.165.67:8080/ServiciosJSONBBraun/services/"
    case DomainReverse = "mx.red-point"
}

class DataManager {
    
    class func getCatalogWithSuccess(url:URL, success:((dData:NSData!, error:NSError?)-> Void)) {
        print("entrando a getCatalogWithSuccess")
        loadDataFromURL(NSURL(string:Domain.URL.rawValue+url.rawValue)!, completion: { (data, error) -> Void in
            if let urlData = data {
                success(dData:urlData, error: error)
            }
        })
    }
    
    class func createOrder(surgeryType: SurgeryTypeViewStruct, contactRequestSurgery: ContactRequestSurgeryViewStruct?, patientInfo: PatientInfoViewStruct?, comments: String?, success:((dData:NSData!)-> Void)) {

        let url = Domain.URL.rawValue+URL.Order.rawValue
        var base64String:String = ""
        print(NSURL(string: url))
        print("createOrder si viene la imagen \(contactRequestSurgery?.image)")
        
        if let image = contactRequestSurgery?.image {
            let size = CGSize(width: 300, height: 300)
            //let size = CGSizeApplyAffineTransform(image.size, CGAffineTransformMakeScale(0.5, 0.5))
            let hasAlpha = false
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
            image.drawInRect(CGRect(origin: CGPointZero, size: size))
            
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            //print("new image \(image)")
            print("new scaledImage \(scaledImage)")
            if let newImage = scaledImage {
                let imageData = UIImagePNGRepresentation(newImage)
                base64String = imageData!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            }
        }
        
        //print("new base64String \(base64String)")
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.HTTPMethod = "POST"
        
        print("surgeryType.surgeryDateTime \(surgeryType.surgeryDateTime)")
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        
        let date = dateFormatter.dateFromString(surgeryType.surgeryDateTime!)
        
        // TODO: Sustituir por la fecha Deposito real cuando vic quite la obligatoriedad
        var dateDeposit = "2015-12-31 14:53"
        if let depositDateContact = contactRequestSurgery?.depositDate, dateDepositDate = dateFormatter.dateFromString(depositDateContact) {
            print("deposit date \(dateDepositDate)")
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            dateDeposit = dateFormatter.stringFromDate(dateDepositDate) ?? ""
        }
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = dateFormatter.stringFromDate(date ?? NSDate())
        print("end date string \(dateString)")
        print("end date deposit \(dateDeposit)")

        print("representante ventas \(surgeryType.salesRepresentativeNumber)")
        print("id set cirugia \(surgeryType.surgerySetNumber)")
        var response: NSURLResponse?
        let folioSolicitud:Int = 0
        let clvTipoCirugia:String = surgeryType.surgerySetNumber!//Int(surgeryType.surgerySetNumber ?? "0") ?? 0
        let fechaHoraCirugia:String = dateString //surgeryType.surgeryDateTime ??
        let tecnicoInstrumentista:Bool = surgeryType.technicalRequired ?? true
        let representanteVentas:Int = Int(surgeryType.salesRepresentativeNumber ?? "0") ?? 0
        
        let idLugarEntrega:Int = Int(contactRequestSurgery?.customerNumber ?? "0") ?? 0
        let calle:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.street ?? "" : ""
        let numeroExterior:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.buildingNumber ?? "" : ""
        let numeroInterior:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.apparment ?? "" : ""
        let colonia:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.locality ?? "" : ""
        let delegacionMunicipio:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.town ?? "" : ""
        let clavePais:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.countryCode ?? "" : ""
        let claveEstado:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.stateCode ?? "" : ""
        let codigoPostal:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.postalCode ?? "" : ""
        let nombre:String = idLugarEntrega == 0 ? contactRequestSurgery?.destinationInfoStruct?.name ?? "" : ""
        
        let medico:String = contactRequestSurgery?.doctorName ?? "DR JULIO PEREIRA ORTIZ"
        let paciente:String = contactRequestSurgery?.patientName ?? "JUAN LUGO PEREZ"
        let numeroTipoEnvio:Int = Int(contactRequestSurgery?.shipmentTypeNumber ?? "0") ?? 0
        let condicionPago:Int = Int(contactRequestSurgery?.paymentConditionCode ?? "0") ?? 0
        let banco:String = contactRequestSurgery?.bank ?? ""
        let fechaDeposito:String = dateDeposit
        let referencia:String = contactRequestSurgery?.referenceNumber ?? ""
        let movimiento:String = contactRequestSurgery?.transactionNumber ?? ""
        let comprobante:String = base64String
        let observaciones:String = comments ?? ""
        
        let facturarA:String = contactRequestSurgery?.shipmentTypeNumber ?? "0"
        let idCliente:Int = Int(contactRequestSurgery?.otherCustomerNumber ?? "-1") ?? -1
        let calleCliente:String = idCliente == 0 ? patientInfo?.street ?? "" : ""
        let numeroExteriorCliente:String = idCliente == 0 ? patientInfo?.buildingNumber ?? "" : ""
        let numeroInteriorCliente:String = idCliente == 0 ? patientInfo?.apparment ?? "" : ""
        let coloniaCliente:String = idCliente == 0 ? patientInfo?.locality ?? "" : ""
        let delegacionMunicipioCliente:String = idCliente == 0 ? patientInfo?.town ?? "" : ""
        let clavePaisCliente:String = idCliente == 0 ? patientInfo?.countryCode ?? "" : ""
        let claveEstadoCliente:String = idCliente == 0 ? patientInfo?.stateCode ?? "" : ""
        let codigoPostalCliente:String = idCliente == 0 ? patientInfo?.postalCode ?? "" : ""
        let nombreCliente:String = idCliente == 0 ? patientInfo?.name ?? "" : ""
        let razonSocial:String = idCliente == 0 ? patientInfo?.taxId ?? "" : ""
        let correo:String = idCliente == 0 ? patientInfo?.email ?? "" : ""
        
        print("clave tipo cirugia \(clvTipoCirugia)")
        print("fechaDeposito \(dateDeposit)")
        print("idcliente \(idCliente)")


        if Reachability.isConnectedToNetwork() {
            var urlData: NSData?
            do {
                let jsonObj = ["ordenCirugia":["solicitudOrdenCirugia":["folioSolicitud":folioSolicitud,"clvTipoCirugia":clvTipoCirugia,"fechaHoraCirugia":fechaHoraCirugia,"tecnicoInstrumentista":tecnicoInstrumentista,"representanteVentas":representanteVentas],"lugarEntrega":["idLugarEntrega":idLugarEntrega,"calle":calle,"numeroExterior":numeroExterior,"numeroInterior":numeroInterior,"colonia":colonia,"delegacionMunicipio":delegacionMunicipio,"clavePais":clavePais,"claveEstado":claveEstado,"codigoPostal":codigoPostal,"nombre":nombre],"medico":medico,"paciente":paciente,"numeroTipoEnvio":numeroTipoEnvio,"facturarA":facturarA,"clienteFactura":["idCliente":idCliente,"razonSocial":nombreCliente,"rfc":razonSocial,"calle":calleCliente,"numeroExterior":numeroExteriorCliente,"numeroInterior":numeroInteriorCliente,"colonia":coloniaCliente,"delegacionMunicipio":delegacionMunicipioCliente,"clavePais":clavePaisCliente,"claveEstado":claveEstadoCliente,"codigoPostal":codigoPostalCliente,"correo":correo],"condicionPago":condicionPago,"informacionDeposito":["banco":banco,"fechaDeposito":fechaDeposito,"referencia":referencia,"movimiento":movimiento,"comprobante":comprobante],"observaciones":observaciones]]
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(jsonObj, options: [])
                urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
                success(dData:urlData)
            } catch _ {
                urlData = nil
            }
        }
    }
    
    class func getOrderStatus(folio: String, success:((dData:NSData!)-> Void)) {
        let url = Domain.URLAlt.rawValue+URL.OrderStatus.rawValue+folio
        print("Order Status \(url)")
        print("OrderStatus URL \(NSURL(string: url))")
        
        loadDataFromURL(NSURL(string:url)!, completion: { (data, error) -> Void in
            if let urlData = data {
                success(dData:urlData)
            }
        })
    }
    
    class func syncronizeAgenda(surgerySetId: String, success:((dData:NSData!)-> Void)) {
        // surgerySetId 30032
        let url = Domain.URLAlt.rawValue+URL.SyncronizeAgenda.rawValue+surgerySetId
        print("Syncronize Agenda \(url)")
        print("OrderStatus URL \(NSURL(string: url))")
        
        loadDataFromURL(NSURL(string:url)!, completion: { (data, error) -> Void in
            if let urlData = data {
                success(dData:urlData)
            }
        })
    }
    
    class func getFolioCatalogWithSuccess(success:((dData:NSData!)-> Void)) {
        loadDataFromURL(NSURL(string:Domain.URL.rawValue+URL.NewRequest.rawValue)!, completion: { (data, error) -> Void in
            if let urlData = data {
                success(dData:urlData)
            }
        })
    }
    

    
  class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
    print("entrando a getCatalogWithSuccess")
    let session = NSURLSession.sharedSession()
    
    // Use NSURLSession to get data from an NSURL
    let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
        print("entrando a completion handler")

        if let responseError = error {
            print("responseError \(responseError.debugDescription)")
            completion(data: nil, error: responseError)
        } else if let httpResponse = response as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                let statusError = NSError(domain: Domain.DomainReverse.rawValue, code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
    
                print("statusError \(statusError.debugDescription)")
                completion(data: nil, error: statusError)
            } else {
                completion(data: data, error: nil)
            }
        }
    })
    loadDataTask.resume()
  }
}


//
//  DestinationInfoStruct.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/11/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//

import Foundation

struct DestinationInfoStruct {
    var apparment: String?
    var buildingNumber: String?
    var countryCode: String?
    var countryName: String?
    var email: String?
    var locality: String?
    var name: String?
    var postalCode: String?
    var stateCode: String?
    var stateName: String?
    var street: String?
    var taxId: String?
    var town: String?
    var destinationId: String?
    var clientId: String?

}
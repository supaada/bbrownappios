//
//  PriceListImplantSystem.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 27/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(PriceListImplantSystem)
class PriceListImplantSystem: NSManagedObject {
    struct PriceListImplantSystemProperties {
        static let implantSystemFilter = "implantSystem.code == %@"
    }

    @NSManaged var price: NSNumber
    @NSManaged var implantSystem: ImplantSystem
    @NSManaged var priceList: PriceList

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(PriceListImplantSystem))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            //var arrayString =  split(item as! String) {$0 == "|"}
            let dictionary = item as! NSDictionary

            let implantSystem: ImplantSystem? = ImplantSystem.getElementById(dictionary["implantSystem"] as! String)
            let priceList: PriceList? = PriceList.getElementById(dictionary["priceList"] as! String)
            
            if implantSystem != nil && priceList != nil {
                let newRow: PriceListImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(PriceListImplantSystem)) as! PriceListImplantSystem
                newRow.implantSystem = implantSystem!
                newRow.priceList = priceList!
                newRow.price = NSNumber(double: (dictionary["priceList"] as! String).doubleValue)
            }
        }
        
        CoreDataHelper.saveContext()
        print("Price List Implant System")
    }
    
    class func saveDataV(){
        let implants = ImplantSystem.getAllElementsT()
        for item in implants {
            let implantType: ImplantSystem = item as! ImplantSystem
            validateRelationship(implantType, keyS:"CASPAR", keyI:"TARGON")
            validateRelationship(implantType, keyS:"DCI", keyI:"LOW")
            validateRelationship(implantType, keyS:"CESPAR", keyI:"PLACA")
            validateRelationship(implantType, keyS:"CCR", keyI:"DHS")
            validateRelationship(implantType, keyS:"SCA", keyI:"TORNILLO")
            validateRelationship(implantType, keyS:"EQUIPO DE PODER", keyI:"PLACA")
            validateRelationship(implantType, keyS:"S4", keyI:"TARGON")
            validateRelationship(implantType, keyS:"COFLEX", keyI:"TORNILLO")
            validateRelationship(implantType, keyS:"LUMBAR  ADICIONAL", keyI:"PLASMACUP")
        }
        
        CoreDataHelper.saveContext()
        print("Relationship Price List Implant")
    }
    
    class func saveDataD(){
        let implants = ImplantSystem.getAllElementsT()
        for item in implants {
            let implantType: ImplantSystem = item as! ImplantSystem
            validateRelationship(implantType, keyS:"CASPAR", keyI:"CASPAR")
            validateRelationship(implantType, keyS:"DCI", keyI:"DCI")
            validateRelationship(implantType, keyS:"CESPAR", keyI:"CESPAR")
            validateRelationship(implantType, keyS:"CCR", keyI:"CCR")
            validateRelationship(implantType, keyS:"SCA", keyI:"SCA")
            validateRelationship(implantType, keyS:"EQUIPO DE PODER", keyI:"EQUIPO DE PODER")
            validateRelationship(implantType, keyS:"S4", keyI:"S4")
            validateRelationship(implantType, keyS:"COFLEX", keyI:"COFLEX")
            validateRelationship(implantType, keyS:"LUMBAR ADICIONAL", keyI:"LUMBAR ADICIONAL")
        }
        
        CoreDataHelper.saveContext()
        print("Relationship Price List Implant")
    }
    
    class func validateRelationship(implant: ImplantSystem, keyS:String, keyI:String){
        //print("validateRelationship code =\(implant.code), keyS = \(keyS), keyI =\(keyS)")

        if implant.code.uppercaseString.rangeOfString(keyS) != nil{
            
            let prices = PriceList.getAllElementsT()
            for price in prices {
                let p = price as! PriceList
                //print("p.name \(p.name)")
                if p.name.uppercaseString.rangeOfString(keyI) != nil{
                    //print("si tienes datos keyS = \(keyS), keyS = \(keyS)")
                    saveInfo(p, implantSystem: implant)
                }
            }
        }
    }
    
    class func saveInfo(priceList:PriceList, implantSystem: ImplantSystem){
        let newRow: PriceListImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(PriceListImplantSystem)) as! PriceListImplantSystem
        newRow.implantSystem = implantSystem
        newRow.priceList = priceList
        
        newRow.price = NSNumber(double: 10000)

        
    }
    
    class func getAllPriceListByImplantSystem(implantSystemCode: String) -> NSMutableArray{
        let prices: NSMutableArray = NSMutableArray()
        var priceDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(PriceListImplantSystem), filter: PriceListImplantSystemProperties.implantSystemFilter, value: implantSystemCode)
        
        for item in array{
            priceDic = NSMutableDictionary()
            
            priceDic!.setValue((item as! PriceListImplantSystem).priceList.name, forKey: "description")
            priceDic!.setValue((item as! PriceListImplantSystem).priceList.code, forKey: "code")
            priceDic!.setValue((item as! PriceListImplantSystem).price, forKey: "detail")
            
            prices.addObject(priceDic!)
        }
        
        return prices
    }
    
    
}

//
//  ImplantSystem.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(ImplantSystem)
class ImplantSystem: NSManagedObject {
    struct ImplantSystemProperties {
        static let idFilter = "code == %@"
    }

    @NSManaged var availabilityStatus: String
    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var updateDateTime: NSDate
    @NSManaged var priceList: NSSet
    @NSManaged var schedule: NSSet
    @NSManaged var surgeryType: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ImplantSystem))
    }
    
    class func saveDataV(array: NSArray){
        for item in array {
            let dictionary: NSDictionary = item as! NSDictionary
            let newRow: ImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(ImplantSystem)) as! ImplantSystem
            newRow.code = dictionary["code"] as! String
            newRow.name = dictionary["name"] as! String
            newRow.availabilityStatus = "Disponible"//dictionary["availabilityStatus"] as! String
        }
        CoreDataHelper.saveContext()
        //print("Implant System saved")
    }

    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss a"
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        
        for item in array {
            
            let dictionary: NSDictionary = item as! NSDictionary
            //var arrayString =  split(item as! String) {$0 == "|"}
            let newRow: ImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(ImplantSystem)) as! ImplantSystem
            let updateDate:String = dictionary["updateDate"] as! String
            let updateTime:String = dictionary["updateTime"] as! String
            let dateTimeString: String = "\(updateDate) \(updateTime)"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            let newString = dateTimeString.stringByReplacingOccurrencesOfString(".", withString: "-")
            
            newRow.updateDateTime = dateFormatter.dateFromString(newString)!
            newRow.code = dictionary["code"] as! String
            newRow.name = dictionary["name"] as! String
            newRow.availabilityStatus = dictionary["availabilityStatus"] as! String
        }
        CoreDataHelper.saveContext()
        //print("Implant System saved")
    }
    
    class func getElementById(code: String) -> ImplantSystem?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(ImplantSystem), filter: ImplantSystemProperties.idFilter, value: code)
        let element: ImplantSystem? = array.last as? ImplantSystem
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let implantSystems: NSMutableArray = NSMutableArray()
        var implantSystemDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(ImplantSystem))
        
        for item in array{
            implantSystemDic = NSMutableDictionary()
         
            let flag: Bool = (item as! ImplantSystem).availabilityStatus == "1" || (item as! ImplantSystem).availabilityStatus == "true"  || (item as! ImplantSystem).availabilityStatus.uppercaseString == "NO DISPONIBLE"
            //print("flag implat \(flag)")
            implantSystemDic!.setValue((item as! ImplantSystem).name, forKey: "description")
            implantSystemDic!.setValue((item as! ImplantSystem).code, forKey: "code")
            implantSystemDic!.setValue(false ? GeneralData.Messages.unavailable: GeneralData.Messages.available, forKey: "detail")
//            implantSystemDic!.setValue(flag ? false : true, forKey: "flag")
            implantSystemDic!.setValue(false, forKey: "flag")
          
            implantSystems.addObject(implantSystemDic!)
        }
        
        return implantSystems
    }
    
    class func getAllElementsT() -> NSArray{
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(ImplantSystem))
        
        return array
    }
}

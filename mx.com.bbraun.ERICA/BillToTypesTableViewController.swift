//
//  BillToTypesTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol BillToTypesDelegate{
    func userDidSelectBillToType(idBillTo: Int, title: String)
}

class BillToTypesTableViewController: UITableViewController {
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "BillToCell"
        }
    }

    var patientName: String?
    var patientRFC: String?
    var idBillTo: Int?
    var billToTypes: Array<BillTo>?
    var delegate: BillToTypesDelegate?
    var destinataryValidClient: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }

    override func didReceiveMemoryWarning() {
        didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if billToTypes != nil {
            return billToTypes!.count
        }else{
            return 0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) 
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        cell.textLabel?.text = billToTypes![indexPath.row].title
        
        if idBillTo != nil {
            if idBillTo == billToTypes![indexPath.row].id {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        idBillTo = billToTypes![indexPath.row].id
        let titleBillTo: String = billToTypes![indexPath.row].title
        
        //self.unselectItems()
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        delegate?.userDidSelectBillToType(idBillTo!, title: titleBillTo)
        goOut()
    }
    
    
    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }

    // MARK: - Business Functions
    
    func initView(){
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        
        billToTypes = [BillTo.getBillToById("2")!, BillTo.getBillToById("3")!]
        if destinataryValidClient == true {
            billToTypes?.append(BillTo.getBillToById("1")!)
        }
        tableView.reloadData()
    }
    
    func goOut(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func unselectItems(){
        for indexRow in 0 ..< self.billToTypes!.count{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}

//
//  RelationshipSurgeryTypeInstrumentalSet.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(RelationshipSurgeryTypeInstrumentalSet)
class RelationshipSurgeryTypeInstrumentalSet: NSManagedObject {
    struct RelationshipSurgeryTypeInstrumentalSetProperties {
        static let surgeryTypeFilter = "surgeryType.number == %@"
    }

    @NSManaged var instrumentalSet: InstrumentalSet
    @NSManaged var surgeryType: SurgeryType

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(RelationshipSurgeryTypeInstrumentalSet))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            var arrayString =  (item as! String).characters.split {$0 == "|"}.map { String($0) }
            let surgeryType: SurgeryType? = SurgeryType.getElementById(arrayString[0])
            let instrumentalSet: InstrumentalSet? = InstrumentalSet.getElementById(arrayString[1])
            
            if surgeryType != nil && instrumentalSet != nil {
                let newRow: RelationshipSurgeryTypeInstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(RelationshipSurgeryTypeInstrumentalSet)) as! RelationshipSurgeryTypeInstrumentalSet
                
                newRow.surgeryType = surgeryType!
                newRow.instrumentalSet = instrumentalSet!
            }
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func saveDataV(instrumentalSetArray:NSArray){
        let surgeryTypes = SurgeryType.getAllElementsT()
        for item in surgeryTypes {
            let surgeryType: SurgeryType = item as! SurgeryType
            let surgeryCode = surgeryType.number
            for set in instrumentalSetArray {
                let setCode = (set as! NSDictionary)["code"] as! String;
                //println("setCode = \(setCode)")
                var setCodeC  = setCode
                if setCode.rangeOfString(" ") != nil {
                    var arr = setCode.characters.split {$0 == " "}.map { String($0) }
                    setCodeC = arr[0]
                }
                //println("setCodeC = \(setCodeC)")
                //println("surgeryCode = \(surgeryCode)")
                //println("surgeryCode == setCode = \(surgeryCode.rangeOfString(setCodeC) != nil)")
                    
                if surgeryCode.rangeOfString(setCodeC) != nil {
                    if let instrumentalSet: InstrumentalSet = InstrumentalSet.getElementById(setCode){
                        saveInfo(instrumentalSet, surgery: surgeryType)
                    }
                }
            }
        }
    }

    
    class func saveDataD(){
        let surgeryTypes = SurgeryType.getAllElementsT()
        for item in surgeryTypes {
            let surgeryType: SurgeryType = item as! SurgeryType
            validateRelationship(surgeryType, keyS:"CASPAR", keyI:"CASPAR")
            validateRelationship(surgeryType, keyS:"DCI", keyI:"DCI")
            validateRelationship(surgeryType, keyS:"CESPAR", keyI:"CEPACE")
            validateRelationship(surgeryType, keyS:"CCR", keyI:"CCR")
            validateRelationship(surgeryType, keyS:"SCA", keyI:"SCA")
            validateRelationship(surgeryType, keyS:"EQUIPO DE PODER", keyI:"HP")
            validateRelationship(surgeryType, keyS:"S4", keyI:"S4")
            validateRelationship(surgeryType, keyS:"COFLEX", keyI:"COFLEX")
            validateRelationship(surgeryType, keyS:"LUMBAR  ADICIONAL", keyI:"SLA")
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func validateRelationship(surgery: SurgeryType, keyS:String, keyI:String){
        if surgery.name.uppercaseString.rangeOfString(keyS) != nil{
            let instrumentalSets = InstrumentalSet.getAllElementsT()
            for set in instrumentalSets {
                let instrumentalSet = set as! InstrumentalSet
                if instrumentalSet.name.uppercaseString.rangeOfString(keyI) != nil{
                    saveInfo(instrumentalSet, surgery: surgery)
                }
            }
        }
    }
    
    
    class func saveInfo(set:InstrumentalSet, surgery: SurgeryType){
        let newRow: RelationshipSurgeryTypeInstrumentalSet = CoreDataHelper.getRowEntity(NSStringFromClass(RelationshipSurgeryTypeInstrumentalSet)) as! RelationshipSurgeryTypeInstrumentalSet
        newRow.surgeryType = surgery
        newRow.instrumentalSet = set
    }
    
    class func getAllElementsBySurgeryType(surgeryTypeCode: String) -> NSMutableArray{
        let implantSystems: NSMutableArray = NSMutableArray()
        var implantSystemDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(RelationshipSurgeryTypeInstrumentalSet), filter: RelationshipSurgeryTypeInstrumentalSetProperties.surgeryTypeFilter, value: surgeryTypeCode)
        
        for item in array{
            implantSystemDic = NSMutableDictionary()
            let flag: Bool = (item as! RelationshipSurgeryTypeInstrumentalSet).instrumentalSet.availabilityStatus == "1" || (item as! RelationshipSurgeryTypeInstrumentalSet).instrumentalSet.availabilityStatus == "true"
            
            implantSystemDic!.setValue((item as! RelationshipSurgeryTypeInstrumentalSet).instrumentalSet.name, forKey: "description")
            implantSystemDic!.setValue((item as! RelationshipSurgeryTypeInstrumentalSet).instrumentalSet.code, forKey: "code")
            implantSystemDic!.setValue(flag ? GeneralData.Messages.available : GeneralData.Messages.unavailable, forKey: "detail")
            implantSystemDic!.setValue(flag, forKey: "flag")
            
            implantSystems.addObject(implantSystemDic!)
        }
        
        return implantSystems
    }
}

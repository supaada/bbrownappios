//
//  RelationshipSurgeryTypeImplantSystem.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(RelationshipSurgeryTypeImplantSystem)
class RelationshipSurgeryTypeImplantSystem: NSManagedObject {
    struct RelationshipSurgeryTypeImplantSystemProperties {
        static let surgeryTypeFilter = "surgeryType.number == %@"
    }

    @NSManaged var implantSystem: ImplantSystem
    @NSManaged var surgeryType: SurgeryType

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(RelationshipSurgeryTypeImplantSystem))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            var arrayString =  (item as! String).characters.split {$0 == "|"}.map { String($0) }
            let surgeryType: SurgeryType? = SurgeryType.getElementById(arrayString[0])
            let implantSystem: ImplantSystem? = ImplantSystem.getElementById(arrayString[1])
            
            if surgeryType != nil && implantSystem != nil {
                let newRow: RelationshipSurgeryTypeImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(RelationshipSurgeryTypeImplantSystem)) as! RelationshipSurgeryTypeImplantSystem
                
                newRow.surgeryType = surgeryType!
                newRow.implantSystem = implantSystem!
            }
        }
        
        CoreDataHelper.saveContext()
    }

    class func saveDataV(implantArray:NSArray){
        let surgeryTypes = SurgeryType.getAllElementsT()
        for item in surgeryTypes {
            let surgeryType: SurgeryType = item as! SurgeryType
            let surgeryCode = surgeryType.number
            for implant in implantArray {
                let implantCode = (implant as! NSDictionary)["code"] as! String;
                //println("setCode = \(setCode)")
                var implantCodeC  = implantCode
                if implantCode.rangeOfString(" ") != nil {
                    var arr = implantCode.characters.split {$0 == " "}.map { String($0) }
                    implantCodeC = arr[0]
                }
                //println("setCodeC = \(setCodeC)")
                //println("surgeryCode = \(surgeryCode)")
                //println("surgeryCode == setCode = \(surgeryCode.rangeOfString(setCodeC) != nil)")
                
                if surgeryCode.rangeOfString(implantCodeC) != nil {
                    if let implantSystem: ImplantSystem = ImplantSystem.getElementById(implantCode){
                        saveInfo(implantSystem, surgery: surgeryType)
                    }
                }
            }
        }
    }
    
    class func saveDataD(){
        let surgeryTypes = SurgeryType.getAllElementsT()
        for item in surgeryTypes {
            let surgeryType: SurgeryType = item as! SurgeryType
            validateRelationship(surgeryType, keyS:"CASPAR", keyI:"CASPAR")
            validateRelationship(surgeryType, keyS:"DCI", keyI:"DCI")
            validateRelationship(surgeryType, keyS:"CESPAR", keyI:"CEPACE")
            validateRelationship(surgeryType, keyS:"CCR", keyI:"CCR")
            validateRelationship(surgeryType, keyS:"SCA", keyI:"SCA")
            validateRelationship(surgeryType, keyS:"EQUIPO DE PODER", keyI:"HP")
            validateRelationship(surgeryType, keyS:"S4", keyI:"S4")
            validateRelationship(surgeryType, keyS:"COFLEX", keyI:"COFLEX")
            validateRelationship(surgeryType, keyS:"LUMBAR  ADICIONAL", keyI:"SLA")
        }
        
        CoreDataHelper.saveContext()
        print("RelationshipSurgeryType")
    }
    
    class func validateRelationship(surgery: SurgeryType, keyS:String, keyI:String){
        if surgery.name.uppercaseString.rangeOfString(keyS) != nil{
            let implantSystems = ImplantSystem.getAllElementsT()
            for implant in implantSystems {
                let imp = implant as! ImplantSystem
                if imp.name.uppercaseString.rangeOfString(keyI) != nil{
                        saveInfo(imp, surgery: surgery)
                }
            }
        }
    }


    class func saveInfo(implant:ImplantSystem, surgery: SurgeryType){
        let newRow: RelationshipSurgeryTypeImplantSystem = CoreDataHelper.getRowEntity(NSStringFromClass(RelationshipSurgeryTypeImplantSystem)) as! RelationshipSurgeryTypeImplantSystem
        newRow.surgeryType = surgery
        newRow.implantSystem = implant

    }

    class func getAllElementsBySurgeryType(surgeryTypeCode: String) -> NSMutableArray{
        let implantSystems: NSMutableArray = NSMutableArray()
        var implantSystemDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(RelationshipSurgeryTypeImplantSystem), filter: RelationshipSurgeryTypeImplantSystemProperties.surgeryTypeFilter, value: surgeryTypeCode)
        
        for item in array{
            implantSystemDic = NSMutableDictionary()
            let flag: Bool = (item as! RelationshipSurgeryTypeImplantSystem).implantSystem.availabilityStatus == "1" || (item as! RelationshipSurgeryTypeImplantSystem).implantSystem.availabilityStatus == "true"
            
            implantSystemDic!.setValue((item as! RelationshipSurgeryTypeImplantSystem).implantSystem.name, forKey: "description")
            implantSystemDic!.setValue((item as! RelationshipSurgeryTypeImplantSystem).implantSystem.code, forKey: "code")
            implantSystemDic!.setValue(flag ? GeneralData.Messages.available : GeneralData.Messages.unavailable, forKey: "detail")
            implantSystemDic!.setValue(flag, forKey: "flag")
            
            implantSystems.addObject(implantSystemDic!)
        }
        
        return implantSystems
    }
}

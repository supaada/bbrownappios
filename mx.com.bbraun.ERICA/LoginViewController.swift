//
//  LoginViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goFrontViewController" {
            let splitViewController: UISplitViewController = segue.destinationViewController as! UISplitViewController
            
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
            
            splitViewController.delegate = splitViewController
        }
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if txtPassword == textField{
            login()
        }
        
        return true;
    }
    
    @IBAction func enterUserTapped(sender: AnyObject) {
        login()
    }
    
    // MARK - Business Functions
    func initView(){
        txtUser.delegate = self
        txtPassword.delegate = self
    }
    
    func login(){
        let userName = txtUser.text
        let password = txtPassword.text
        //let user: Usuario? = Usuario.getUserInfo()
        
        // Verificamos que no esten vacios
        if(userName!.isEmpty){
            // Se muestra mensaje de error
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Usuario")
            return
        }
        
        if(password!.isEmpty){
            //Se muestra mensaje de error
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Password")
            return
        }
        
        //Validar con la BD
        if userName == "erica" && password == "erica"{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            goMain()
        
        }else{
            // Se muestra mensaje de error
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "No existe usuario registrado en el dispositivo")
            return
        }
        
        /*if user != nil {
            if userName != user!.nombre {
                // Se muestra mensaje de error
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "El Usuario es incorrecto")
                return
            }
            
            if password != user!.password {
                // Se muestra mensaje de error
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "La Contraseña es incorrecta")
                return
            }
        }else{
            // Se muestra mensaje de error
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "No existe usuario registrado en el dispositivo")
            return
        }*/
        
        //Dejamos accesar a la aplicacion
       
    }
    
    func goMain(){
        self.performSegueWithIdentifier("goFrontViewController", sender: nil)
    }
}

//
//  CreateRequestSurgeryTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

enum SumaryCell:String {
    case SurgerySumary = "SurgerySumaryCell", DestinatarySumary = "DestinatarySumaryCell", BillToSumary = "BillToSumaryCell", PendingsSumary = "PendingsSumaryCell", NotesSumary = "NotesSumaryCell", None
    init(position:Int){
        switch(position) {
        case 0:
            self = SurgerySumary
        case 1:
            self = DestinatarySumary
        case 2:
            self = BillToSumary
        case 3:
            self = PendingsSumary
        case 4:
            self = NotesSumary
        default:
            self = None
        }
    }
    
    func getSectionTitle() -> String? {
        switch(self) {
        case .SurgerySumary:
            return "TIPO DE CIRUGÍA"
        case .DestinatarySumary:
            return "DESTINO DE ENTREGA"
        case .BillToSumary:
            return "DATOS DE FACTURACIÓN"
        case .PendingsSumary:
            return "PENDIENTES"
        case .NotesSumary:
            return "INGRESE OBSERVACIONES:"
        case .None:
            return nil
        }
    }
    
    func getRowHeight() -> CGFloat {
        switch(self) {
        case .SurgerySumary, .DestinatarySumary, .BillToSumary:
            return 50.0
        case .PendingsSumary:
            return 60.0
        case .NotesSumary:
            return 100.0
        case .None:
            return 44.0
        }
    }
    
}

class CreateRequestSurgeryTableViewController: UITableViewController, ContactRequestSurgeryDelegate, SurgeryTypeDelegate {
    struct TableView {
        struct SegueIdentifiers {
            static let SurgeryTypeSegue = "goSurgeryType"
            static let ContactSegue = "goContact"
            static let PatientInfoSegue = "goPatientInfo"
            static let DepositsRatesSegue = "goDepositsRates"
            static let CommentsSegue = "goComments"
            static let SearchOrder = "goSearchOrders"
        }
        
        static let Titles: [String] = ["Crear Solicitud de Cirugía", "Solicitud"]
    }
    
    var showOrderDetail: Bool?
    
    // Vistas Singleton
    var surgeryTypeController: SurgeryTypeTableViewController?
    var contactRequestSurgeyController: ContactRequestSurgeryTableViewController?
    
    // Estructuras singleton
    var surgeryTypeStruct: SurgeryTypeViewStruct?
    var contactRequestSurgeyStruct: ContactRequestSurgeryViewStruct?
    var billInfo: PatientInfoViewStruct?
    
    var comments: String?
    var recordDateTime: NSDate?
    var id: NSNumber?
    var sections: Int?

    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - Table View Delegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SumaryCell.init(position: indexPath.section).getRowHeight()
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 3 {
            if let surgeryTypeStruct = surgeryTypeStruct, pendings = surgeryTypeStruct.pendings where pendings.count > 0 {
                return SumaryCell.init(position: section).getSectionTitle()
            } else {
                return SumaryCell.NotesSumary.getSectionTitle()
            }
        }
        
        return SumaryCell.init(position: section).getSectionTitle()

    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let surgeryTypeStruct = surgeryTypeStruct, pendings = surgeryTypeStruct.pendings where pendings.count > 0 {
            sections = sections ?? 4 + 1
        }
        
        return sections ?? 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = getAndConfigureCell(indexPath)
        return cell
    }
    
    func getAndConfigureCell(indexPath: NSIndexPath) -> UITableViewCell{
            switch (indexPath.section){
            case 0:
                guard let surgerySumarycell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.SurgerySumary.rawValue) as? SurgerySumaryUITableViewCell, surgeryTypeStruct = surgeryTypeStruct else {
                    return UITableViewCell()
                }
                var surgeryType = ""
                if let surgeryName = surgeryTypeStruct.surgeryTypeName, surgeryFamily = surgeryTypeStruct.surgeryFamilyTypeName {
                    surgeryType = "\(surgeryName) - \(surgeryFamily)"
                }
                surgerySumarycell.config(surgeryType, surgeryDate: surgeryTypeStruct.surgeryDateTime, surgerySet: surgeryTypeStruct.surgerySetName, technicalInstrumentalist: surgeryTypeStruct.technicalRequired)
                return surgerySumarycell
            case 1:
                guard let destinatarySumaryCell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.DestinatarySumary.rawValue) as? DestinataryUITableViewCell, contactRequestSurgeyStruct = contactRequestSurgeyStruct else { return UITableViewCell() }
                destinatarySumaryCell.config(contactRequestSurgeyStruct.doctorName, shipmentType: contactRequestSurgeyStruct.shipmentType, destinatary: contactRequestSurgeyStruct.customerName, destinataryId: contactRequestSurgeyStruct.customerNumber)
                return destinatarySumaryCell
            case 2:
                guard let billToSumaryCell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.BillToSumary.rawValue) as? BillToTableViewCell, contactRequestSurgeyStruct = contactRequestSurgeyStruct else { return UITableViewCell() }
                billToSumaryCell.config(contactRequestSurgeyStruct.otherCustomerName ?? "Sin Datos Fiscales", clientId: contactRequestSurgeyStruct.billToTitle ?? "Sin Facturar a", paymentType: contactRequestSurgeyStruct.transactionNumber ?? "Sin Depósito")
                return billToSumaryCell
            case 3:
                guard let surgeryTypeStruct = surgeryTypeStruct, pendings = surgeryTypeStruct.pendings where pendings.count > 0 else {
                    guard let notesSumaryCell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.NotesSumary.rawValue) as? NotesTableViewCell else { return UITableViewCell() }
                    if let comments = comments {
                        notesSumaryCell.config(comments)
                    } else {
                        notesSumaryCell.configDelegate()
                    }
                    notesSumaryCell.delegate = self
                    return notesSumaryCell
                }
                if let pendingsSumaryCell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.PendingsSumary.rawValue) as? PendingsSumaryTableViewCell {
                    pendingsSumaryCell.config(pendings)
                    pendingsSumaryCell.delegate = self
                    return pendingsSumaryCell
                }
            case 4:
                guard let notesSumaryCell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.NotesSumary.rawValue) as? NotesTableViewCell else { return UITableViewCell() }
                notesSumaryCell.delegate = self
                if let comments = comments {
                    notesSumaryCell.config(comments)
                } else {
                    notesSumaryCell.configDelegate()
                }
                return notesSumaryCell
            default: break
            }
        
        return UITableViewCell()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;

        
        switch (indexPath.section) {
        case 0 :
            performSegueWithIdentifier(TableView.SegueIdentifiers.SurgeryTypeSegue, sender: nil)
        case 1,2:
            performSegueWithIdentifier("goContact", sender: nil)
        default: break
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == TableView.SegueIdentifiers.SurgeryTypeSegue {
            let viewController = segue.destinationViewController as! SurgeryTypeTableViewController
            
            viewController.showOrderDetail = showOrderDetail
            viewController.showButtons = false
            viewController.structVar = self.surgeryTypeStruct
            viewController.firstTime = false
            viewController.delegate = self
        }else if segue.identifier == TableView.SegueIdentifiers.ContactSegue {
            let viewController = segue.destinationViewController as! ContactRequestSurgeryTableViewController
            
            viewController.showOrderDetail = showOrderDetail
            viewController.showButtons = false
            viewController.firstTime = false
            viewController.structVar = contactRequestSurgeyStruct
            viewController.patientInfoStruct = billInfo
            viewController.delegate = self
        }else if segue.identifier == TableView.SegueIdentifiers.SearchOrder {
            let splitViewController: UISplitViewController = segue.destinationViewController as! UISplitViewController
            
            let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
            
            navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
            
            splitViewController.delegate = splitViewController

        }
    }
    
    // MARK: - Business Functions
    
    func initView(){
        setNavigationButtons()
        setSwipes()
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func setSwipes() {
        
        let swipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeCreateOrder:")
        swipeViewNext.direction = .Left
        navigationController?.navigationBar.addGestureRecognizer(swipeViewNext)
        
        let secondSwipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeCreateOrder:")
        secondSwipeViewNext.direction = .Left
        tableView.addGestureRecognizer(secondSwipeViewNext)
    }
    
    func respondToSwipeCreateOrder(gesture:UISwipeGestureRecognizer){
        confirmCreateOrder()
    }
    
    func loadData(){
        setValues()
        tableView.reloadData()
    }
    
    func setNavigationButtons(){
        if showOrderDetail == false {
            var buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "confirmCreateOrder")
            navigationItem.rightBarButtonItems = [buttonItem]
            
            buttonItem = UIBarButtonItem(image: UIImage(named: "ic_option_cancel"), style: UIBarButtonItemStyle.Plain, target: self, action: "closeTapped")
            navigationItem.leftBarButtonItems = [buttonItem]
        }
    }
    
    func setValues(){
        setStructValue()
        setValuesFromStruct()
    }
    
    func setStructValue(){
        if showOrderDetail == true && id != nil && surgeryTypeStruct == nil{
            let order: OrderSurgery? = OrderSurgery.getOrder(self.id!)
            print("order \(order)")
            if order != nil {
                setValuesFromEntity(order!)
            }
        }
    }
    
    func setValuesFromEntity(order: OrderSurgery){
        
        billInfo = SurgeryRequestBusiness.getValuePatientInfoStruct(order.patientInfo)
        contactRequestSurgeyStruct = SurgeryRequestBusiness.getValueContactRequestSurgeryStruct(order)
        surgeryTypeStruct = SurgeryRequestBusiness.getValueSurgeryTypeStruct(order)
        comments = order.comments
        print("order comments \(order.comments)")
    }
    
    func setValuesFromStruct(){
        //setValueSurgerySumaryView(surgeryTypeStruct)
        //setValueContactRequestSurgerySumaryView(contactRequestSurgeyStruct)
        //setValueCommentsView(comments)
    }
    
    func setValueCommentsView(comments: String?){
        if let comments = comments {
            if let cell = tableView.dequeueReusableCellWithIdentifier(SumaryCell.NotesSumary.rawValue) as? NotesTableViewCell {
                cell.config(comments)
            } else {
                print("dequeue false")
            }
        }
    }
    
    func setValueSurgerySumaryView(surgerySumary: SurgeryTypeViewStruct?){
        if let surgerySumary = surgerySumary {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? SurgerySumaryUITableViewCell {
                cell.config("\(surgerySumary.surgeryTypeName) - \(surgerySumary.surgeryFamilyTypeName)", surgeryDate: surgerySumary.surgeryDateTime, surgerySet: surgerySumary.surgerySetName, technicalInstrumentalist: surgerySumary.technicalRequired)
            }
        }
    }
    
    func setValueSurgeryTypeView(surgeryTypeName: String?){
        if surgeryTypeName != nil {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell?.detailTextLabel?.text = surgeryTypeName
        }
    }
    
    func setValueContactRequestSurgerySumaryView(contactRequestSurgeyStruct: ContactRequestSurgeryViewStruct?){
        if let contactRequestSurgeyStruct = contactRequestSurgeyStruct {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 1)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell?.detailTextLabel?.text = contactRequestSurgeyStruct.customerName
        }
    }
    
    func setValueContactRequestSurgeryView(customerName: String?){
        if customerName != nil {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 1)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell?.detailTextLabel?.text = customerName
        }
    }
    
    func confirmCreateOrder(){
        
        if let surgeryTypeStruct = surgeryTypeStruct, pendings = surgeryTypeStruct.pendings where pendings.count > 0 {
            if let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 4), cell = tableView.cellForRowAtIndexPath(indexPath) as? NotesTableViewCell {
                cell.resignNotesFirstResponder()
            }
        } else {
            if let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 3), cell = tableView.cellForRowAtIndexPath(indexPath) as? NotesTableViewCell {
                cell.resignNotesFirstResponder()
            }
        }
        
        SCLAlertView().showConfirm(GeneralData.AlertTitles.confirmTitle, subTitle: "¿Son Correctos los datos de la Solicitud de Cirugía?", buttonAcceptTarget: self, buttonAcceptSelector: "createOrder")
    }
    
    func createOrder(){
        print("si vine la imagen \(contactRequestSurgeyStruct?.image)")
        SwiftSpinner.show("Creando la Solicitud de Cirugía")
        
        if let surgeryTypeStruct = surgeryTypeStruct, pendings = surgeryTypeStruct.pendings where pendings.count > 0 {
            if let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 4), cell = tableView.cellForRowAtIndexPath(indexPath) as? NotesTableViewCell {
                comments = cell.getNotes()
            }
        } else {
            if let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 3), cell = tableView.cellForRowAtIndexPath(indexPath) as? NotesTableViewCell {
                comments = cell.getNotes()
            }
        }
        
        dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
            if self.surgeryTypeStruct?.statistics == true {
                self.surgeryTypeStruct?.statusId = 3
            } else {
                self.surgeryTypeStruct?.statusId = GeneralData.Status.created
            }
            let result: NSMutableDictionary = SurgeryRequestBusiness.createOrder(self.surgeryTypeStruct!, contactRequestSurgery: self.contactRequestSurgeyStruct, patientInfo: self.billInfo, comments: self.comments)
                
            if result.valueForKey("errorMessage") == nil {
                if result.valueForKey("warningMessage") == nil {
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        SwiftSpinner.delay(seconds: 0.8, completion: {
                            self.showOrderInfo(1, message: "Solicitud creada Satisfactoriamente")
                        })
                    }
                    
                }else{
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        SwiftSpinner.delay(seconds: 0.8, completion: {
                            self.showOrderInfo(2, message: "Solicitud creada Satisfactoriamente pero sin Enviar a los Sistemas Remotos")
                        })
                    }
                }
            }else{
                let errorMessage: String = result.valueForKey("errorMessage") as! String
                
                if errorMessage.uppercaseString.indexOf("WARNING") >= 0{
                    var arrayMessage = errorMessage.characters.split {$0 == "|"}.map { String($0) }
                    
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        SwiftSpinner.delay(seconds: 0.4, completion: {
                            self.showOrderInfo(2, message: arrayMessage[1])
                        })
                    }
                }else{
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        SwiftSpinner.delay(seconds: 0.4, completion: {
                            self.showOrderInfo(3, message: errorMessage)
                        })
                    }
                }
            }
        }
    }
    
    func showOrderInfo(success: Int, message: String){
        let alert: SCLAlertView = SCLAlertView()
        
        alert.addButton("Aceptar"){
            alert.hideView()
            self.goOut()
        }
        
        if success == 1 {
            alert.showSuccess(GeneralData.AlertTitles.addOrderTitle, subTitle: message, showDoneButton: false)
        }else if success == 2 {
            alert.showWarning(GeneralData.AlertTitles.addOrderTitle, subTitle: message, showDoneButton: false)
        }else{
            alert.showError(GeneralData.AlertTitles.errorTitle, subTitle: message, showDoneButton: false)
        }
    }
    
    func closeTapped() {
        SCLAlertView().showConfirm(GeneralData.AlertTitles.addOrderTitle, subTitle: "Esta apunto de perder los datos asignados hasta ahora. ¿Desea continuar?", buttonAcceptTarget: self, buttonAcceptSelector: "goOut")
    }
    
    func goOut(){
        performSegueWithIdentifier(TableView.SegueIdentifiers.SearchOrder, sender: nil)
    }
    
    // MARK: - Business Delegate Functions
    func userDidEnterPatienInfo(structVar: PatientInfoViewStruct?) {
        billInfo = structVar
    }
    
    func userDidEnterContactRequestSurgery(structVar: ContactRequestSurgeryViewStruct?) {
        contactRequestSurgeyStruct = structVar
    }
    
    func userDidEnterSurgeryType(structVar: SurgeryTypeViewStruct?) {
        surgeryTypeStruct = structVar
    }
}

extension CreateRequestSurgeryTableViewController: NotesSumaryDelegate {
    func addNotesSumaryDelegate(text: String) {
        comments = text
        print("comments \(comments)")
    }
}

extension CreateRequestSurgeryTableViewController: PendingButtonDelegate {
    func pendingPressed(pending: PendingEnum) {
        switch pending {
        case .OffLine:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .ComponentsInventoryConfirmation:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .InstrumentalSetAgendaConfirmation:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .BillInfoConfirmation:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .PaymentConfirmation:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .SAPConfirmation:
            SCLAlertView().showWarning(GeneralData.AlertTitles.pendingTitle, subTitle: pending.getMessage())
        case .None: break
        }
    }
}



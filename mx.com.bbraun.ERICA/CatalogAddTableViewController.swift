//
//  CatalogAddTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/17/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol AddDoctorTableViewControlerDelegate {
    func addDoctor(doctorName:String)
}

public protocol CatalogNewDelegate{
    func userDidSelectElementNew(code: String, description: String)
}

class CatalogAddTableViewController: UITableViewController,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    struct TableView {
        struct CellIdentifiers {
            static let CellANew = "CatalogCellANew"
            static let CellBNew = "CatalogCellBNew"
            static let CellCNew = "CatalogCellCNew"
        }
        static let CatalogTitles: [String] = ["Tipo de Cirugía","Familia de Set de Cirugía","Set de Cirugía", "Sistemas de Implantes", "Sets de Instrumental", "Técnicos", "Representante de Ventas", "Precio de Lista", "Lugar de Entrega", "Doctor", "Tipo de Envío", "Facturar a ", "Cliente a Facturar", "Ninguno"]
            
        struct Predicate {
            static let DescriptionCode: String = "description CONTAINS[cd] %@ OR code CONTAINS[cd] %@"
            static let Description: String = "description CONTAINS[cd] %@"
        }
            
    }
        
    var idCatalog: Int?
    var elements: NSArray?
    var filteredElements: NSArray?
    var filterString: String?
    var code: String?
    var delegate: CatalogNewDelegate?
    var cell: UITableViewCell?
    var name: String?
    var detail: AnyObject?
    var firstTime:Bool?
    let alert = SCLAlertView()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            initView()
        }
        
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(true)
            loadData()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        // MARK: - Table view data source
        
        override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            //if section == 1 {
                if let elements = filteredElements {
                    return elements.count
                }else{
                    return 0
                }
            //}
            //return 1
        }
        
        override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
                let cell = getAndConfigureCell(indexPath, element: filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary)
            
                // this is where you set your color view
                let customColorView: UIView = UIView()
            
                customColorView.backgroundColor = GeneralData.Appearance.thirdColor
                cell.selectedBackgroundView =  customColorView;
            
                if let codePresent = code, filtered = filteredElements, element = filtered.objectAtIndex(indexPath.row) as? NSMutableDictionary, let codeElement = element.valueForKey("code") as? String where codePresent == codeElement{
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
            
                return cell
        }
        
        override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            let element: NSDictionary = filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary
            code = element.valueForKey("code") as? String
            let description: String = element.valueForKey("description") as! String
            detail = element.valueForKey("detail")
            name = description
            self.cell = cell
            
            if let idCatalogPresent = idCatalog {
                switch (CatalogPosition.init(rawValue: idCatalogPresent)){
                case .IdDoctorCatalog:
                    acceptItem(cell, description: description, detail: detail)
                default: break
                }
            }
        }
        
        override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            if let idCatalogPresent = idCatalog {
                switch (CatalogPosition.init(rawValue: idCatalogPresent)){
                    case .IdDoctorCatalog:
                    return 35.0
                    default: break
                }
            }
            return 44.0
        }
        
        // MARK: - Search Bar
        
        func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
            var predicate: NSPredicate?
            
            if let idCatalogPresent = idCatalog {
                switch (CatalogPosition.init(rawValue: idCatalogPresent)){
                case .IdDoctorCatalog:
                    predicate = NSPredicate(format: TableView.Predicate.Description, searchText)
                default: break
                }
            }
            
            if predicate != nil {
                filteredElements = searchText.isEmpty ? elements : NSArray(array: elements!.filteredArrayUsingPredicate(predicate!))
                tableView.reloadData()
            }
        }
        
        // MARK: - Butons Actions
        
        @IBAction func cancelTapped(sender: AnyObject) {
            goOut()
        }
        
        // MARK: - Business Functions
        
        func initView(){
            setNavigationButtons()
            title = TableView.CatalogTitles[idCatalog!]
            searchBar.delegate = self
            tableView.tableFooterView = UIView(frame: CGRectZero)
            
        }
    
    func setNavigationButtons(){
        /*if firstTime == true {
            var buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_next"), style: UIBarButtonItemStyle.Plain, target: self, action: "goNextScreen")
                
                self.navigationItem.rightBarButtonItems = [buttonItem]
                
                buttonItem = UIBarButtonItem(image: UIImage(named: "ic_option_exit"), style: UIBarButtonItemStyle.Plain, target: self, action: "goExit")
                
                self.navigationItem.rightBarButtonItems?.append(buttonItem)
        }else{*/
        
        let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addDoctorAlert")
        /*let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptInfo")*/
                
        navigationItem.rightBarButtonItems = [button]
        //}
    }
    
    func addDoctorAlert(){
        // Add a text field
       
        let txt = alert.addTextField("Nombre del Doctor")
        txt.delegate = self
        alert.addButton("Aceptar") {
            if let name = txt.text where !name.isEmpty {
                self.alert.hideView()
                self.delegate?.userDidSelectElementNew(name.uppercaseString, description: name.uppercaseString)
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
        alert.addButton("Cancelar") {
            self.alert.hideView()
            self.dismissViewControllerAnimated(true, completion: nil)
        }

        alert.showEdit("Alta", subTitle: "Ingrese el nombre del Doctor",showDoneButton: false)
    }
        
        func loadData(){
            elements = getElementsByCatalog(idCatalog)
            filteredElements = elements
            tableView.reloadData()
            unselectItems()
        }
        
        func getElementsByCatalog(idCatalog: Int?) -> NSArray? {
            var elements: NSArray?
            
            if let idCatalogPresent = idCatalog {
                switch (CatalogPosition.init(rawValue: idCatalogPresent)){
                    case .IdDoctorCatalog:
                    elements = Doctor.getAllElements()
                default: break
                }
            }
            return elements
        }
        
        func goOut(){
            dismissViewControllerAnimated(true, completion: nil)
        }
        
        func getAndConfigureCell(indexPath: NSIndexPath, element: NSDictionary) -> UITableViewCell{
            
            if let idCatalogPresent = idCatalog {
                switch (CatalogPosition.init(rawValue: idCatalogPresent)){
                    case .IdDoctorCatalog:
                    let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.CellCNew, forIndexPath: indexPath) as! CatalogCellCNew
                    if let description = element.valueForKey("description") as? String {
                        cell.configure(description)
                    }
                    return cell
                default: break
                }
            }
            return UITableViewCell()
        }
        
        func acceptItemTapped(){
            if let cellPresent = cell, namePresent = name {
                acceptItem(cellPresent, description: namePresent, detail: detail)
            }else{
                goOut()
            }
        }
    
        func acceptItem(cell: UITableViewCell, description: String, detail: AnyObject?){
            unselectItems()
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            if let cod = code {
                delegate?.userDidSelectElementNew(cod, description: description)
            }
            goOut()
        }
    
        func unselectItems(){
            let numRows: Int = tableView.numberOfRowsInSection(0);
            
            for indexRow in 0 ..< numRows{
                let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
                let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
                cell?.accessoryType = UITableViewCellAccessoryType.None
            }
        }
}

extension CatalogAddTableViewController: AddDoctorTableViewControlerDelegate {
    func addDoctor(doctorName: String) {
        delegate?.userDidSelectElementNew(doctorName, description: doctorName)
        goOut()
    }
}


extension CatalogAddTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if let name = textField.text where !name.isEmpty {
            textField.resignFirstResponder()
            self.alert.hideView()
            self.delegate?.userDidSelectElementNew(name.uppercaseString, description: name.uppercaseString)
            self.dismissViewControllerAnimated(true, completion: nil)
            return true
        }
        return false
    }
}

class CatalogCellNew: UITableViewCell{
    
    @IBOutlet weak var doctorNameTextField: UITextField!
    var indexPath:NSIndexPath!
    var addDoctorDelegate:AddDoctorTableViewControlerDelegate?

    
    @IBAction func createDoctorPressed(sender: UIButton) {

        if let doctorName = doctorNameTextField.text where doctorName != "" {
            addDoctorDelegate?.addDoctor(doctorName)
        } else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre del Doctor")
        }
    }
    
    func configure(indexPath: NSIndexPath){
       self.indexPath = indexPath
    }
}
class CatalogCellCNew: UITableViewCell{
    var indexPath:NSIndexPath!
    @IBOutlet weak var descriptionLabel: UILabel!
    func configure(description: String){
        descriptionLabel.text = description
    }
}

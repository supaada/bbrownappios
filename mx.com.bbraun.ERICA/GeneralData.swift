//
//  GeneralData.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit

class GeneralData{
    struct Erros {
        static let ERROR: String = "ERROR"
        static let INVALIDATE_USR_ERROR: String = "-001"
        static let MAX_NUM_ATTEMPTS: Int = 10
    }
    
    struct AlertTitles {
        static let errorTitle: String = "Error"
        static let confirmTitle: String = "Pregunta"
        static let infoTitle: String = "Informativo"
        
        static let addOrderTitle: String = "Creación de Solicitud"
        static let addProductsTitle: String = "Agregar Articulos"
        static let quoteOrderTitle: String = "Cotización"
        static let searchOrderTitle: String = "Busqueda de Pedido"
        static let createOrderTitle: String = "Creación"
        static let orderFoundTitle: String = "Pedido encontrado"
        static let pendingTitle: String = "Pendiente"

        
        static let syncCatalogTitle: String = "Catálogos"
        static let syncOrdersTitle: String =  "Pedidos"
        
        static let inventoryTitle: String = "Inventario"
    }
    
    struct Patterns {
        static let passwordMiddlePattern: String = "[a-z0-9_-]{6,8}"
        static let passwordHighPattern: String = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,8}"
        static let taxIdPeople:String="[A-Z]{3,4}[0-9]{6}[A-Z0-9]{2}[0-9]{1}"
    }
    
    struct Appearance {
        static let mainColor: UIColor = GeneralData.UIColorFromRGB(0x00B482)
        static let secondColor: UIColor = GeneralData.UIColorFromRGB(0x40C7A1)
        static let thirdColor: UIColor = GeneralData.UIColorFromRGB(0x73D6BA)
        static let fourthColor: UIColor = GeneralData.UIColorFromRGB(0xCCF0E6)
        
        static let mainColorText: UIColor = GeneralData.UIColorFromRGB(0x464646)
        static let secondColorText: UIColor = GeneralData.UIColorFromRGB(0x464646)
        static let thirdColorText: UIColor = GeneralData.UIColorFromRGB(0x6E6E6E)
        
        static let redColorText: UIColor = GeneralData.UIColorFromRGB(0xC5004B)
        static let orangeColorText: UIColor = GeneralData.UIColorFromRGB(0xE57228)
    }
    
    struct Messages {
        static let available = "DISPONIBLE"
        static let unavailable = "NO DISPONIBLE"
        static let description = "Descripción"
        static let availability = "Disponibiliad"
    }
    
    struct TextButtons {
        static let accept = "Aceptar"
        static let reject = "Rechazar"
        static let cancel = "Cancelar"
        static let save = "Guardar"
    }
    
    struct Status {
        static let offline: NSNumber = 0
        static let created: NSNumber = 1
        static let rejected: NSNumber = 2
        static let pendingSAP: NSNumber = 3
        static let noBill: NSNumber = 4
        static let noDeposit: NSNumber = 5
    }
    
    class func UIColorFromRGB(rgbColor: UInt) -> UIColor{
        let color: UIColor = UIColor(
            red: CGFloat((rgbColor & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbColor & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbColor & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
        
        return color
    }
    
    class func containsMatch(pattern: String, inString string: String) -> Bool {
        let regex = try? NSRegularExpression(pattern: pattern, options: [])
        let range = NSMakeRange(0, string.characters.count)
        return regex?.firstMatchInString(string, options: [], range: range) != nil
    }
    
    class func containsPasswordMatch(string: String) -> Bool {
        return containsMatch(Patterns.passwordHighPattern, inString: string)
    }
    
    class func containsRFCMatch(string: String) -> Bool {
        return containsMatch(Patterns.taxIdPeople, inString: string)
    }
}
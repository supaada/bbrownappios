//
//  TimeIntervalScheduleTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol TimeIntervalScheduleDelegate{
    func userDidSelectTimeIntervalSyncCatalogs(timeIntervalIndex: Int)
    func userDidSelectTimeIntervalSyncOrders(timeIntervalIndex: Int)
}

class TimeIntervalScheduleTableViewController: UITableViewController {
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "TimeIntervalCell"
        }
        
        static let SynchronizationTitles: [String] = ["Sincronzación de Catálogos", "Sincronzación de Ordenes"]
    }
    
    var idSynchronization: Int?
    var index: Int?
    var timeIntervalScheduleIndex: Int?
    var timeIntervals: Array<TimeInterval>?
    var delegate: TimeIntervalScheduleDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.timeIntervals != nil {
            return self.timeIntervals!.count
        }else{
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) 

        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        cell.textLabel?.text = self.timeIntervals![indexPath.row].title
        
        if self.timeIntervalScheduleIndex != nil {
            if self.timeIntervalScheduleIndex == indexPath.row {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell = self.tableView.cellForRowAtIndexPath(indexPath)!
        self.index = indexPath.row
        
        self.unselectItems()
        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.goOut()
    }
    
    @IBAction func acceptTapped(sender: AnyObject) {
        if self.index != nil && self.timeIntervals != nil {
            if self.idSynchronization == 0 {
                self.delegate?.userDidSelectTimeIntervalSyncCatalogs(self.index!)
            }else{
                self.delegate?.userDidSelectTimeIntervalSyncOrders(self.index!)
            }
        }
        
        self.goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.title = TableView.SynchronizationTitles[self.idSynchronization!]
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        self.tableView.reloadData()
    }
    
    func goOut(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func unselectItems(){
        for indexRow in 0 ..< self.timeIntervals!.count{
            let indexPath: NSIndexPath = NSIndexPath(forRow: indexRow, inSection: 0)
            let cell: UITableViewCell = self.tableView.cellForRowAtIndexPath(indexPath)!
            
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}

//
//  TimeInterval.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit



class TimeInterval {
    struct TimeIntervalSeconds {
        static let byHalfHour: NSTimeInterval = 1800.0
        static let byHour: NSTimeInterval = 3600.0
        static let byTwoHours: NSTimeInterval = 7200.0
        static let byFourHours: NSTimeInterval = 14400.0
        static let byEightHours: NSTimeInterval = 28800.0
        static let byDay: NSTimeInterval = 86400.0
        
        static let timeIntervals: [TimeInterval] = [ TimeInterval(title: "Cada media hora", timeInterval: TimeInterval.TimeIntervalSeconds.byHalfHour),
            TimeInterval(title: "Cada hora", timeInterval: TimeInterval.TimeIntervalSeconds.byHour),
            TimeInterval(title: "Cada dos horas", timeInterval: TimeInterval.TimeIntervalSeconds.byTwoHours),
            TimeInterval(title: "Cada cuatro horas", timeInterval: TimeInterval.TimeIntervalSeconds.byFourHours),
            TimeInterval(title: "Cada ocho horas", timeInterval: TimeInterval.TimeIntervalSeconds.byEightHours),
            TimeInterval(title: "Diariamente", timeInterval: TimeInterval.TimeIntervalSeconds.byDay) ]
    }
    
    let title: String
    let timeInterval: NSTimeInterval
    
    init(title: String, timeInterval: NSTimeInterval) {
        self.title = title
        self.timeInterval = timeInterval
    }
    
    class func allMainTimeIntervals() -> Array<TimeInterval> {
        return TimeIntervalSeconds.timeIntervals
    }
}
//
//  CalendarDayCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class CalendarDayCell : RDVCalendarDayCell{
    let width: CGFloat = 16
    let height: CGFloat = 16
    let color: UIColor = GeneralData.Appearance.orangeColorText
    
    var dotMarker: CVCircleView?
    
    override init(reuseIdentifier: String!) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let frame = CGRectMake(0, 0, width, height)
        
        self.dotMarker = CVCircleView(frame: frame, color: color, _alpha: 1)
        self.dotMarker!.hidden = true
        self.contentView.addSubview(self.dotMarker!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let viewSize: CGSize = self.contentView.frame.size
        self.dotMarker!.center = CGPointMake(viewSize.width/2,viewSize.height - 6)
        self.selectedBackgroundView.backgroundColor = GeneralData.Appearance.fourthColor
        self.textLabel.textColor = GeneralData.Appearance.mainColorText
        self.textLabel.highlightedTextColor = GeneralData.Appearance.secondColorText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.dotMarker?.hidden = true
    }
}
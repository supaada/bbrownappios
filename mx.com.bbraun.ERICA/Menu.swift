//
//  Menu.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/24/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import UIKit

public enum MenuSegue:String {
    case OrdersSegue = "sw_front_ord", SynchronizeSegue = "sw_front_syn", SearchSegue = "sw_front_sea", NewOrderSegue = "sw_front_new_ord", LogoutSegue = "sw_front_act", None
    public init(rawValue: String){
        switch(rawValue){
        case OrdersSegue.rawValue:
            self = OrdersSegue
        case SynchronizeSegue.rawValue:
            self = SynchronizeSegue
        case SearchSegue.rawValue:
            self = SearchSegue
        case NewOrderSegue.rawValue:
            self = NewOrderSegue
        case LogoutSegue.rawValue:
            self = LogoutSegue
        default:
            self = None
        }
    }
    
    func getStruct() -> MenuStruct {
        switch (self){
        case .SynchronizeSegue:
            return MenuStruct(title: "Sincronizar", description: "Catálogos y Pedidos Pendientes", image: UIImage(named: "ic_menu_sincronizar"), forward: self.rawValue)
        case .SearchSegue:
            return MenuStruct(title: "Consultas", description: "Solicitudes de Cirugia", image: UIImage(named: "ic_menu_consultas"), forward: self.rawValue)
        case .NewOrderSegue:
            return MenuStruct(title: "Solicitud de Cirugía", description: "Nueva Solicitud de Cirugía", image: UIImage(named: "ic_menu_pedidos"), forward: self.rawValue)
        case .LogoutSegue:
            return MenuStruct(title: "Salir", description: "Cierra sesion", image: UIImage(named: "ic_menu_salir"), forward: self.rawValue)
        default:
            return MenuStruct(title: "", description: "", image: nil, forward: "")
        }
    }
}

class Menu {
    
    class func allMainMenu() -> Array<MenuStruct> {
            return [MenuSegue.NewOrderSegue.getStruct(), MenuSegue.SearchSegue.getStruct(), MenuSegue.SynchronizeSegue.getStruct()/*,MenuSegue.LogoutSegue.getStruct()*/]
    }
    
}
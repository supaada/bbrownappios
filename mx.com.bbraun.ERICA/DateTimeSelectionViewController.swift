//
//  DateTimeSelectionViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol DateTimeSelectionDelegate{
    func userDidSelectDateTime(date: String, time: String)
}

class DateTimeSelectionViewController: UIViewController {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    var isPopover: Bool = false
    var isOrder: Bool = false
    var idCatalog:Int = 1
    var code:String = "0"
    var name:String = "Instrumental CASPAR Solo Intrumental"
    var delegate: DateTimeSelectionDelegate?
    var delegatePending: SurgeryRequestPendingDelegate?
    var delegateStatistics: SurgeryRequestStatisticsDelegate?
    var delegateDismiss: DismissSurgeryRequestDelegate?
    let array:NSMutableArray = []
    var datePrev:NSDate?
    var datePrevString:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation

    
    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
    
    @IBAction func datePickerChangedTapped(sender: AnyObject) {
        if let datePrevString = datePrevString {
            dateFormatter.dateFormat = "HH:mm"
            let timeString = getStringDate(datePicker.date)
            dateLabel.text = "\(datePrevString) \(timeString)"
        } else {
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            dateLabel.text = dateFormatter.stringFromDate(datePicker.date)
        }
    }
    
    @IBAction func datePickedTapped(sender: AnyObject) {
        dateFormatter.dateFormat = "HH:mm"
        let timeString = getStringDate(datePicker.date)
        print(" timeString \(timeString), ")
        if let datePrevString = datePrevString {
            print(" datePrevString \(datePrevString) ")
            delegate?.userDidSelectDateTime(datePrevString, time: timeString)
        } else {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dateString = getStringDate(datePicker.date)
            print(" dateString \(dateString) ")
            delegate?.userDidSelectDateTime(dateString, time: timeString)
        }
        
        goOut()
        
    }

    // MARK: - Business Functions
    
    func initView(){
    }
    
    func loadData(){
            //SwiftSpinner.show("Creando la Orden")
        if isOrder {
            /*dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
                SurgeryRequestBusiness.loadAgendaInventory("4",array: self.array)
                //TODO agregar notificacion
            }*/
        }
        
        if let datePrevString = datePrevString {
            dateLabel.text = datePrevString
        } else {
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            dateLabel.text = dateFormatter.stringFromDate(NSDate())
        }
    }
    
    func goOut(){
        if isOrder {
            dismissViewControllerAnimated(true, completion: nil)
        } else if isPopover {
            navigationController?.popViewControllerAnimated(true)
        } else {
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func getStringDate(date: NSDate) -> String{
        let strDate: String = dateFormatter.stringFromDate(date)
        
        return strDate
    }
}

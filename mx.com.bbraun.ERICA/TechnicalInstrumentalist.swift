//
//  TechnicalInstrumentalist.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 05/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(TechnicalInstrumentalist)
class TechnicalInstrumentalist: NSManagedObject {
    struct TechnicalInstrumentalistProperties {
        static let idFilter = "number == %@"
    }

    @NSManaged var dateTime: NSDate
    @NSManaged var hospital: String
    @NSManaged var name: String
    @NSManaged var number: String
    @NSManaged var status: String
    @NSManaged var schedule: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(TechnicalInstrumentalist))
    }
    
    class func saveData(array: NSArray){
        let dateFormatter: NSDateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
        dateFormatter.dateFormat = "dd\\/MM\\/yyyy HH:mm:ss a"
        for item in array {
            let dictionary: NSDictionary = item as! NSDictionary
            let updateDate:String = dictionary["updateDate"] as! String
            let updateTime:String = dictionary["updateTime"] as! String
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            let dateTimeString: String = "\(updateDate) \(updateTime)"
            if let dateTimeF = dateFormatter.dateFromString(dateTimeString){
                let newRow: TechnicalInstrumentalist = CoreDataHelper.getRowEntity(NSStringFromClass(TechnicalInstrumentalist)) as! TechnicalInstrumentalist
                newRow.dateTime = dateTimeF
                newRow.number = dictionary["code"] as! String
                newRow.name = dictionary["name"] as! String
                newRow.hospital = dictionary["hospital"] as! String
                newRow.status = dictionary["availabilityStatus"] as! String
            }
        }
        print(" TechnicalInstrumentalist saved")
        CoreDataHelper.saveContext()
    }
    
    class func getElementById(code: String) -> TechnicalInstrumentalist?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(TechnicalInstrumentalist), filter: TechnicalInstrumentalistProperties.idFilter, value: code)
        let element: TechnicalInstrumentalist? = array.last as? TechnicalInstrumentalist
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        let technicals: NSMutableArray = NSMutableArray()
        var technicalDic: NSMutableDictionary?
        
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(TechnicalInstrumentalist))
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        for item in array{
            technicalDic = NSMutableDictionary()
            
            technicalDic!.setValue((item as! TechnicalInstrumentalist).name, forKey: "description")
            technicalDic!.setValue((item as! TechnicalInstrumentalist).number, forKey: "code")
//            technicalDic!.setValue(dateFormatter.stringFromDate((item as! TechnicalInstrumentalist).dateTime), forKey: "detail")
            technicalDic!.setValue(false, forKey: "flag")
            technicalDic!.setValue(false ? GeneralData.Messages.unavailable: GeneralData.Messages.available, forKey: "detail")

//            technicalDic!.setValue((item as! TechnicalInstrumentalist).status == "0" || (item as! TechnicalInstrumentalist).status == "false", forKey: "flag")

            
            technicals.addObject(technicalDic!)
        }
        
        return technicals
    }
    
    class func getAllElementsBis() -> NSMutableArray{
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        let technicals: NSMutableArray = NSMutableArray()
        var technicalDic: NSMutableDictionary?
        
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(TechnicalInstrumentalist))
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        for item in array{
            technicalDic = NSMutableDictionary()
            let flag: Bool = (item as! TechnicalInstrumentalist).status == "1" || (item as! TechnicalInstrumentalist).status == "true"
            
            technicalDic!.setValue((item as! TechnicalInstrumentalist).name, forKey: "description")
            technicalDic!.setValue((item as! TechnicalInstrumentalist).number, forKey: "code")
            technicalDic!.setValue(true ? GeneralData.Messages.available : GeneralData.Messages.unavailable, forKey: "detail")
            technicalDic!.setValue(true, forKey: "flag")
//            technicalDic!.setValue(flag ? false : true, forKey: "flag")

            
            technicals.addObject(technicalDic!)
        }
        
        return technicals
    }
}

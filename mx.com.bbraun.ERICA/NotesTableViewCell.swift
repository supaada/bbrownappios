//
//  NotesTableViewCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/7/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol NotesSumaryDelegate {
    func addNotesSumaryDelegate(text:String)
}

class NotesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notesTextView: UITextView!
    var delegate: NotesSumaryDelegate?
    
    func config (text:String){
        notesTextView.text = text
        configDelegate()
    }
    
    func configDelegate() {
        notesTextView.delegate = self
    }
    
    func getNotes() -> String {
        return notesTextView.text
    }
    
    func resignNotesFirstResponder(){
        notesTextView.resignFirstResponder()
    }
}

extension NotesTableViewCell: UITextViewDelegate {
    override func resignFirstResponder() -> Bool {
        return true
    }
}

//
//  DepositRateTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit
import Photos

protocol DepositRateDelegate{
    func userDidEnterDepositRateInfo(bankName: String, depositDate: String, referenceNumber: String, transactionNumber: String, image:UIImage?)
}

class DepositRateTableViewController: UITableViewController, UITextFieldDelegate, DateTimeSelectionDelegate {
    struct TableView {
        struct SegueIdentifiers {
            static let DateTimeSelectionSegue = "goDateTimeSelection2"
        }
    }
    
    @IBOutlet weak var textBankName: UITextField!
    @IBOutlet weak var labelDepositDate: UILabel!
    @IBOutlet weak var textReferenceNumber: UITextField!
    @IBOutlet weak var textTransactionNumber: UITextField!
    @IBOutlet weak var billImageView: UIImageView!
    
    var image: UIImage?
    
    var isPopover: Bool = false
    var showOrderDetail: Bool = false
    var delegate: DepositRateDelegate?
    
    var bank: String?
    var depositDate: String?
    var reference: String?
    var transaction: String?
    var assetCollection: PHAssetCollection?
    var albumFound : Bool = false
    var photosAsset: PHFetchResult?
    var assetThumbnailSize:CGSize?
    var collection: PHAssetCollection?
    var assetCollectionPlaceholder: PHObjectPlaceholder?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;
        if indexPath.row == 4 {
            pickPhoto()
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == TableView.SegueIdentifiers.DateTimeSelectionSegue {
            let controller: DateTimeSelectionViewController = segue.destinationViewController as! DateTimeSelectionViewController
            
            controller.delegate = self
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if self.showOrderDetail {
            return false
        }
        
        return true
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textReferenceNumber.isFirstResponder() {
            print("reference")
            textTransactionNumber.becomeFirstResponder()
            return false
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if self.showOrderDetail == true{
            return false
        }
        
        return true
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.setNavigationButtons()
        self.textBankName.delegate = self
        self.textReferenceNumber.delegate = self
        self.textTransactionNumber.delegate = self
        billImageView.hidden = true
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }

    
    func loadData(){
        self.setValues()
        
        self.tableView.reloadData()
    }
    
    func setNavigationButtons(){
        if self.showOrderDetail == false {
            let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptDepositTapped")
            let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Camera, target: self, action: "takePhotoTapped")

            self.navigationItem.rightBarButtonItems = [buttonItem,button]
        }
    }
    
    func setValues(){
        self.setStructValue()
        self.setInReadMode()
    }
    
    func setStructValue(){
        if self.bank != nil {
            self.textBankName.text = self.bank
        }
        
        if self.depositDate != nil {
            self.labelDepositDate.text = self.depositDate
        }
        
        if self.reference != nil {
            self.textReferenceNumber.text = self.reference
        }
        
        if self.transaction != nil {
            self.textTransactionNumber.text = self.transaction
        }
    }
    
    func setInReadMode(){
        if showOrderDetail == true {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 1, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            self.textBankName.enabled = false
            self.textReferenceNumber.enabled = false
            self.textTransactionNumber.enabled = false
        }
    }
    
    func goOut(){
        if isPopover {
            dismissViewControllerAnimated(true, completion: nil)
        }else{
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func acceptDepositTapped() {
        saveDepositRateInfo()
    }
    
    func takePhotoTapped() {
        pickPhoto()
    }
    
    func saveDepositRateInfo(){
        if isValidFields() {
            delegate?.userDidEnterDepositRateInfo(textBankName.text!, depositDate: labelDepositDate.text!, referenceNumber: textReferenceNumber.text!, transactionNumber: textTransactionNumber.text!, image: image)
            goOut()
        }
    }
    
    func isValidFields() -> Bool{
        guard let textBank = textBankName.text where !textBank.isEmpty else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Nombre del Banco")
            return false
        }
        guard let textDate = labelDepositDate.text where !textDate.isEmpty && textDate != "dd/mm/aaaa" else {
        //if self.labelDepositDate.text == nil || self.labelDepositDate.text!.isEmpty || self.labelDepositDate.text! == "dd/mm/aaaa" {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar la Fecha de Deposito")
            return false
        }
        if (textReferenceNumber.text != nil || textTransactionNumber.text != nil) {
            
            guard let textReference = textReferenceNumber.text, let textTransaction = textTransactionNumber.text where !textReference.isEmpty || !textTransaction.isEmpty else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Número Referencia o Número de Movimiento")
                return false
            }
        } else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar Número Referencia o Número de Movimiento")
            return false
        }
        
        
        if self.textTransactionNumber.text!.isEmpty {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar el Número de Movimiento")
            return false
        }
        
        return true
    }
    
    // MARK: - Business Delegate Functions
    
    func userDidSelectDateTime(date: String, time: String) {
        self.labelDepositDate.text = date
    }
    
    
}



extension DepositRateTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func pickPhoto() {
        if true || UIImagePickerController.isSourceTypeAvailable(.Camera) {
            showPhotoMenu()
        } else {
            choosePhotoFromLibrary()
        }
    }
    
    func takePhotoWithCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .Camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func choosePhotoFromLibrary() {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .PhotoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func showPhotoMenu() {
            let alertController = UIAlertController(title: nil, message: nil,
            preferredStyle: .ActionSheet)
            let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            let takePhotoAction = UIAlertAction(title: "Tomar una Foto",
            style: .Default, handler: {_ in self.takePhotoWithCamera()})
            alertController.addAction(takePhotoAction)
            let chooseFromLibraryAction = UIAlertAction(title:
            "Seleccionar de Librería", style: .Default, handler: {_ in self.choosePhotoFromLibrary()})
            alertController.addAction(chooseFromLibraryAction)
            presentViewController(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("picker camera = \(picker.sourceType)")
        image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        if let image = image {
            if picker.sourceType == UIImagePickerControllerSourceType.Camera {
                print("picked camera ")

                let writePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("photo.png")
                if let imagePNG = UIImagePNGRepresentation(image) {
                    
                    if imagePNG.writeToURL(writePath, atomically: true) {
                        if let convertedImage = UIImage(named:"photo.png") {
                            showImage(convertedImage)
                        } else {
                            print("convertedImage nil")
                            let imageBacked =  UIImage(data: imagePNG)
                            print("convertedImage imageBacked = \(imageBacked)")
                            showImage(imageBacked!)
                        }
                    } else { print("couldnt write imagePNG")}
                } else { print("imagePNG nil")}
            } else {
                print("convertedImage image = \(image)")

                print("picked photo library")
                showImage(image)
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if error == nil {
            showImage(image)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            presentViewController(ac, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) { dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showImage(image: UIImage) {
        billImageView.image = image
        billImageView.hidden = false
        billImageView.frame = CGRect(x: 10, y: 10, width: 600, height: 200)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Landscape, .Portrait]
    }
 
}

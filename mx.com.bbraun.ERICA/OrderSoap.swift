//
//  OrderSoap2.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 19/08/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class OrderSoap: NSObject, SOAPHelperDelegate {
    let ACTION_CREATE_WS_NAME = "crearPedido"
    let ACTION_QUOTE_WS_NAME = "cotizarPedido"
    let ACTION_SEARCH_WS_NAME = "consultarPedido"
    let ACTION_SYNCHRONIZE_WS_NAME = "sincronizarPedido"
    let PEDIDO_WS_NANE = "PedidoWS"
    
    var result:NSDictionary?
    var errorMessage:String = ""
    
    func quote(in0: String, in1: NSMutableArray){
        sendRequest(in0, in1: in1, actionWS: ACTION_QUOTE_WS_NAME)
    }
    
    func create(in0: String, in1: NSMutableArray){
        sendRequest(in0, in1: in1, actionWS: ACTION_CREATE_WS_NAME)
    }
    
    func synchronize(in0: String, in1: NSMutableArray){
        sendRequest(in0, in1: in1, actionWS: ACTION_SYNCHRONIZE_WS_NAME)
    }
    
    func sendRequest(in0: String, in1: NSMutableArray, actionWS: String) {
        if !ConfigWS().isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        
        var error:NSError?
        var soap = SOAPHelper()
        let url: String = ConfigWS().getUrl(PEDIDO_WS_NANE)
        
        soap.delegate = self
        
        soap.setValueParameter(in0, forKey: "in0")
        soap.setValueParameter(in1, forKey: "in1")
        
        var xmlDoc:AEXMLElement?
        do {
            xmlDoc = try soap.syncRequestURL(url,
                        soapAction: actionWS)
        } catch var error1 as NSError {
            error = error1
            xmlDoc = nil
        }
        
        if let err = error {
            self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): \(err.localizedDescription)"
            
            print(err.localizedDescription)
        }else{
            if xmlDoc != nil && xmlDoc!.children.count > 0 {
                var element: AEXMLElement = xmlDoc!["ns1:out"]
                
                if element.name != AEXMLElement.errorElementName {
                    setResult(element)
                }else{
                    element = xmlDoc!["faultstring"]
                    
                    if element.name != AEXMLElement.errorElementName {
                        let response: String = element.stringValue
                        
                        self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): \(response)"
                    }else{
                        self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): \(AEXMLElement.errorElementName)"
                    }
                }
            }else{
                self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): Respuesta Nula o Vacia"
            }
        }
    }
    
    /*func search(in0: String, in1: String) {
        if !ConfigWS().isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        
        var error:NSError?
        var soap = SOAPHelper()
        let url: String = ConfigWS().getUrl(PEDIDO_WS_NANE)
        
        soap.delegate = self
        
        soap.setValueParameter(in0, forKey: "in0")
        soap.setValueParameter(in1, forKey: "in1")
        
        var xmlDoc:AEXMLElement = soap.syncRequestURL(url,
            soapAction: ACTION_SEARCH_WS_NAME,
            error: &error)
        
        if let err = error {
            self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): \(err.localizedDescription)"
            
            println(err.localizedDescription)
        }else{
            
            if result != nil && result?.count > 0 {
                if let response: AnyObject? = result!["ns1:out"] as? NSDictionary{
                    if response != nil{
                        if let arrayOfString = response!["ns1:string"] as? NSArray{
                            if let infoOrder = arrayOfString[0] as? String {
                                if ValidityUtil.isErrorValidityActivate(infoOrder) {
                                    self.errorMessage = ValidityUtil.getErroMessage(infoOrder)
                                }else if infoOrder.indexOf("ERROR") >=  0{
                                    self.errorMessage = ValidityUtil.getGroupSellers(infoOrder)
                                }
                            }
                        }else if let message = response!["ns1:string"] as? String{
                            if ValidityUtil.isErrorValidityActivate(message) {
                                self.errorMessage = ValidityUtil.getErroMessage(message)
                            }else if message.indexOf("ERROR") >=  0{
                                self.errorMessage = ValidityUtil.getGroupSellers(message)
                            }
                        }
                    }
                }
            }else{
                self.errorMessage = "Falló la conexion del Web Service (\(self.PEDIDO_WS_NANE)): Respuesta Nula o Vacia"
            }
        }
    }*/
    
    internal func setResult(element: AEXMLElement){
        let children = element.children
        let mutableArray: NSMutableArray = NSMutableArray()
        var array: NSArray?
        var dictionary: NSDictionary?
        let response: String = children.first!.stringValue
        
        if ValidityUtil.isErrorValidityActivate(response) {
            self.errorMessage = ValidityUtil.getErroMessage(response)
        }else{
            for child in children {
                mutableArray.addObject(child.stringValue)
            }
            
            array = NSArray(array: mutableArray)
            dictionary = ["ns1:string" : array!]
            result = ["ns1:out" : dictionary!]
        }
    }
    
    //SOAPHelperDelegate
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeSendingURLRequest request: NSMutableURLRequest) -> NSMutableURLRequest {
        //println(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))
        
        return request
    }
    
    func soapHelper(soapHelper: SOAPHelper!, didBeforeParsingResponseString data: NSData) -> NSData {
        //println(NSString(data: data, encoding: NSUTF8StringEncoding))
        
        return data
    }
}
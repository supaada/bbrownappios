//
//  SearchingTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

enum SearchSegue:String {
    case DetailSurgerySegue = "goEditRequestSurgery"
}

class SearchingTableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet weak var segmentReport: ADVSegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    struct TableView {
        struct CellIdentifiers {
            static let ReportCell = "ReportCell"
            static let OrderCatalogCell = "OrdeGeneradaCell"
        }
        
        struct Predicate {
            static let Description: String = "id CONTAINS[cd] %@ OR folioOrder CONTAINS[cd] %@ OR patientName CONTAINS[cd] %@ OR surgeryType CONTAINS[cd] %@ OR customerName CONTAINS[cd] %@ OR doctorName CONTAINS[cd] %@ OR surgeryDateTime CONTAINS[cd] %@ "
        }
    }
    
    var elements: NSArray?
    var filteredElements: NSArray?

    
    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let results = filteredElements where results.count > 0 {
            return filteredElements!.count
        }else{
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let results = filteredElements where results.count > 0 {
            if segmentReport.selectedIndex == 0 || segmentReport.selectedIndex == 1 || segmentReport.selectedIndex == 2 || segmentReport.selectedIndex == 3 {
                let cell: OrderCatalogCell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.OrderCatalogCell, forIndexPath: indexPath) as! OrderCatalogCell
                cell.configure(filteredElements?.objectAtIndex(indexPath.row) as! NSDictionary)
                return cell
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("ClienteCell", forIndexPath: indexPath)
                if let element = filteredElements?.objectAtIndex(indexPath.row) as? NSDictionary, description = element.objectForKey("description") as? String, code = element.objectForKey("code") as? String {
                    cell.textLabel?.text = description
                    cell.detailTextLabel?.text = code
                    if let flag = element.objectForKey("flag") as? Bool where flag == false {
                        cell.textLabel?.textColor = GeneralData.Appearance.mainColorText
                        cell.detailTextLabel?.textColor = GeneralData.Appearance.secondColorText
                    } else{
                        cell.textLabel?.textColor = GeneralData.Appearance.redColorText
                        cell.detailTextLabel?.textColor = GeneralData.Appearance.redColorText
                    }
                }
                /*let cell: ReportCell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.ReportCell, forIndexPath: indexPath) as! ReportCell
                cell.configure(filteredElements?.objectAtIndex(indexPath.row) as! NSMutableDictionary)*/
                return cell
            }
        }else {
            return tableView.dequeueReusableCellWithIdentifier("EmptyResults") as UITableViewCell!
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let results = filteredElements where results.count > 0 {

            switch segmentReport.selectedIndex {
            case 0,1,2,3:
                performSegueWithIdentifier(SearchSegue.DetailSurgerySegue.rawValue, sender: filteredElements![indexPath.row])
            default:
                break
            }
        }
    }
    
    // MARK: - Search Bar
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if let results = filteredElements where results.count > 0{
            let predicate: NSPredicate? = NSPredicate(format: TableView.Predicate.Description, searchText, searchText)
            
            if predicate != nil {
                filteredElements = searchText.isEmpty ? elements : NSMutableArray(array: elements!.filteredArrayUsingPredicate(predicate!))
                tableView.reloadData()
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SearchSegue.DetailSurgerySegue.rawValue {
            let vc = segue.destinationViewController as! CreateRequestSurgeryTableViewController
            let element: NSMutableDictionary = sender as! NSMutableDictionary
            vc.id = element.valueForKey("id") as? NSNumber
            print("id \(vc.id)")
            vc.showOrderDetail = true
        }
    }
    
    func showElementsTapped(sender: AnyObject) {
        if segmentReport.selectedIndex == 0 {
            let buttonItemAccept: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_sincronizar"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptItemTapped")
            navigationItem.rightBarButtonItems = [buttonItemAccept]
        } else {
            navigationItem.rightBarButtonItems = []
        }
        displayElements(segmentReport.selectedIndex)
    }
    
    func acceptItemTapped(){
        if Reachability.isConnectedToNetwork() {
            
            SwiftSpinner.show("Enviando Pedidos Pendientes")
            
            /*dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
                let errorMessage: String = SynchronizeBusiness.synchronizeOrderRequest()
                
                dispatch_async(GDCUtil.GlobalMainQueue) {
                    SwiftSpinner.delay(seconds: 1, completion: {
                        SwiftSpinner.hide()
                        SCLAlertView().showSuccess(GeneralData.AlertTitles.syncOrdersTitle, subTitle: "Sincronización TERMINADA")
                    })
                }
            }*/
            SwiftSpinner.show("Sincronizando Solicitudes Pendientes")
            let error = SynchronizeBusiness.synchronizeOrderRequest()
            if error.isEmpty {
                SwiftSpinner.hide()
                SwiftSpinner.delay(seconds: 1.5, completion: {
                    SCLAlertView().showSuccess("Solicitudes", subTitle: "Sincronización concluyo Satisfactoriamente")
                    self.loadData()
                })
            } else {
                SwiftSpinner.hide()
                SCLAlertView().showError("Error", subTitle: "No se pudo sincronizar las Solicitudes")
            }
        } else {
            SCLAlertView().showError("Error", subTitle: "No cuenta con acceso a internet")

        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let results = filteredElements where results.count > 0 {
            switch (segmentReport.selectedIndex){
                case 0,1,2,3:
                    return 80.0
                default: break
            }
        }
        return 44.0
        
    }
    
    // MARK: - Bussines functions
    
    func initView(){
        segmentReport.items = ["Solicitudes", "Ordenes", "No Concretadas","Rechazadas"]//, "Clientes"]
        segmentReport.addTarget(self, action: "showElementsTapped:", forControlEvents: .ValueChanged)
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = GeneralData.Appearance.secondColor.CGColor
        searchBar.delegate = self
        
        clearsSelectionOnViewWillAppear = false
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        let buttonItemAccept: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_sincronizar"), style: UIBarButtonItemStyle.Plain, target: self, action: "acceptItemTapped")
        navigationItem.rightBarButtonItems = [buttonItemAccept]

    }
    
    func loadData(){
        elements = OrderSurgery.getAllPendingOrders()
        filteredElements = elements
        refreshInfo()
    }
    
    func displayElements(selectedIndex: Int){
        elements = getElements(selectedIndex)
        filteredElements = elements
        searchBar.text = ""
        
        refreshInfo()
    }
    
    func getElements(selectedIndex: Int) -> NSArray{
        switch (selectedIndex) {
        case 0:
            return OrderSurgery.getAllPendingOrders()
        case 1:
            return OrderSurgery.getAllCreatedOrders()
        case 2:
            return OrderSurgery.getAllStatisticOrders()
        case 3:
            return OrderSurgery.getAllRejectedOrders()
        default:
            return Customer.getAllElements()
        }
    }
    
    func refreshInfo(){
        self.tableView.reloadData()
    }
}

class ReportCell: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    func configure(element: NSMutableDictionary){
        descriptionLabel?.text = element.objectForKey("description") as? String
        codeLabel?.text = element.objectForKey("code") as? String
        detailLabel?.text = element.objectForKey("detail") as? String
        
        if let flag = element.objectForKey("flag") as? Bool where flag == false {
            descriptionLabel?.textColor = GeneralData.Appearance.mainColorText
            codeLabel?.textColor = GeneralData.Appearance.secondColorText
            detailLabel?.textColor = GeneralData.Appearance.secondColorText
        } else{
            descriptionLabel?.textColor = GeneralData.Appearance.redColorText
            codeLabel?.textColor = GeneralData.Appearance.redColorText
            detailLabel?.textColor = GeneralData.Appearance.redColorText
        }
    }
}


class OrderCatalogCell: UITableViewCell{
    
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var surgeryDateLabel: UILabel!
    @IBOutlet weak var surgerySetLabel: UILabel!
    @IBOutlet weak var orderFolio: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    
    func configure(order: NSDictionary){
        if let doctorName = order.valueForKey("doctorName") as? String {
            doctorNameLabel.text = doctorName
        }
        if let folioOrder = order.valueForKey("folioOrder") as? String {
            orderFolio.text = folioOrder
        }
        if let surgeryType = order.valueForKey("surgeryType") as? String {
            surgerySetLabel.text = surgeryType
        }
        if let surgeryDateTime = order.valueForKey("surgeryDateTime") as? String {
            surgeryDateLabel.text = surgeryDateTime
        }
        if let customerName = order.valueForKey("customerName") as? String {
            destinationLabel.text = customerName
        }
        
        preservesSuperviewLayoutMargins = false
        layoutMargins = UIEdgeInsetsZero
    }
}

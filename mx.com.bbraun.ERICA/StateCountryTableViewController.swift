//
//  StateCountryTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol StateCountryDelegate{
    func userDidSelectStateAndCountry(countryCode: String, countryName: String, stateCode: String, stateName: String)
}

class StateCountryTableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    struct TableView {
        struct CellIdentifiers {
            static let Cell = "StateCountryCell"
        }
        
        struct Predicate {
            static let StateName: String = "name CONTAINS[cd] %@"
        }
    }
    
    var countries: NSMutableArray?
    var filteredCountries: NSMutableArray?

    var countryCode: String?
    var stateCode: String?
    var delegate: StateCountryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.filteredCountries != nil {
            return self.filteredCountries!.count
        }
        
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filteredCountries != nil {
            let country: NSMutableDictionary = self.filteredCountries!.objectAtIndex(section) as! NSMutableDictionary
            let states: NSMutableArray? = country.valueForKey("states") as? NSMutableArray
            
            if states != nil {
                return states!.count
            }else{
                return 0
            }
        }
        
        return 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.Cell, forIndexPath: indexPath) 

        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell.selectedBackgroundView =  customColorView;
        
        if self.filteredCountries != nil {
            let country: NSMutableDictionary = self.filteredCountries!.objectAtIndex(indexPath.section) as! NSMutableDictionary
            let states: NSMutableArray? = country.valueForKey("states") as? NSMutableArray
            let state: NSMutableDictionary = states?.objectAtIndex(indexPath.row) as! NSMutableDictionary
            let stateName: String = state.valueForKey("name") as! String
            
            cell.textLabel?.text = stateName
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.filteredCountries != nil {
            let country: NSMutableDictionary = self.filteredCountries!.objectAtIndex(section) as! NSMutableDictionary
            let countryName: String = country.valueForKey("name") as! String
            
            return " " + countryName
        }
        
        return ""
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title: UILabel = UILabel()
        
        let titleStr: String? = self.tableView(tableView, titleForHeaderInSection: section)
        title.text = titleStr
        title.textColor = GeneralData.Appearance.mainColorText
        title.backgroundColor = GeneralData.Appearance.thirdColor
        title.font = UIFont.boldSystemFontOfSize(15)
        
        return title
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell = self.tableView.cellForRowAtIndexPath(indexPath)!
        let country: NSMutableDictionary = self.filteredCountries!.objectAtIndex(indexPath.section) as! NSMutableDictionary
        let states: NSMutableArray = country.valueForKey("states") as! NSMutableArray
        let state: NSMutableDictionary = states.objectAtIndex(indexPath.row) as! NSMutableDictionary
        print("why derrito \(delegate)  ?")
        self.delegate?.userDidSelectStateAndCountry(country.valueForKey("code") as! String, countryName: country.valueForKey("name") as! String, stateCode: state.valueForKey("code") as! String, stateName: state.valueForKey("name") as! String)
        self.goOut()
    }
    
    // MARK: - Search Bar
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if self.countries != nil {
            self.filteredCountries = searchText.isEmpty ? self.countries : self.filterStatesCountries(searchText)
                
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Butons Actions
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.goOut()
    }
    
    // MARK: - Business Functions
    
    func initView(){
        self.searchBar.delegate = self
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        self.countries = Country.getAllElements()
        self.filteredCountries = self.countries
        
        self.tableView.reloadData()
    }
    
    func filterStatesCountries(searchDescriptionText: String) -> NSMutableArray{
        let results: NSMutableArray = NSMutableArray()
        let countriesCopy: NSMutableArray? = NSKeyedUnarchiver.unarchiveObjectWithData(NSKeyedArchiver.archivedDataWithRootObject(self.countries!)) as? NSMutableArray
        
        if countriesCopy != nil {
            let predicate: NSPredicate = NSPredicate(format: TableView.Predicate.StateName, searchDescriptionText, searchDescriptionText)
            
            for itemC in countriesCopy! {
                let countryDic: NSMutableDictionary = itemC as! NSMutableDictionary
                var states: NSMutableArray = countryDic.valueForKey("states") as! NSMutableArray
                
                states = NSMutableArray(array: states.filteredArrayUsingPredicate(predicate))
                
                countryDic.setValue(states, forKey: "states")
                
                if states.count > 0 {
                    results.addObject(countryDic)
                }
            }
        }
        
        //results = Country.getElementsByStateDescription(searchDescriptionText)
        
        return results
    }
    
    func goOut(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

//
//  ContactRequestSurgeryTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol ContactRequestSurgeryDelegate {
    func userDidEnterPatienInfo(structVar: PatientInfoViewStruct?)
    func userDidEnterContactRequestSurgery(structVar: ContactRequestSurgeryViewStruct?)
}

class ContactRequestSurgeryTableViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var textPatient: UITextField!
    @IBOutlet weak var switchInfoBillTo: UISwitch!
    @IBOutlet weak var switchInfoDeposit: UISwitch!
    @IBOutlet weak var destinataryNameLabel: UILabel!
    @IBOutlet weak var destinataryDescriptionLabel: UILabel!
    
    struct TableView {
        struct SegueIdentifiers {
            static let CatalogSegue = "goCatalogSelection2"
            static let DoctorSegue = "goCreateDoctor"
            static let InfoPatientSegue = "goInfoPatient"
            static let InfoDepositSegue = "goInfoDeposit"
            static let BillToTypesSegue = "goBillToTypes"
            static let CreateOrderSegue = "goCreateOrder"
            static let CounterSaleSegue = "goCounterSale"
            static let CreateDestination = "goCreateDestination"
        }
        
        static let IdCustomerCatalog: Int = 4
        static let IdBillToCatalog: Int = 6
        static let IdShipmentTypeCatalog: Int = 8
        static let IdDoctorCatalog: Int = 10
        static let IdOtherCustomerCatalog: Int = 11
        
        static let IdBillToSameHospital = 1
        static let IdBillToPatient = 2
        static let IdBillToOtherCustomer = 3
        
        static let IdPaymentConditionCredit = 1
        static let IdPaymentConditionCash = 2
        
        static let reasonCreditCustomer: String = "Crédito"
    }
    
    var showOrderDetail: Bool?
    var showButtons: Bool?
    var firstTime: Bool = true
    var idBillTo: Int?
    var idPaymentCondition: Int?
    var folioOrder: String?
    var delegate: ContactRequestSurgeryDelegate?
    var destinataryValidClient: Bool?
    
    //Para Lista
    var nextViewController: AnyObject?
    var prevViewController: AnyObject?
    
    //Estructura de la vista
    var structVar: ContactRequestSurgeryViewStruct?
    var patientInfoStruct: PatientInfoViewStruct?

    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        loadData()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        executeDelegates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return 2
        }else if section == 2 {
            return 2
        }
        return 5
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;
        
        if showOrderDetail == false && indexPath.section == 1 {
                switch(indexPath.row) {
                case 0:
                    performSegueWithIdentifier(TableView.SegueIdentifiers.CreateDestination, sender: CatalogPosition.IdCustomerCatalog.hashValue)
                case 1:
                    performSegueWithIdentifier(TableView.SegueIdentifiers.CatalogSegue, sender: CatalogPosition.IdShipmentTypeCatalog.hashValue)
                default: break
                }
        }else if showOrderDetail == false && indexPath.section == 2 {
            switch(indexPath.row) {
            case 0:
                performSegueWithIdentifier(TableView.SegueIdentifiers.DoctorSegue, sender: CatalogPosition.IdDoctorCatalog.hashValue)
            default: break
            }
        }else if showOrderDetail == false && indexPath.section == 3 {
            switch(indexPath.row) {
            case 1:
                performSegueWithIdentifier(TableView.SegueIdentifiers.BillToTypesSegue, sender: nil)
            case 2 where idBillTo == TableView.IdBillToOtherCustomer:
                performSegueWithIdentifier(TableView.SegueIdentifiers.CatalogSegue, sender: CatalogPosition.IdOtherCustomerCatalog.hashValue)
            case 2 where idBillTo == TableView.IdBillToPatient:
                performSegueWithIdentifier(TableView.SegueIdentifiers.CounterSaleSegue, sender: nil)
            case 4:
                performSegueWithIdentifier(TableView.SegueIdentifiers.InfoDepositSegue, sender: nil)
            default: break
            }

        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if sender is Int{
            if segue.identifier == "goCreateDestination" {
                let controller: DestinationCatalogTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! DestinationCatalogTableViewController
                controller.idCatalog = sender as? Int
                controller.delegateAdd = self
                controller.delegate = self
            }else if segue.identifier == "goCreateDoctor" {
                    let controller: CatalogAddTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! CatalogAddTableViewController
                    controller.idCatalog = CatalogPosition.IdDoctorCatalog.rawValue
                    controller.delegate = self
            } else if let idCatalog: Int = sender as? Int where idCatalog >= 0 && segue.identifier == "goCatalogSelection2"  {
                let controller: CatalogSelectionTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! CatalogSelectionTableViewController
                    controller.idCatalog = idCatalog
                    controller.code = self.getCodeByCatalog(idCatalog)
                    controller.delegate = self
            }
        } else {
            if segue.identifier == TableView.SegueIdentifiers.BillToTypesSegue {
                let controller: BillToTypesTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! BillToTypesTableViewController
                controller.idBillTo = idBillTo
                controller.delegate = self
                controller.destinataryValidClient = destinataryValidClient
            }
        
            if segue.identifier == TableView.SegueIdentifiers.InfoDepositSegue {
                let controller: DepositRateTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! DepositRateTableViewController
                controller.isPopover = true
                controller.delegate = self
                controller.showOrderDetail = showOrderDetail == nil ? false : showOrderDetail!
                controller.bank = structVar?.bank
                controller.depositDate = structVar?.depositDate
                controller.reference = structVar?.referenceNumber
                controller.transaction = structVar?.transactionNumber
                controller.image = structVar?.image
            }
            
            if segue.identifier == TableView.SegueIdentifiers.CounterSaleSegue {
                let controller: PatientInfoTableViewController = (segue.destinationViewController as! UINavigationController).topViewController as! PatientInfoTableViewController
                controller.showButtons = showButtons
                controller.delegate = self
            }
        
            if segue.identifier == TableView.SegueIdentifiers.CreateOrderSegue {
                let controller: CreateRequestSurgeryTableViewController = segue.destinationViewController as! CreateRequestSurgeryTableViewController
                if let prevViewController = prevViewController as? SurgeryTypeTableViewController, surgeryStructVar = prevViewController.structVar {
                    controller.surgeryTypeStruct = surgeryStructVar
                }
                controller.contactRequestSurgeyStruct = structVar
                controller.billInfo = patientInfoStruct
                controller.showOrderDetail = false

            }
            
            if segue.identifier == SelectSegueIdentifier.SearchOrdersContact.rawValue {
                let splitViewController: UISplitViewController = segue.destinationViewController as! UISplitViewController
                let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
                navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
                navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
                splitViewController.delegate = splitViewController
            }
        }
    }
    
    // MARK: - TextField
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if showOrderDetail == true{
            return false
        }
        return true
    }
    
    @IBAction func switchDepositInfoChanged(sender: AnyObject) {

    }
    
    // MARK: - Business Functions
    
    func initView(){
        setNavigationButtons()
        setSwipes()
        textPatient.delegate = self
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func loadData(){
        setValues()
        tableView.reloadData()
    }
    
    func executeDelegates(){
        if !firstTime {
            setTextFieldValuesToStruct()
            delegate?.userDidEnterPatienInfo(patientInfoStruct)
            delegate?.userDidEnterContactRequestSurgery(structVar)
        }
    }
    
    func setNavigationButtons(){
        if showButtons == true {
            let buttonItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_option_accept"), style: UIBarButtonItemStyle.Plain, target: self, action: "goNext")
            let buttonItemExit = UIBarButtonItem(image: UIImage(named: "ic_option_exit"), style: UIBarButtonItemStyle.Plain, target: self, action: "goExit")
            navigationItem.rightBarButtonItems = [buttonItem, buttonItemExit]
        }
    }
    
    func setSwipes() {
        let swipeView = UISwipeGestureRecognizer(target: self, action: "respondToSwipe:")
        swipeView.direction = .Right
        navigationController?.navigationBar.addGestureRecognizer(swipeView)
        
        let secondSwipeView = UISwipeGestureRecognizer(target: self, action: "respondToSwipe:")
        secondSwipeView.direction = .Right
        tableView.addGestureRecognizer(secondSwipeView)
        
        let swipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeNext:")
        swipeViewNext.direction = .Left
        navigationController?.navigationBar.addGestureRecognizer(swipeViewNext)
        
        let secondSwipeViewNext = UISwipeGestureRecognizer(target: self, action: "respondToSwipeNext:")
        secondSwipeViewNext.direction = .Left
        tableView.addGestureRecognizer(secondSwipeViewNext)
    }
    
    func respondToSwipe(gesture:UISwipeGestureRecognizer){
        navigationController?.popViewControllerAnimated(true)
    }
    
    func respondToSwipeNext(gesture:UISwipeGestureRecognizer){
        goNext()
    }
    
    func setValues(){
        setStructValue()
        setInReadMode()
    }
    
    func setStructValue(){
        if structVar == nil {
            structVar = ContactRequestSurgeryViewStruct()
            setValueFolio(folioOrder)
        }else{
            setValuesFromStruct()
        }
    }
    
    func setValuesFromStruct(){
        setValueFolioView(structVar?.folioOrder)
        setValueCustomerView(structVar?.customerName, description: structVar?.customerDescription)
        setValueDoctorView(structVar?.doctorName)
        setValuePatientView(structVar?.patientName)
        setValueShipmentTypeView(structVar?.shipmentType)
        setValueBillToView(structVar?.billToTitle)
        setValueOtherCustomerBillView(structVar?.otherCustomerName)
        setValueDepositInfoView(structVar?.transactionNumber)
        setValueIdBillTo(structVar?.billToCode)
        setValueIdPaymentCondition(structVar?.paymentConditionCode)
        setValuePatientInfoView(patientInfoStruct?.taxId)
    }
    
    func setValueFolio(folioOrder: String?){
        setValueFolioView(folioOrder)
        structVar?.folioOrder = folioOrder
    }

    
    func setValueCustomer(number: String?, name: String?, description: String?){
        setValueCustomerView(name, description: description)
        structVar?.customerNumber = number
        structVar?.customerName = name
        structVar?.customerDescription = description
    }

    
    func setValueDoctor(name: String?){
        setValueDoctorView(name)
        structVar?.doctorName = name
    }

    
    func setValuePatient(name: String?){
        setValuePatientView(name)
        structVar?.patientName = name
    }

    
    func setValueShipmentType(code:String, name: String?){
        setValueShipmentTypeView(name)
        structVar?.shipmentType = name
        structVar?.shipmentTypeNumber = code
    }
    
    func setValueBillTo(code: String?, title: String?){
        setValueBillToView(title)
        
        structVar?.billToCode = code
        structVar?.billToTitle = title
    }
    
    func setValueIdBillTo(code: String?){
        if let code = code, _ = Int(code) {
            idBillTo = BillTo.getBillToByCode(code)?.id
        }
    }
    
    func setValueOtherCustomerBill(number: String?, name: String?){
        guard let number = number, let client = Customer.getElementById(number) else { return }
        
        setValueOtherCustomerBillView(client.name)
        
        structVar?.otherCustomerNumber = number
        structVar?.otherCustomerName = client.name
    }

    
    func setValueIdPaymentCondition(code: String?){
        if code != nil {
            idPaymentCondition = Int(code!)
        }
    }
    
    func setValueDepositInfo(bank: String?, date: String?, reference: String?, transaction: String?, image:UIImage?, hasDepositInfo: Bool?){
        setValueDepositInfoView(transaction)
        
        structVar?.bank = bank
        structVar?.depositDate = date
        structVar?.referenceNumber = reference
        structVar?.transactionNumber = transaction
        structVar?.hasDepositInfo = hasDepositInfo
        structVar?.image = image
    }
    
    func setInReadMode(){
        if self.showOrderDetail == true {
            var indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 1)
            var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            indexPath = NSIndexPath(forRow: 1, inSection: 1)
            cell = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            indexPath = NSIndexPath(forRow: 0, inSection: 2)
            cell = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            indexPath = NSIndexPath(forRow: 1, inSection: 3)
            cell = tableView.cellForRowAtIndexPath(indexPath)
            
            cell!.accessoryType = UITableViewCellAccessoryType.None
            
            textPatient.enabled = false
            switchInfoDeposit.enabled = false
            switchInfoBillTo.enabled = false
        }
    }
    
    func getCodeByCatalog(idCatalog: Int) -> String? {
        var code: String?
        switch(CatalogPosition.init(rawValue: idCatalog)){
        case CatalogPosition.IdCustomerCatalog:
            code = structVar?.customerNumber
        case CatalogPosition.IdDoctorCatalog:
            code = structVar?.doctorName
        case CatalogPosition.IdShipmentTypeCatalog:
            code = structVar?.shipmentType
        case CatalogPosition.IdBillToCatalog:
            code = structVar?.billToCode
        case CatalogPosition.IdOtherCustomerCatalog:
            code = structVar?.otherCustomerNumber
        default:break
        }
        
        return code
    }
    
    func setTextFieldValuesToStruct(){
        structVar?.patientName = textPatient.text
    }
    
    func goOut(){
        performSegueWithIdentifier(SelectSegueIdentifier.SearchOrdersContact.rawValue, sender: nil)
    }
    
    func goNext(){
        if isValidFields() {
            goNextScreen()
        }
    }
    
    func goNextScreen(){
        goCreateOrder()
    }
    
    func isValidCustomer() -> Bool{
        let customerNumber: String = self.getCustomerNumberForValidate()
        let customer: Customer? = Customer.getElementById(customerNumber)
        
        if customer != nil {
            if customer!.locked.boolValue {
                return false
            }else{
                return true
            }
        }
        
        return false
    }
    
    func isValidStatementCustomer() -> Bool{
        let customerNumber: String = self.getCustomerNumberForValidate()
        let statementsCustomer: NSMutableArray = StatementCustomer.getAllElementsByCustomer(customerNumber)
        
        for item in statementsCustomer {
            let statementCustomer: StatementCustomer = item as! StatementCustomer
            
            if Int(statementCustomer.daysDelayAfeterDue) > 10 {
                return false
            }
        }
        
        return true
    }
    
    func getCustomerNumberForValidate() -> String{
        var customerNumber: String?
        
        if self.idBillTo == TableView.IdBillToSameHospital {
            customerNumber = self.structVar?.customerNumber
        }else{
            customerNumber = self.structVar?.otherCustomerNumber
        }
        
        customerNumber = customerNumber == nil ? "0" : customerNumber
        
        return customerNumber!
    }
    
    func rejectOrderBy(reason: String){
        let alert: SCLAlertView = SCLAlertView()
        
        alert.addButton("Aceptar"){
            alert.hideView()
            self.dropOrderBy(reason)
        }
        
        alert.showError(GeneralData.AlertTitles.addOrderTitle, subTitle: "Se detendrá la Creación de la Orden por \(reason)", showDoneButton: false)
    }
    
    func dropOrderBy(reason: String){
        SwiftSpinner.show("Deteniendo Creación de la Orden por \(reason)")
        
        if self.structVar != nil && self.structVar?.folioOrder != nil && ConfigWS().isConnectedToNetwork() {
            dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
                let result: NSMutableDictionary = SurgeryRequestBusiness.rejectOrder((self.prevViewController as? SurgeryTypeTableViewController)!.structVar!, reason: reason)
                
                if result.valueForKey("errorMessage") == nil {
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        self.goOut()
                        //self.saveSynchronizeDate()
                    }
                }else{
                    let errorMessage: String = result.valueForKey("errorMessage") as! String
                    dispatch_async(GDCUtil.GlobalMainQueue) {
                        SwiftSpinner.hide()
                        SwiftSpinner.delay(seconds: 0.4, completion: {
                            SCLAlertView().showError(GeneralData.AlertTitles.errorTitle, subTitle: errorMessage)
                            self.goOut()
                        })
                    }
                }
            }
        }else{
            SwiftSpinner.delay(seconds: 2.0, completion: {
                SwiftSpinner.hide()
                self.goOut()
            })
        }
    }
    
    func goPantienInfo(showButtons: Bool, prevViewController: AnyObject?){
        if self.nextViewController == nil {
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            self.nextViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("PatientInfoTableViewController")
        }
        
        (self.nextViewController as? PatientInfoTableViewController)?.showOrderDetail = self.showOrderDetail
        (self.nextViewController as? PatientInfoTableViewController)?.showButtons = showButtons
        (self.nextViewController as? PatientInfoTableViewController)?.firstTime = self.firstTime
        (self.nextViewController as? PatientInfoTableViewController)?.prevViewController = prevViewController
        (self.nextViewController as? PatientInfoTableViewController)?.delegate = self
        
        if self.patientInfoStruct != nil {
            (self.nextViewController as? PatientInfoTableViewController)?.structVar = self.patientInfoStruct
        }
        
        self.navigationController?.pushViewController((self.nextViewController as? PatientInfoTableViewController)!, animated: true)
    }
    
    func goCreateOrder(){
        performSegueWithIdentifier(TableView.SegueIdentifiers.CreateOrderSegue, sender: nil)
    }
    
    func goExit(){
        SCLAlertView().showConfirm(GeneralData.AlertTitles.addOrderTitle, subTitle: "Esta apunto de perder los datos asignados hasta ahora. ¿Desea continuar?", buttonAcceptTarget: self, buttonAcceptSelector: "goOut")
    }
    
    func isValidFields() -> Bool{
        if let prevViewController = prevViewController as? SurgeryTypeTableViewController, let structPrev = prevViewController.structVar, let statistics = structPrev.statistics where statistics == true {
            return true
        }
        guard let customerNumber = structVar?.customerNumber where !customerNumber.isEmpty && customerNumber != "Vacío" else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Destinatario de Entrega")
            return false
        }
        
        guard let doctorName = structVar?.doctorName where !doctorName.isEmpty && doctorName != "Vacío" else {
            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar al Doctor")
            return false
        }
        
        
        guard let shipmentType = structVar?.shipmentType where !shipmentType.isEmpty && shipmentType != "Vacío" else {            SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe seleccionar el Tipo de Envío")
            return false
        }
        
        if switchInfoBillTo.on {
            let message = "Debe seleccionar a quien se le va a Facturar"
            guard let billToCode = structVar?.billToCode where !billToCode.isEmpty && billToCode != "Vacío" else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle:message)
                return false
            }
            let messageOtherCustomer = "Debe seleccionar Cliente"
            if billToCode == "Seleccione Cliente" {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle:messageOtherCustomer)
                return false
            }
            let messageTaxInformation = "Debe ingresar Datos de Facturación"
            if billToCode == "Ingrese Datos de Facturación" {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle:messageTaxInformation)
                return false
            }

        } else {
            if let prevViewController = prevViewController as? SurgeryTypeTableViewController {
                if let structPrev = prevViewController.structVar, let pendigs = structPrev.pendings {
                    if !pendigs.contains(PendingEnum.BillInfoConfirmation){
                        prevViewController.structVar!.pendings!.append(PendingEnum.BillInfoConfirmation)
                    }
                } else {
                    prevViewController.structVar!.pendings = [PendingEnum.BillInfoConfirmation]
                }
            }
        }
        
        if switchInfoDeposit.on {
            guard let paymentConditionCode = structVar?.transactionNumber where !paymentConditionCode.isEmpty && paymentConditionCode != "Vacío" else {
                SCLAlertView().showWarning(GeneralData.AlertTitles.errorTitle, subTitle: "Debe ingresar la Información de Deposito")
                return false
            }
        } else {
            if let prevViewController = prevViewController as? SurgeryTypeTableViewController {
                if let structPrev = prevViewController.structVar, let pendigs = structPrev.pendings {
                    if !pendigs.contains(PendingEnum.PaymentConfirmation){
                        prevViewController.structVar!.pendings!.append(PendingEnum.PaymentConfirmation)
                    }
                } else {
                    prevViewController.structVar!.pendings = [PendingEnum.PaymentConfirmation]
                }
            }
        }
        
        setTextFieldValuesToStruct()
        return true
    }
    
}

extension ContactRequestSurgeryTableViewController: BillToTypesDelegate {
    func userDidSelectBillToType(idBillTo: Int, title: String) {
        setValueBillTo("\(idBillTo)", title: title)
        self.idBillTo = idBillTo
        switch idBillTo {
        case 1 where destinataryValidClient == true:
            if let code = structVar?.customerNumber, let description = structVar?.customerDescription {
                setValueOtherCustomerBill(code, name: description)
            } else {
                setValueOtherCustomerBill(nil, name: "")
            }
        case 2:
            setValueOtherCustomerBill("-1", name: "Ingrese Datos de Facturación")
        case 3:
            setValueOtherCustomerBill("0", name: "Seleccione Cliente")
        default:
            break
        }
    }
}

extension ContactRequestSurgeryTableViewController: DepositRateDelegate {
    func userDidEnterDepositRateInfo(bankName: String, depositDate: String, referenceNumber: String, transactionNumber: String, image: UIImage?) {
        setValueDepositInfo(bankName, date: depositDate, reference: referenceNumber, transaction: transactionNumber, image: image, hasDepositInfo: true)
    }
}

extension ContactRequestSurgeryTableViewController:CatalogNewDelegate {
    func userDidSelectElementNew(code: String, description: String) {
        setValueDoctor(description)
    }
}

extension ContactRequestSurgeryTableViewController: CatalogDestinationDelegate{
    func userDidSelectElementDestination(code: String, description: String, detail: String) {
        destinataryValidClient = true
        setValueCustomer(code, name: detail, description: description)
        structVar?.customerDescription = description
    }
}

extension ContactRequestSurgeryTableViewController: DestinationInfoDelegate{
    func userDidEnterDestinationInfo(structVar2: DestinationInfoStruct?){
        destinataryValidClient = false
        guard let destination = structVar2 else {
            return
        }
        let destinationDirection = "\(destination.street!) \(destination.buildingNumber!), \(destination.postalCode!), \(destination.stateName!), \(destination.countryName!)"
        setValueCustomerView(destination.name, description: destinationDirection)
        structVar?.customerNumber = "0"
        structVar?.customerName = destinationDirection
        structVar?.destinationInfoStruct = structVar2
    }
}

extension ContactRequestSurgeryTableViewController: SurgeryRequestPendingDelegate {
    func addPending(pending: PendingEnum) {
        if let prevViewController = prevViewController as? SurgeryTypeTableViewController, let structPrev = prevViewController.structVar, let pendigs = structPrev.pendings {
            if !pendigs.contains(pending){
                prevViewController.structVar!.pendings!.append(PendingEnum.BillInfoConfirmation)
            }
        }
    }
}

extension ContactRequestSurgeryTableViewController: PatientInfoDelegate {
    func userDidEnterPatientInfo(structVar: PatientInfoViewStruct?) {
        patientInfoStruct = structVar
        setValueOtherCustomerBill("0", name: patientInfoStruct?.taxId)

    }
}

extension ContactRequestSurgeryTableViewController: CatalogDelegate {
    func userDidSelectElement(idCatalog: Int, code: String, description: String, detail: AnyObject?) {
        switch(CatalogPosition.init(rawValue: idCatalog)){
        case CatalogPosition.IdCustomerCatalog:
            setValueCustomer(code, name: description, description: detail as? String)
        case CatalogPosition.IdDoctorCatalog:
            setValueDoctor(description)
        case CatalogPosition.IdShipmentTypeCatalog:
            setValueShipmentType(code, name:description)
        case CatalogPosition.IdBillToCatalog:
            setValueBillTo(code, title: description)
        case CatalogPosition.IdOtherCustomerCatalog:
            setValueOtherCustomerBill(code, name: description)
        default:break
        }
    }
}


extension ContactRequestSurgeryTableViewController {
    
    func setValueFolioView(folioOrder: String?){
        if folioOrder != nil {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            if folioOrder!.isEmpty{
                cell?.detailTextLabel?.text = "0000000000"
            }else{
                cell?.detailTextLabel?.text = folioOrder
            }
        }
    }
    
    
    func setValueCustomerView(name: String?, description: String?){
        destinataryNameLabel.text = name
        destinataryDescriptionLabel.text = description
    }
    
    func setValueShipmentTypeView(name: String?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 1, inSection: 1)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
        cell?.detailTextLabel?.text = name
    }
    
    func setValueDoctorView(name: String?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 2)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
        cell?.detailTextLabel?.text = name
    }
    
    func setValuePatientView(name: String?){
        textPatient.text = name
    }
    
    
    func setValueBillToView(title: String?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 1, inSection: 3)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
        cell?.detailTextLabel?.text = title
    }
    
    func setValueOtherCustomerBillView(name: String?){
        let indexPath: NSIndexPath = NSIndexPath(forRow: 2, inSection: 3)
        let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
        cell?.detailTextLabel?.text = name
    }
    
    func setValueDepositInfoView(transaction: String?){
        if let transaction = transaction {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 4, inSection: 3)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            cell?.detailTextLabel?.text = transaction
            switchInfoDeposit?.on = true
        }
    }
    
    func setValuePatientInfoView(taxId: String?){
        if taxId != nil {
            let indexPath: NSIndexPath = NSIndexPath(forRow: 3, inSection: 2)
            let cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
            
            cell?.detailTextLabel?.text = taxId
        }
    }
    
}

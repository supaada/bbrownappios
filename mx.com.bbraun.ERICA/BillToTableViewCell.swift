//
//  BillToTableViewCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/6/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

class BillToTableViewCell: UITableViewCell {
    
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!

    func config(clientName:String, clientId:String, paymentType:String){
        clientNameLabel.text = clientName
        paymentTypeLabel.text = paymentType
    }
    
}

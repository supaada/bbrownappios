//
//  SynchronizeTableViewController.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//
import UIKit

class SynchronizeTableViewController: UITableViewController {
    @IBOutlet weak var syncronizeDateLabel: UILabel!
    @IBOutlet weak var syncronizeOrdersDateLabel: UILabel!
    @IBOutlet weak var numberPendingOrdersLabel: UILabel!

    var areThereCatalogs: Bool = true
    
    override func  preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        // this is where you set your color view
        let customColorView: UIView = UIView()
        
        customColorView.backgroundColor = GeneralData.Appearance.thirdColor
        cell!.selectedBackgroundView =  customColorView;
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    @IBAction func syncronizeCustomersTapped(sender: AnyObject) {
        SCLAlertView().showConfirm(GeneralData.AlertTitles.syncCatalogTitle, subTitle: "Esto iniciara la sincronización. ¿Desea continuar con el proceso?", buttonAcceptTarget: self, buttonAcceptSelector: "syncronizeCustomers")
    }
    
    @IBAction func syncronizePendingOrdersTapped(sender: AnyObject) {
        SCLAlertView().showConfirm(GeneralData.AlertTitles.syncOrdersTitle, subTitle: "Esto iniciara la sincronización. ¿Desea continuar con el proceso?", buttonAcceptTarget: self, buttonAcceptSelector: "syncronizePendingOrders")
    }
    
    // MARK: - Bussines functions
    
    func initView(){
        
    }
    
    func loadData(){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        let synchronizeDate = NSUserDefaults.standardUserDefaults().valueForKey("synchronizeDate") as? NSDate
        let synchronizePendingOrdersDate = NSUserDefaults.standardUserDefaults().valueForKey("synchronizePendingOrdersDate") as? NSDate
        
        if synchronizeDate != nil {
            syncronizeDateLabel.text = dateFormatter.stringFromDate(synchronizeDate!)
        }
        
        if synchronizePendingOrdersDate != nil {
            syncronizeOrdersDateLabel.text = dateFormatter.stringFromDate(synchronizePendingOrdersDate!)
        }
        
        if areThereCatalogs {
            showNumberPendingOrders()
        }
    }
    
    func showNumberPendingOrders(){
        let numberPendingOrders: Int = OrderSurgery.countPendingOrders()
        
        numberPendingOrdersLabel.text = "\(numberPendingOrders)"
    }
    
    func syncronizeCustomers(){
        SwiftSpinner.show("Descargando Catálogos")

        /* CARGA CATALOGOS*/
        //dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
            let errorMessage: String = SynchronizeBusiness.syncronizeCatalogs()
            
            print("errorMessage \(errorMessage)")
            if errorMessage.isEmpty {
                //dispatch_async(GDCUtil.GlobalMainQueue) {
                    self.saveSynchronizeDate()
                    print("uno")
                    SwiftSpinner.delay(seconds: 3.0, completion: {
                        SwiftSpinner.hide()
                        SCLAlertView().showSuccess(GeneralData.AlertTitles.syncCatalogTitle, subTitle: "Sincronización de catálogos SATISFACTORIA")
                    })
                //}
            }else{
                //dispatch_async(GDCUtil.GlobalMainQueue) {
                    SwiftSpinner.hide()
                    print("else")
                    self.saveSynchronizeDate()
                    SwiftSpinner.delay(seconds: 3.0, completion: {
                        SCLAlertView().showError(GeneralData.AlertTitles.errorTitle, subTitle: errorMessage)
                    })
                //}
            }
        //}
    }
    
    func syncronizePendingOrders(){
        SwiftSpinner.show("Enviando Pedidos Pendientes")
        
        dispatch_async(GDCUtil.GlobalUserInteractiveQueue) {
            let errorMessage: String = SynchronizeBusiness.syncronizeOrders()
            
            dispatch_async(GDCUtil.GlobalMainQueue) {
                SwiftSpinner.delay(seconds: 1, completion: {
                    SwiftSpinner.hide()
                    SCLAlertView().showSuccess(GeneralData.AlertTitles.syncOrdersTitle, subTitle: "Sincronización TERMINADA")
                })
                self.saveSynchronizePendingOrdersDate()
                self.showNumberPendingOrders()
            }
        }
    }
    
    func saveSynchronizeDate(){
        let dateFormatter = NSDateFormatter()
        let date = NSDate()
        
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        NSUserDefaults.standardUserDefaults().setValue(date, forKeyPath: "synchronizeDate")
        NSUserDefaults.standardUserDefaults().synchronize()
        syncronizeDateLabel.text = dateFormatter.stringFromDate(date)
    }
    
    func saveSynchronizePendingOrdersDate(){
        let dateFormatter = NSDateFormatter()
        let date = NSDate()
        
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        NSUserDefaults.standardUserDefaults().setValue(date, forKeyPath: "synchronizePendingOrdersDate")
        NSUserDefaults.standardUserDefaults().synchronize()
        syncronizeOrdersDateLabel.text = dateFormatter.stringFromDate(date)
    }
    
    func getHead(order: NSArray) -> NSDictionary{
        let header: String = order[0] as! String
        let arrayStringHead: [String]  =  header.characters.split {$0 == "|"}.map { String($0) }
        
        let headDictionary: NSDictionary = ["numberOrder":arrayStringHead[0], "subtotal":arrayStringHead[1], "discount":arrayStringHead[2], "tax":arrayStringHead[3], "total":arrayStringHead[4], "currency":arrayStringHead[5]]
        
        return headDictionary
    }
    
    func getDetail(order: NSArray) -> NSArray{
        let items: NSMutableArray = NSMutableArray()
        
        for index in 1..<order.count{
            let detail: String = order[index] as! String
            let arrayStringDetail  =  detail.characters.split {$0 == "|"}.map { String($0) }
            let detailDictionary: NSDictionary = ["numberProduct":arrayStringDetail[0], "unitPrice":arrayStringDetail[2], "discount":arrayStringDetail[4], "subtotal":arrayStringDetail[5], "tax":arrayStringDetail[6], "total":arrayStringDetail[7], "unitMeasurement":arrayStringDetail[8]]
            
            items.addObject(detailDictionary)
        }
        
        return items
    }
    
    func getOrderFromWebServiceResult(result: NSDictionary) -> NSArray{
        let response: NSDictionary = result["ns1:out"] as! NSDictionary
        let arrayOfString: NSArray = response["ns1:string"] as! NSArray
        
        return arrayOfString
    }
}

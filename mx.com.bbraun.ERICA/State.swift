//
//  State.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(State)
class State: NSManagedObject {

    class StateDic:NSObject {
        init(code : String, name : String, countryCode: String) {
            self.code = code
            self.name = name
            self.countryCode = countryCode
        }
        var code: String
        var name: String
        var countryCode: String
        
        override func isEqual(object: AnyObject?) -> Bool {
            if let object = object as? StateDic {
                return countryCode == object.countryCode && code == object.code
            } else {
                return false
            }
        }
        
        override var hash: Int {
            return (code+countryCode).hashValue
        }
    }
    @NSManaged var code: String
    @NSManaged var name: String
    @NSManaged var country: Country

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(State))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            let dictionary = item as! NSDictionary
            let state:StateDic = StateDic(code: dictionary["stateCode"] as! String, name: dictionary["stateName"] as! String, countryCode:  dictionary["countryCode"] as! String)
            saveInfo(state)
        }
        
        CoreDataHelper.saveContext()
    }
    
    class func saveInfo(row: StateDic){
        let country: Country? = Country.getElementById(row.countryCode)

        let newRow: State = CoreDataHelper.getRowEntity(NSStringFromClass(State)) as! State
        if let countryT = country {

            newRow.country = countryT
            newRow.code = row.code
            newRow.name = row.name
        }
    }

}

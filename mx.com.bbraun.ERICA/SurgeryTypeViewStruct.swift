//
//  SurgeryTypeViewStruct.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

public enum PendingEnum:Int {
    case OffLine = 0, InstrumentalSetAgendaConfirmation, ComponentsInventoryConfirmation, SAPConfirmation, BillInfoConfirmation, PaymentConfirmation, None
    
    public init(rawValue:Int) {
        switch(rawValue) {
        case 0:
                self = OffLine
        case 1:
            self = InstrumentalSetAgendaConfirmation
        case 2:
            self = ComponentsInventoryConfirmation
        case 3:
            self = SAPConfirmation
        case 4:
            self = BillInfoConfirmation
        case 5:
            self = PaymentConfirmation
        default:
            self = None
        }
    }
    
    public func getMessage() -> String {
        switch(self){
        case .OffLine:
            return "Conectese a Internet para enviar la Solicitud de Cirugía"
        case .InstrumentalSetAgendaConfirmation:
            return Reachability.isConnectedToNetwork() ? "Comuniquese con el Coordinador de ERICA para confirmar Agenda de Instrumental" : "Conectese a Internet para poder validar Agenda de Instrumental"
        case .ComponentsInventoryConfirmation:
            return Reachability.isConnectedToNetwork() ? "Comuniquese con el coordinador de ERICA para confirmar Inventario de Componentes" : "Conectese a Internet para poder confirmar Inventario de Componentes"
        case .SAPConfirmation:
            return "Pendiente de Documento SAP"
        case .BillInfoConfirmation:
            return "Ingrese Datos de Facturación"
        case .PaymentConfirmation:
            return "Ingrese Información de Depósito"
        case .None:
            return ""
        }
    }
    
    public func getImage() -> UIImage? {
        switch(self){
        case .OffLine:
            return UIImage(named: "offlineFlat")
        case .InstrumentalSetAgendaConfirmation:
            return UIImage(named: "agendaFlat")
        case .ComponentsInventoryConfirmation:
            return UIImage(named: "inventoryFlat")
        case .SAPConfirmation:
            return UIImage(named: "logoSapFlat")
        case .BillInfoConfirmation:
            return UIImage(named: "billToFlat")
        case .PaymentConfirmation:
            return UIImage(named: "creditFlat")
        case .None:
            return UIImage(named: "offlineFlat")
        }
    }
}

public protocol RequestPendingsDelegate{
    func updateRequestPendings(pendigs:[String])
}

struct SurgeryTypeViewStruct {
    
    var statistics: Bool?
    var pendings: [PendingEnum]?
    var folioOrder: String?
    var surgeryTypeNumber: String?
    var surgeryTypeName: String?
    var surgeryFamilyTypeNumber: String?
    var surgeryFamilyTypeName: String?
    var surgerySetNumber: String?
    var surgerySetName: String?
    var surgeryDateTime: String?
    var id: NSNumber?
    var implantSystemCode: String?
    var implantSystemName: String?
    var implantSystemAvailability: String?
    var implantSystemRequired: Bool?
    var instrumentalSetCode: String?
    var instrumentalSetName: String?
    var instrumentalSetAvailability: String?
    var instrumentalSetRequired: Bool?
    var technicalNumber: String?
    var technicalName: String?
    var technicalAvailability: String?
    var technicalRequired: Bool?
    var salesRepresentativeNumber: String?
    var salesRepresentativeName: String?
    var statusId: NSNumber?
    var priceListCode: String?
    var priceListName: String?
    var priceImplantSystem: NSNumber?
    
}
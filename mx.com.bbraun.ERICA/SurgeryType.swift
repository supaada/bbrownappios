//
//  SurgeryType.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(SurgeryType)
class SurgeryType: NSManagedObject {
    struct SurgeryTypeProperties {
        static let idFilter = "number == %@"
        static let idFilterName = "name == %@"
    }

    @NSManaged var name: String
    @NSManaged var number: String
    @NSManaged var implantSystem: NSSet
    @NSManaged var instrumentalSet: NSSet

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(SurgeryType))
    }
    
    class func saveData(array: NSArray){
        
        for item in array {
            let surgery:NSDictionary = item as! NSDictionary;
            let newRow: SurgeryType = CoreDataHelper.getRowEntity(NSStringFromClass(SurgeryType)) as! SurgeryType
            
            newRow.number = surgery["number"] as! String
            newRow.name = surgery["name"] as! String
        }
        
        CoreDataHelper.saveContext()
        
        print("surgery type saved")
    }
    
    class func getElementById(number: String) -> SurgeryType?{
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(SurgeryType), filter: SurgeryTypeProperties.idFilter, value: number)
        let element: SurgeryType? = array.last as? SurgeryType
        
        return element
    }
    
    class func getAllElements() -> NSMutableArray{
        let surgeryTypes: NSMutableArray = NSMutableArray()
        var surgeryTypeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchAllEntity(NSStringFromClass(SurgeryType))
        var surgeryTypeDicUnique = [String:String]()
        
        for item in array{
            surgeryTypeDic = NSMutableDictionary()
            if let current = item as? SurgeryType {
                if let _ = surgeryTypeDicUnique[current.name] {
                }else {
                    surgeryTypeDic!.setValue(current.name, forKey: "description")
                    surgeryTypeDic!.setValue(current.number, forKey: "code")
                    surgeryTypeDic!.setValue(false, forKey: "flag")
                    surgeryTypes.addObject(surgeryTypeDic!)
                    surgeryTypeDicUnique[current.name] = current.number
                }
            }
        }
        
        return surgeryTypes
    }
    
    class func getAllElementsFam(nameDelegate:String) -> NSMutableArray{
        let surgeryTypes: NSMutableArray = NSMutableArray()
        var surgeryTypeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString(NSStringFromClass(SurgeryType), filter: SurgeryTypeProperties.idFilterName, value: nameDelegate)
        
        for item in array{
            surgeryTypeDic = NSMutableDictionary()
            if let current = item as? SurgeryType {
                    surgeryTypeDic!.setValue(current.name, forKey: "description")
                    surgeryTypeDic!.setValue(current.number, forKey: "code")
                    surgeryTypeDic!.setValue(false, forKey: "flag")
                    surgeryTypes.addObject(surgeryTypeDic!)
            }
        }
        
        return surgeryTypes
    }
    
    class func getAllElementsT() -> NSArray{
        return CoreDataHelper.fetchAllEntity(NSStringFromClass(SurgeryType))
    }

}

//
//  SynchronizeBusiness.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 08/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

class SynchronizeBusiness{
    
    class func syncronizeCatalogs() -> String {
        var errorMessage:String = ""

        if !Reachability.isConnectedToNetwork() {
            errorMessage = ConfigWS().ERROR_NETWORK_CONNECTION
        }
        print("sin errores va a sincronizar")
        if errorMessage.isEmpty {
            //dispatch_sync(GDCUtil.GlobalMainQueue) {
                SynchronizeBusiness.deleteCatalogs()
                SynchronizeBusiness.saveDataToAllCatalogs()
            //}
        }
        return errorMessage
    }
    
    class func deleteCatalogs() {
        Surgery.deleteDataAll()
        Doctor.deleteData()
        ShipmentType.deleteData()
        SalesRepresentative.deleteData()
        State.deleteData()
        Country.deleteData()
        Customer.deleteData()
        
    }
    
    class func saveDataToAllCatalogs() -> String{
        var errorMessage:String = ""
        print("antes de sincronizar")
        DataManager.getCatalogWithSuccess(URL.AllCatalogos) { (dData, error) -> Void in
            let json = JSON(data:dData)
            if error != nil {
                errorMessage = "No se pudo sincronizar catalogos"
            }

            var catalogArray:NSMutableArray = []
            // Carga Catalogo de Paises y Estados
            if let arrayStates = json["catalogos"]["paisesEstados"].array {
                for appDic in arrayStates {
                    if let countryCode:String = appDic["clvPais"].string, let countryName:String = appDic["nombrePais"].string, let stateCode:String = appDic["clvEstado"].string, let stateName:String = appDic["nombreEstado"].string {
                        let dictionary: NSDictionary = ["countryCode":countryCode,"countryName":countryName, "stateCode":stateCode, "stateName":stateName]
                        catalogArray.addObject(dictionary)
                        
                    }
                }
                Country.saveData(catalogArray)
                State.saveData(catalogArray)
                print("States")
            }
            
            // Carga Catalogo de Representantes de Ventas
            if let arraySalesRepresentatives = json["catalogos"]["representantesVentas"].array {
                catalogArray = []
                for appDic in arraySalesRepresentatives {
                    if let name:String = appDic["nombreRepVtas"].string, number:String = appDic["numEmpleado"].string, num:Int = Int(number){
                        let dictionary: NSDictionary = ["number":"\(num)","name":name]
                        catalogArray.addObject(dictionary)
                    }
                }
                SalesRepresentative.saveData(catalogArray)
                print("sales representatives")
            }
            
            // Carga Catalogo de Tipos de Envio
            print("tipos envio \(json["catalogos"]["tiposEnvio"])")
            print("tipos envio \(json["catalogos"]["tiposEnvio"].count)")
            if let arrayShipmentTypes = json["catalogos"]["tiposEnvio"].array {
                catalogArray = []
                for appDic in arrayShipmentTypes {
                    if let name = appDic["tipoEnvio"].string, mode = appDic["tipoEnvio"].string, typeNumber = appDic["numeroTipoEnvio"].int {
                        let dictionary: NSDictionary = ["name":name,"mode":mode, "type":String(typeNumber)]
                        catalogArray.addObject(dictionary)
                    }
                }
                ShipmentType.saveData(catalogArray)
                print("shipment type \(catalogArray.count)")
            }
            
            // Carga Catalogo de Doctores
            if let arrayDoctores = json["catalogos"]["doctores"].array {
                var doctors = [String]()
                for appDic in arrayDoctores {
                    if let doctorName:String = appDic["nombreDoctor"].string {
                        doctors.append(doctorName)
                    }
                }
                Doctor.saveData(doctors)
                print("doctors")
            }
            
            // TODO - Carga Catalogo de Destinatarios
            if let arrayDestinatary = json["catalogos"]["destinatarios"].array {
                catalogArray = []
                for appDic in arrayDestinatary {
                    if let country:String = appDic["clvPais"].string, customer = appDic["numDestinatario"].number , customerNumber:String = String(customer), name:String = appDic["nombre"].string, number:String = appDic["numeroExt"].string, postalCode:String = appDic["cp"].string, region:String = appDic["distrito"].string, state:String = appDic["clvEstado"].string, street:String = appDic["calle"].string, taxId:String = appDic["rfc"].string, town:String = appDic["poblacion"].string {
                        
                        let dictionary: NSDictionary = ["country":country,"customerNumber":customerNumber, "locked":"1", "name":name, "number":number, "postalCode":postalCode, "region":region, "state":state, "street":street, "taxId":taxId, "totalCredit":"0", "town":town]
                        catalogArray.addObject(dictionary)
                    }
                }
                Customer.saveData(catalogArray)
            }
            
            print("json[catalogos][tiposCirugia].array \(json["catalogos"]["tiposCirugia"].array)")
            // Carga Catalogo de Tipos de Cirugia
            if let arraySurgeryType = json["catalogos"]["tiposCirugia"].array {
                catalogArray = []
                print("Surgery Type sent \(arraySurgeryType.count)")
                for appDic in arraySurgeryType {
                    
                    if let surgeryId = appDic["clvTipoCirugia"].int, surgeryCode:String = appDic["tipoCirugia"].string, surgerySetFamily:String = appDic["familiaTipoCirugia"].string, surgerySetCode:String = String(surgeryId), surgerySetDescription:String = appDic["descripcionSetTipoCirugia"].string, precio:Int = appDic["precio"].int {
                        let instrumentalSetCode = surgeryId
                        let claveSetImplante = surgeryId
                        let instrumentalBase:String = appDic["setInstrumental"]["base"].string ?? ""
                        let aditional1:String = appDic["setInstrumental"]["adicional1"].string ?? ""
                        let aditional2:String = appDic["setInstrumental"]["adicional2"].string ?? ""
                        let aditional3:String = appDic["setInstrumental"]["adicional3"].string ?? ""
                        let aditional4:String = appDic["setInstrumental"]["adicional4"].string ?? ""
                        let implantBase:String = appDic["setImplante"]["base"].string ?? ""
                        let aditionalImplant1:String = appDic["setImplante"]["adicional"].string ?? ""
                        
                        let dictionary: NSDictionary = ["surgeryId":surgeryId,"surgeryCode":surgeryCode, "surgerySetFamily":surgerySetFamily, "surgerySetCode":surgerySetCode, "surgerySetDescription":surgerySetDescription, "instrumentalSetCode":instrumentalSetCode, "instrumentalBase":instrumentalBase, "aditional1": aditional1, "aditional2":aditional2, "aditional3":aditional3, "aditional4":aditional4, "claveSetImplante":claveSetImplante,"implantBase":implantBase, "aditionalImplant1":aditionalImplant1, "precio":precio ]
                        catalogArray.addObject(dictionary)
                    }
                }
                print("Surgery Type \(catalogArray.count)")
                Surgery.saveData(catalogArray)
                print("Surgery Type sent \(Surgery.getAllElementsSurgery().count)")

                print("Surgery Types")
            }
        }
        //
        print("exit errorMessage \(errorMessage)")
        return errorMessage
    }
    
    class func syncronizeOrders() -> String{
        let pendingOrders: NSArray = OrderSurgery.getAllOrdersToSend()
        var errorMessage: String = ""
        print("pendingOrders \(pendingOrders.count)")
        for item in pendingOrders {
            var result: NSMutableDictionary = NSMutableDictionary()
            print("syncronizeOrders item \(item)")
            if let order: OrderSurgery = item as? OrderSurgery {
                let patientInfoStruct: PatientInfoViewStruct? = SurgeryRequestBusiness.getValuePatientInfoStruct(order.patientInfo)
                let contactRequestSurgeyStruct: ContactRequestSurgeryViewStruct? = SurgeryRequestBusiness.getValueContactRequestSurgeryStruct(order, toSynchronize: true)
                let surgeryTypeStruct: SurgeryTypeViewStruct? = SurgeryRequestBusiness.getValueSurgeryTypeStruct(order, toSynchronize: true)
                let comments: String? = order.comments
            
                if order.statusId == GeneralData.Status.created {
                    result = SurgeryRequestBusiness.synchronizeOrder(surgeryTypeStruct!, contactRequestSurgery: contactRequestSurgeyStruct, patientInfo: patientInfoStruct, comments: comments)
                }else{
                    result = SurgeryRequestBusiness.rejectOrder(surgeryTypeStruct!, reason: comments!)
                }
            } else {
                result.setValue("Imposible sincronizar solicitud", forKey: "errorMessage")
            }
            if result.valueForKey("errorMessage") != nil {
                errorMessage += result.valueForKey("errorMessage") as! String
                errorMessage += "\n"
            }

        }
        
        print(errorMessage)
        return errorMessage
    }
    
    class func synchronizeOrderRequest() -> String{
        print("entering -> synchronizeOrderRequest")
        let pendingOrders: NSArray = OrderSurgery.getAllOrdersToSend()
        let errorMessage: String = ""
        for item in pendingOrders {
            var result: NSMutableDictionary = NSMutableDictionary()
            if let order: OrderSurgery = item as? OrderSurgery {
                let patientInfoStruct: PatientInfoViewStruct? = SurgeryRequestBusiness.getValuePatientInfoStruct(order.patientInfo)
                let contactRequestSurgeyStruct: ContactRequestSurgeryViewStruct? = SurgeryRequestBusiness.getValueContactRequestSurgeryStruct(order, toSynchronize: true)
                let surgeryTypeStruct: SurgeryTypeViewStruct? = SurgeryRequestBusiness.getValueSurgeryTypeStruct(order, toSynchronize: true)
                let comments: String? = order.comments
                print("folio to sync \(order.folioOrder)")
                SurgeryRequestBusiness.synchronizeOrderRequest(order.folioOrder, surgeryType: surgeryTypeStruct!, contactRequestSurgery: contactRequestSurgeyStruct, patientInfo: patientInfoStruct, comments: comments)
                
            } else {
                result.setValue("Imposible sincronizar solicitud", forKey: "errorMessage")
            }
        }
        
        print(errorMessage)
        
        return errorMessage
    }
}
//
//  MenuStruct.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/24/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

struct MenuStruct {
    
    let title: String
    let description: String
    let image: UIImage?
    let forward: String

}
//
//  PendingsSumaryTableViewCell.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/6/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import UIKit

protocol PendingButtonDelegate {
    func pendingPressed(pending:PendingEnum)
}


class PendingsSumaryTableViewCell: UITableViewCell {

    @IBOutlet weak var firstPendingButton: UIButton!
    @IBOutlet weak var secondPendingButton: UIButton!
    @IBOutlet weak var thirdPendingButton: UIButton!
    @IBOutlet weak var fourthPendingButton: UIButton!
    @IBOutlet weak var fifthPendingButton: UIButton!
    
    var pendings : [PendingEnum]?
    var delegate: PendingButtonDelegate?

    
    func config(pendings:[PendingEnum]){
        self.pendings = pendings
        let buttons = [firstPendingButton, secondPendingButton,thirdPendingButton, fourthPendingButton, fifthPendingButton]
        for position in 0...4 {
            if pendings.count >= (position + 1) {
                buttons[position].setImage(pendings[position].getImage(), forState: .Normal)
                buttons[position].hidden = false
            } else {
                buttons[position].hidden = true
            }
        }
    }
    
    @IBAction func firstPendingButtonPressed(sender: UIButton) {
        if let pendings = pendings where pendings.count > 0{
            delegate?.pendingPressed(pendings[0])
        }
    }

    @IBAction func seconfPendingButtonPressed(sender: UIButton) {
        if let pendings = pendings where pendings.count > 1{
            delegate?.pendingPressed(pendings[1])
        }
    }
    
    @IBAction func thirdPendingButtonPressed(sender: UIButton) {
        if let pendings = pendings where pendings.count > 2{
            delegate?.pendingPressed(pendings[2])
        }    }
    
    @IBAction func fourthPendingButtonPressed(sender: UIButton) {
        if let pendings = pendings where pendings.count > 3{
            delegate?.pendingPressed(pendings[3])
        }
    }
    
    @IBAction func fifthPendingButtonPressed(sender: UIButton) {
        if let pendings = pendings where pendings.count > 4{
            delegate?.pendingPressed(pendings[4])
        }
    }
    
}

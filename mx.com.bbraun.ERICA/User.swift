//
//  User.swift
//  ERICA
//
//  Created by Administrador Prospectiva on 07/07/15.
//  Copyright (c) 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
class User: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var lastOrderId: NSNumber
    @NSManaged var name: String
    @NSManaged var password: String

    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(User))
    }
    
    class func save(id: NSNumber, name: String, password: String) -> User{
        let newUser: User = CoreDataHelper.getRowEntity(NSStringFromClass(User)) as! User
        
        newUser.id = id
        newUser.name = name
        newUser.password = password
        newUser.lastOrderId = NSNumber(integer: 0)
        
        CoreDataHelper.saveContext()
        
        return newUser
    }
    
    class func getUserInfo() -> User?{
        let users: [User] = CoreDataHelper.fetchAllEntity(NSStringFromClass(User)) as! [User]
        var user: User?
        
        if users.count > 0{
            user = users[0]
        }
        
        return user
    }
}

//
//  ContactRequestSurgeryViewStruct.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/2/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation

struct ContactRequestSurgeryViewStruct {
    var pendings: [String]?
    var folioOrder: String?
    var customerName: String?
    var customerDescription: String?
    var customerNumber: String?
    var doctorNumber: String?
    var doctorName: String?
    var patientName: String?
    var shipmentType: String?
    var shipmentTypeNumber: String?
    var billToCode: String?
    var billToTitle: String?
    var otherCustomerName: String?
    var otherCustomerNumber: String?
    var paymentConditionCode: String?
    var paymentConditionTitle: String?
    
    var hasDepositInfo: Bool?
    var bank: String?
    var depositDate: String?
    var referenceNumber: String?
    var transactionNumber: String?
    var image: UIImage?
    
    var destinationInfoStruct: DestinationInfoStruct?
}
//
//  SalesRepresentative+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 1/6/16.
//  Copyright © 2016 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SalesRepresentative {

    @NSManaged var name: String?
    @NSManaged var number: String?

}

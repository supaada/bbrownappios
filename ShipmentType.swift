//
//  ShipmentType.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 12/30/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//

import Foundation
import CoreData

@objc(ShipmentType)
class ShipmentType: NSManagedObject {

    
    class func deleteData(){
        CoreDataHelper.deleteFrom(NSStringFromClass(ShipmentType))
    }
    
    class func saveData(array: NSArray){
        for item in array {
            let dictionary = item as! NSDictionary
            let newRow: ShipmentType = CoreDataHelper.getRowEntity(NSStringFromClass(ShipmentType)) as! ShipmentType
            
            newRow.name = dictionary["name"] as? String
            newRow.mode = dictionary["mode"] as? String
            newRow.type = dictionary["type"] as? String
        }
        CoreDataHelper.saveContext()
    }
    
    class func getAllElementsFilter(shipmenType: String) -> NSMutableArray{
        let shipmentTypes: NSMutableArray = NSMutableArray()
        var shipmentTypeDic: NSMutableDictionary?
        let array = CoreDataHelper.fetchFilterEntityString("ShipmentType", filter: "type like[c] %@", value: shipmenType)
        
        for item in array{
            shipmentTypeDic = NSMutableDictionary()
            shipmentTypeDic!.setValue((item as! ShipmentType).name, forKey: "name")
            shipmentTypeDic!.setValue((item as! ShipmentType).mode, forKey: "mode")
            shipmentTypeDic!.setValue((item as! ShipmentType).type, forKey: "type")
            shipmentTypeDic!.setValue((item as! ShipmentType).name, forKey: "description")
            shipmentTypeDic!.setValue((item as! ShipmentType).type, forKey: "code")
            
            shipmentTypes.addObject(shipmentTypeDic!)
        }
        
        return shipmentTypes
    }
    
    class func getAllElements() -> NSArray{
        print("Entering getAllElements")
        guard let shipmentTypes:[ShipmentType] = CoreDataHelper.fetchAllEntity("ShipmentType") as? [ShipmentType] else {
            print("Couldnt parse")
            return []
        }
        print("Parse Correct \(shipmentTypes.count)")
        let catalog = shipmentTypes.map { (shipmentType:ShipmentType) -> [String:String] in
            return ["code":shipmentType.type!,"description":shipmentType.name!]
        }
        print("let catalog \(catalog.count)")
        return catalog
    }
}

//
//  Surgery+CoreDataProperties.swift
//  ERICA
//
//  Created by Luis Alfonso Marquez Lecona on 11/4/15.
//  Copyright © 2015 B. Braun de Mexico. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Surgery {

    @NSManaged var claveSetCirugia: String?
    @NSManaged var descripcionSetCirugia: String?
    @NSManaged var familyType: String?
    @NSManaged var uuid: NSNumber?
    @NSManaged var precio: NSNumber?
    @NSManaged var surgeryType: String?
    @NSManaged var implantSet: ImplantSet?
    @NSManaged var surgeryTypeSet: SurgeryTypeSet?

}
